# Finance Analysis

#### 股票价格数学模型

* 股票价格数学模型是针对股票价格与时间之间的数量依存关系，通过抽象和简化，采用数学语言和方法，概括地或近似地表述出的一种数学结构。股票价格数学模型从数量关系上对股票价格运动作形式化的描述和刻画，可为研究股票价格运动现象及规律提供简洁精确的数学符号语言。

#### 时间序列分析

* 时间序列分析（Time Series Analysis）是量化投资中的一门基本技术。时间序列是指在一定时间内按时间顺序测量的某个变量的取值序列。比如变量是股票价格，那么它随时间的变化就是一个时间序列；同样的，如果变量是股票的收益率，则它随时间的变化也是一个时间序列。时间序列分析就是使用统计的手段对这个序列的过去进行分析，以此对该变量的变化特性建模、并对未来进行预测。
* 时间序列分析试图通过研究过去来预测未来。

#### 参与贡献

1.  GF Fork 本仓库
2.  GF 新建 master 分支
3.  GF 提交代码
4.  GF 新建 Pull Request

----------------------------------------------------------------------------------------------------
#### EOF

