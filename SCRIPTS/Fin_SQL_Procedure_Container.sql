-- Fin_SQL_Procedure_Container.sql

-- Create By GF 2023-10-14 15:45

/* -------------------------------------------------- */

/*
 * #######################################################################################
 * ##                                                                                   ##
 * ## Due to configuration reasons, MySQL does not distinguish between Upper and Lower. ##
 * ##                                                                                   ##
 * ## Beware of the parameter names of functions being the same as table field names.   ##
 * ##                                                                                   ##
 * #######################################################################################
 */

/* -------------------------------------------------- */

/* Drop Table: Container (For Calculation Purposes) */
DROP TABLE IF EXISTS container;

/* Create Table: Container (For Calculation Purposes) */
CREATE TABLE IF NOT EXISTS container(cntr_index BIGINT(12) DEFAULT 0, /* Index(Non System Index) : Alternatively, Use "AUTO_INCREMENT" */
                                     cntr_class CHAR(80), /* 类别(Class) */
                                     cntr_name CHAR(80), /* 名称(Name) */
                                     cntr_number BIGINT(12) DEFAULT 0, /* 编号(Number) */
                                     cntr_purpose CHAR(80), /* 用途(Purpose) */
                                     cntr_bigint_value BIGINT(12),
                                     cntr_double_value DOUBLE(16,4),
                                     PRIMARY KEY(cntr_index, cntr_class, cntr_name, cntr_number)) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4;

/* -------------------------------------------------- */

-- 结束符修改。
DELIMITER //

/* -------------------------------------------------- */
/* Container Variable Procedure */

/* 删除存储过程: 容器 - 变量管理 (Container - Variable Manage - Double) */
DROP PROCEDURE IF EXISTS PROC_CNTR_VARIABLE_MANAGE_DOUBLE//

/* 创建存储过程: 容器 - 变量管理 (Container - Variable Manage - Double) */
CREATE PROCEDURE PROC_CNTR_VARIABLE_MANAGE_DOUBLE(IN In_Operation CHAR(80), IN In_Variable_Name CHAR(80), IN In_Variable_Sub_Name CHAR(80), INOUT Inout_Double_Value DOUBLE(16,4))
BEGIN

    DECLARE Inner_Operation CHAR(80);

    /* **************************************** */

    SET Inner_Operation = LOWER(In_Operation);

    /* **************************************** */

    IF Inner_Operation = "create" THEN
        /* Recursive Calling This Store Procedure : Requirement : SET @@max_sp_recursion_depth = 1 or N */
        CALL PROC_CNTR_VARIABLE_MANAGE_DOUBLE("Delete", In_Variable_Name, In_Variable_Sub_Name, Inout_Double_Value);
        /* ************************************ */
        INSERT INTO container(cntr_class, cntr_name, cntr_sub_name, cntr_double_value) VALUES ("Variable", In_Variable_Name, In_Variable_Sub_Name, Inout_Double_Value);
    ELSEIF Inner_Operation = "read" THEN
        SET Inout_Double_Value = (SELECT cntr_double_value FROM container WHERE cntr_class = "Variable" AND cntr_name = In_Variable_Name AND cntr_sub_name = In_Variable_Sub_Name);
    ELSEIF Inner_Operation = "rewrite" THEN
        UPDATE container SET cntr_double_value = Inout_Double_Value WHERE cntr_class = "Variable" AND cntr_name = In_Variable_Name AND cntr_sub_name = In_Variable_Sub_Name;
    ELSEIF Inner_Operation = "delete" THEN
        DELETE FROM container WHERE cntr_class = "Variable" AND cntr_name = In_Variable_Name AND cntr_sub_name = In_Variable_Sub_Name;
    ELSE
        SET @Error = "Container - Variable Manage - Double : Invalid Operation.";
    END IF;

END//

/* -------------------------------------------------- */
/* Container Math Procedure */

/* 删除存储过程: 基于容器之数学 - 数据切片清除 (Container Math - Date Slice Drop) */
DROP PROCEDURE IF EXISTS PROC_CNTR_MATH_DATA_SLICE_DROP//

/* 创建存储过程: 基于容器之数学 - 数据切片清除 (Container Math - Date Slice Drop) */
CREATE PROCEDURE PROC_CNTR_MATH_DATA_SLICE_DROP(IN In_Table_Name CHAR(80))
BEGIN

    DELETE FROM container WHERE cntr_class = "Data_Slice_Bgn_Idx" AND cntr_name = In_Table_Name;
    DELETE FROM container WHERE cntr_class = "Data_Slice_End_Idx" AND cntr_name = In_Table_Name;

END//

/* 删除存储过程: 基于容器之数学 - 数据索引切片 (Container Math - Date Index Slice) */
DROP PROCEDURE IF EXISTS PROC_CNTR_MATH_DATA_INDEX_SLICE//

/* 创建存储过程: 基于容器之数学 - 数据索引切片 (Container Math - Date Index Slice) */
CREATE PROCEDURE PROC_CNTR_MATH_DATA_INDEX_SLICE(IN In_Table_Name CHAR(80), IN In_Num_of_Slice INT(8))
BEGIN

    /* Requirement : MySQL 5.5+ */

    /* **************************************** */

    DECLARE Inner_Table_Num_of_Index BIGINT(12);
    DECLARE Inner_Table_Num_of_Index_Per_Slice BIGINT(12);
    DECLARE Inner_Cycle INT(8);
    DECLARE Inner_Bgn_Index INT(8); /* Inner Variable : Begin Index */
    DECLARE Inner_End_Index INT(8); /* Inner Variable : Ends Index */

    /* **************************************** */

    /* Calling Other Procedure : Drop Data Slice */
    CALL PROC_CNTR_MATH_DATA_SLICE_DROP(In_Table_Name);

    /* **************************************** */

    /*
     * Prepare SQL Syntax : Calculate The Table Number of Index.
     *
     * SELECT COUNT(*) INTO @Session_Result FROM In_Table_Name;
     */
    SET @Session_Table_Num_of_Index = CONCAT("SELECT COUNT(*) INTO @Session_Result FROM ",In_Table_Name,";");

    /* Prepared Statements */
    PREPARE Stmt FROM @Session_Table_Num_of_Index;
    EXECUTE Stmt;
    DEALLOCATE PREPARE Stmt;

    /* Variable Assignment */
    SET Inner_Table_Num_of_Index = @Session_Result;

    /* **************************************** */

    /* CEILING() 向上取整 : 将一个数值向上取整到最接近的整数 */
    /* FLOOR() 向下取整 : 将一个数值向下取整到最接近的整数 */
    
    /* Calculate Number of Index Per Data Slice */
    SET Inner_Table_Num_of_Index_Per_Slice = FLOOR(Inner_Table_Num_of_Index / In_Num_of_Slice); /* 如果总索引数不能被整除则总会漏掉几个索引 */

    /* **************************************** */

    SET Inner_Cycle = 1;
    SET Inner_Bgn_Index = 1;
    SET Inner_End_Index = Inner_Table_Num_of_Index_Per_Slice;
    WHILE Inner_Cycle <= In_Num_of_Slice DO
        INSERT INTO container(cntr_class, cntr_name, cntr_number, cntr_purpose, cntr_bigint_value)
            VALUES ("Data_Slice_Bgn_Idx", In_Table_Name, Inner_Cycle, "Begin_Index", Inner_Bgn_Index); /* Insert : Start Index */
        INSERT INTO container(cntr_class, cntr_name, cntr_number, cntr_purpose, cntr_bigint_value)
            VALUES ("Data_Slice_End_Idx", In_Table_Name, Inner_Cycle, "Ends_Index", Inner_End_Index); /* Insert : Ends Index */
        /* ************************************ */
        SET Inner_Cycle = (Inner_Cycle + 1);
        SET Inner_Bgn_Index = (Inner_Bgn_Index + Inner_Table_Num_of_Index_Per_Slice); /* Setting : The Begin Index + The Table Number of Index Per Slice */
        SET Inner_End_Index = (Inner_End_Index + Inner_Table_Num_of_Index_Per_Slice); /* Setting : The Ends Index + The Table Number of Index Per Slice */
    END WHILE;

END//

/* -------------------------------------------------- */
/* Container Array Procedure */

/* 删除存储过程: 容器数组 - 删除数组 (Container Array - Drop) */
DROP PROCEDURE IF EXISTS PROC_CNTR_ARRAY_DROP//

/* 创建存储过程: 容器数组 - 删除数组 (Container Array - Drop) */
CREATE PROCEDURE PROC_CNTR_ARRAY_DROP(IN In_Array_Name CHAR(80))
BEGIN
    
    DELETE FROM container WHERE cntr_class = "Array" AND cntr_name = In_Array_Name;

END//

/* 删除存储过程: 容器数组 - 初始化数组 (Container Array - Initialize Array) */
DROP PROCEDURE IF EXISTS PROC_CNTR_ARRAY_INIT//

/* 创建存储过程: 容器数组 - 初始化数组 (Container Array - Initialize Array) */
CREATE PROCEDURE PROC_CNTR_ARRAY_INIT(IN In_Array_Name CHAR(80), IN In_Array_Length BIGINT(12))
BEGIN

    /*
     * Requirement : MySQL 5.5+.
     *
     * If In_Array_Length = 0 : Just delete possible arrays with the same name without allocating any memory.
     * If In_Array_Length = 1 : Delete possible arrays with the same name and allocate memory for 1 element.
     * If In_Array_Length = 2 : Delete possible arrays with the same name and allocate memory for 2 element.
     * ...
     * If In_Array_Length = N : Delete possible arrays with the same name and allocate memory for N element.
     */

    /* **************************************** */

    DECLARE Inner_Cycle BIGINT(12);

    /* **************************************** */

    /* Calling Other Store Procedure */
    CALL PROC_CNTR_ARRAY_DROP(In_Array_Name);

    /* **************************************** */

    SET Inner_Cycle = 1;
    WHILE Inner_Cycle <= In_Array_Length DO
        INSERT INTO container(cntr_class, cntr_name, cntr_purpose, cntr_number) VALUES ("Array", In_Array_Name, "Index", Inner_Cycle);
        SET Inner_Cycle = (Inner_Cycle + 1);
    END WHILE;

END//

/* 删除存储过程: 容器数组 - 数组长度 (Container Array - Length) */
DROP PROCEDURE IF EXISTS PROC_CNTR_ARRAY_LENGTH//

/* 创建存储过程: 容器数组 - 数组长度 (Container Array - Length) */
CREATE PROCEDURE PROC_CNTR_ARRAY_LENGTH(IN In_Array_Name CHAR(80), OUT Out_Array_Length BIGINT(12))
BEGIN

    SET Out_Array_Length =
        (SELECT COUNT(*) FROM container WHERE cntr_class = "Array" AND cntr_name = In_Array_Name); 

END//

/* 删除存储过程: 容器数组 - 末尾添加小数 (Container Array - Append Decimal) */
DROP PROCEDURE IF EXISTS PROC_CNTR_ARRAY_APPEND_DECIMAL//

/* 创建存储过程: 容器数组 - 末尾添加小数 (Container Array - Append Decimal) */
CREATE PROCEDURE PROC_CNTR_ARRAY_APPEND_DECIMAL(IN In_Array_Name CHAR(80), IN In_Double_Value DOUBLE(16,4))
BEGIN

    DECLARE Inner_Array_Length BIGINT(12);

    /* **************************************** */

    /* Calling Other Store Procedure */
    CALL PROC_CNTR_ARRAY_LENGTH(In_Array_Name, Inner_Array_Length);

    /* **************************************** */

    INSERT INTO container(cntr_class, cntr_name, cntr_purpose, cntr_number, cntr_double_value)
        VALUES ("Array", In_Array_Name, "Index", (Inner_Array_Length + 1), In_Double_Value);

END//

/* 删除存储过程: 容器数组 - 通过索引删除元素 (Container Array - Delete Element By Index) */
DROP PROCEDURE IF EXISTS PROC_CNTR_ARRAY_DELETE_ELEMENT_BY_INDEX//

/* 创建存储过程: 容器数组 - 通过索引删除元素 (Container Array - Delete Element By Index) */
CREATE PROCEDURE PROC_CNTR_ARRAY_DELETE_ELEMENT_BY_INDEX(IN In_Array_Name CHAR(80), IN In_Array_Index BIGINT(12))
BEGIN

    DECLARE Inner_Array_Length BIGINT(12);
    DECLARE Inner_Cycle BIGINT(12);

    /* **************************************** */

    /* Calling Other Store Procedure */
    CALL PROC_CNTR_ARRAY_LENGTH(In_Array_Name, Inner_Array_Length);

    /* **************************************** */

    /* Delete elements through index and reset index */
    IF In_Array_Index < Inner_Array_Length THEN
        DELETE FROM container WHERE cntr_class = "Array" AND cntr_name = In_Array_Name AND cntr_number = In_Array_Index;
        /* ************************************ */
        SET Inner_Cycle = (In_Array_Index + 1); /* If the index of the deleted element is 1, the loop start index starts from 2 */
        WHILE Inner_Cycle <= Inner_Array_Length DO
            UPDATE container SET cntr_number = (Inner_Cycle - 1) WHERE cntr_class = "Array" AND cntr_name = In_Array_Name AND cntr_number = Inner_Cycle;
            SET Inner_Cycle = (Inner_Cycle + 1);
        END WHILE;
    ELSEIF In_Array_Index = Inner_Array_Length THEN
        DELETE FROM container WHERE cntr_class = "Array" AND cntr_name = In_Array_Name AND cntr_number = In_Array_Index;
    ELSE
        SET @Error = "Container Array - Delete Element By Index : Invalid Array Index.";
    END IF;

END//

/* 删除存储过程: 容器数组 - 输出数组 (Container Array - Print) */
DROP PROCEDURE IF EXISTS PROC_CNTR_ARRAY_PRINT//

/* 创建存储过程: 容器数组 - 输出数组 (Container Array - Print) */
CREATE PROCEDURE PROC_CNTR_ARRAY_PRINT(IN In_Array_Name CHAR(80))
BEGIN
    
    SELECT cntr_number as Array_Index, cntr_double_value as Array_Value FROM container WHERE cntr_class = "Array" AND cntr_name = In_Array_Name;

END//

/* 删除存储过程: 容器数组 - 数组求和 (Container Array - Sum) */
DROP PROCEDURE IF EXISTS PROC_CNTR_ARRAY_SUM//

/* 创建存储过程: 容器数组 - 数组求和 (Container Array - Sum) */
CREATE PROCEDURE PROC_CNTR_ARRAY_SUM(IN In_Array_Name CHAR(80))
BEGIN
    
    SELECT SUM(cntr_double_value) FROM container WHERE cntr_class = "Array" AND cntr_name = In_Array_Name;

END//

-- 改回默认结束符。
DELIMITER ;

/* -------------------------------------------------- */
/* EOF */
