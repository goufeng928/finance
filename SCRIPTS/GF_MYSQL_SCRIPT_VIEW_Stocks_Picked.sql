/*
 * GF_MYSQL_SCRIPT_VIEW_Stocks_Picked.sql
 * Create by GF 2024-10-14 17:46
 */

CREATE OR REPLACE VIEW view_stocks_picked as
SELECT
    f_source,
    f_site,
    f_time_gl,
    f_adjusted,
    f_date,
    f_code,
    f_open,
    f_high,
    f_low,
    f_close,
    f_change,
    f_chg_pct,
    f_volume,
    (SELECT MAX(f_date) FROM dataset_stocks) AS f_latest_date,
    DATEDIFF(CURDATE(), f_date) AS f_date_distance
FROM
    dataset_stocks_picked
WHERE
    f_source   = "Tushare"     AND
    f_site     = "tushare.pro" AND
    f_time_gl  = "Daily"       AND
    f_adjusted = "不复权"
ORDER BY
    f_date DESC
;
