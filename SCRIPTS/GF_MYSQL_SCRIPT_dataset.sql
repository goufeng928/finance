-- source /home/goufeng/GF_MYSQL_SCRIPT_dataset.sql

-- Create by GF 2023-06-20

/* ################################################## */

/* Delete Data Table (Only Delete if it Exists) */
DROP TABLE IF EXISTS dataset_api_accounts;

/* Create Data Table (Only if it does not Exist) */
CREATE TABLE IF NOT EXISTS dataset_api_accounts(f_id       INT NOT NULL AUTO_INCREMENT,
                                                f_source   VARCHAR(32),  -- 来源 (Source)
                                                f_site     VARCHAR(32),  -- 地址 (Site)
                                                f_phone    VARCHAR(16),  -- 电话 (Phone)
                                                f_email    VARCHAR(32),  -- 邮箱 (Email)
                                                f_username VARCHAR(32),  -- 用户名 (Username)
                                                f_password VARCHAR(32),  -- 密码 (Password)
                                                f_key      VARCHAR(256), -- 密钥 (Key / Secret Key)
                                                f_pub_key  VARCHAR(256), -- 公钥 (Public Key)
                                                f_pri_key  VARCHAR(256), -- 私钥 (Private Key)
                                                PRIMARY KEY (f_id));

/* .................................................. */

GRANT SELECT ON dataset.dataset_api_accounts TO 'goufeng'@'%' WITH GRANT OPTION;

/* ################################################## */

/* Delete Data Table (Only Delete if it Exists) */
DROP TABLE IF EXISTS dataset_calendar;

/* Create Data Table (Only if it does not Exist) */
CREATE TABLE IF NOT EXISTS dataset_calendar(f_id       INT NOT NULL AUTO_INCREMENT,
                                            f_year     INT,  -- 年 (Year)
                                            f_month    INT,  -- 月 (Month)
                                            f_date     DATE, -- 日期 (Date)
                                            f_week     INT,  -- 周 (Site)
                                            f_holiday  INT,  -- 休息日 (Holiday)
                                            f_festival INT,  -- 节日 (Festival)
                                            f_trade    INT,  -- 交易日: 金融 (Trading Day: Finance)
                                            PRIMARY KEY (f_id));

/* .................................................. */

GRANT SELECT ON dataset.dataset_calendar TO 'goufeng'@'%' WITH GRANT OPTION;

/* ################################################## */
/* EOF Signed by GF*/
