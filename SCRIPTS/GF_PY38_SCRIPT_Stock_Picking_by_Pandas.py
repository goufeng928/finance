# GF_PY38_SCRIPT_Stock_Picking_by_Pandas.py
# Environment: Python 3.8.0
# Create By GF 2024-04-13 20:00

import os

# Get Specific System Environment Variables.
dbHost:str       = os.environ.get("GF_DB_HOST")
dbUser:str       = os.environ.get("GF_DB_USER")
dbPassword:str   = os.environ.get("GF_DB_PASSWORD")
LIBRARY_PATH:str = os.environ.get("GF_LIBRARY_PATH")

import sys

# Add "Library Path" to The Working Path of Python.
sys.path.append(LIBRARY_PATH)

# ##################################################

import datetime
# ..................................................
import pandas
import sqlalchemy
# ..................................................
from GF_PY38_FUNCTION_Pandas_1_x_Stock import *

# ##################################################

COMMAND_LINE_ARGS = sys.argv

print("[Message] Stock Picking by Pandas -> Starting......")

# >>> Splicing SQLAlchemy's MySQL URI <<<
# '''''''''''''''''''''''''''''''''''''''

sqlalchemyEngine = sqlalchemy.create_engine(f"mysql+pymysql://{dbUser}:{dbPassword}@{dbHost}/dataset")

# >>> Use Pandas 1.4.1 + SQLAlchemy 1.x to Read The Basic Information Data Table <<<
# ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

PandasDF = pandas.read_sql_query(
    "SELECT f_confused, f_standard FROM dataset_columns_name_mapping WHERE f_major = 'Finance' AND f_use_to = 'CSV';",
    sqlalchemyEngine
)
# ..................................................
columnsNameMappingToCSV = {key: val for key, val in zip(PandasDF["f_confused"], PandasDF["f_standard"])}
# -------------------------------------------------
PandasDF = pandas.read_sql_query(
    "SELECT f_confused, f_standard FROM dataset_columns_name_mapping WHERE f_major = 'Finance' AND f_use_to = 'Database';",
    sqlalchemyEngine
)
# ..................................................
columnsNameMappingToDatabase = {key: val for key, val in zip(PandasDF["f_confused"], PandasDF["f_standard"])}

# >>> Define Time Range <<<
# '''''''''''''''''''''''''

dateX = datetime.date(2024, 12, 12)
# Caution: In Python 3.8, "sys.argv", The First Parameter on The Command Line is The Script Name.
if (len(COMMAND_LINE_ARGS) > 1): dateX = COMMAND_LINE_ARGS[1]
if (type(dateX) == str): dateX = datetime.datetime.strptime(dateX, "%Y-%m-%d")
# ..................................................
dateA = dateX - datetime.timedelta(days=90)
dateB = dateX
# ..................................................
strDateA = dateA.strftime("%Y-%m-%d")
strDateB = dateB.strftime("%Y-%m-%d")

# >>> Use Pandas 1.4.1 + SQLAlchemy 1.x to Read The Stocks Data Table Within The Range <<<
# ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

PandasDF = pandas.read_sql_query(
    "SELECT * FROM dataset_stocks WHERE '%s' <= f_date AND f_date <= '%s';" % (strDateA, strDateB),
    sqlalchemyEngine
)
PandasDF["..."] = str("...")
# ..................................................
PandasDF = PandasDF.drop_duplicates(subset=["f_date", "f_code"]).reset_index(drop=True)
# ..................................................
PandasDF = PandasDF.rename(columns=columnsNameMappingToCSV)
# ..................................................
print(PandasDF[["source", "date", "code", "open", "high", "...", "low", "close", "pre_close", "chg_pct", "volume"]])

print('')

# >>> Use Pandas Filter The Top 100 Targets in Terms of Transaction Amount <<<
# ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

PandasDF_TOP100 = PandasDF
PandasDF_TOP100 = PandasDF_TOP100[PandasDF_TOP100["date"] == dateX.date()]
PandasDF_TOP100 = PandasDF_TOP100[PandasDF_TOP100["change"] > 0]
PandasDF_TOP100 = PandasDF_TOP100.sort_values("amount", ascending=False)
PandasDF_TOP100 = PandasDF_TOP100.head(100)
PandasDF_TOP100 = PandasDF_TOP100[["date", "code"]]
print(PandasDF_TOP100)

print('')

# >>> Use Pandas Calculate The Indicators of Targets <<<
# ''''''''''''''''''''''''''''''''''''''''''''''''''''''

PandasDF_Daily  = Pandas_1_x_Stock_Identify_DataFrame_as_Daily(Input=PandasDF) # Daily's Data.
PandasDF_Weekly = Pandas_1_x_Stock_Aggregate_DataFrame_Daily_to_Weekly(Input=PandasDF_Daily) # Weekly's Data.
# ..................................................
PandasDF_Daily  = Pandas_1_x_Stock_Calculate_Indicator_FinInd(PandasDF_Daily, COL_NAME_SUFFIX="dy")
PandasDF_Daily  = Pandas_1_x_Stock_Calculate_Indicator_EtgInd(PandasDF_Daily, COL_NAME_SUFFIX="dy")
# ..................................................
PandasDF_Weekly = Pandas_1_x_Stock_Calculate_Indicator_FinInd(PandasDF_Weekly, COL_NAME_SUFFIX="wk")
PandasDF_Weekly = Pandas_1_x_Stock_Calculate_Indicator_EtgInd(PandasDF_Weekly, COL_NAME_SUFFIX="wk")
# ..................................................
# Preparation Before Merging Data, Merge Data Through Fields ("code", "year", "week_num").
PandasDF_Daily["year(dy)"]     = PandasDF_Daily["date"].dt.year
PandasDF_Daily["week_num(dy)"] = PandasDF_Daily["date"].dt.strftime("%U").astype("int64")
PandasDF_Daily["year"]         = PandasDF_Daily["year(dy)"]
PandasDF_Daily["week_num"]     = PandasDF_Daily["week_num(dy)"]
# ..................................................
PandasDF_Weekly = PandasDF_Weekly.drop("date", axis=1) # -> To Avoid Confusion with Daily Data, The "date" Column is Deleted.
# ..................................................
PandasDF_Merged = pandas.merge(left=PandasDF_Daily, right=PandasDF_Weekly, on=["code", "year", "week_num"])
PandasDF_Merged["..."] = str("...")
print(
    PandasDF_Merged[
        ["source", "date", "code", "open(dy)", "open(wk)", "...", "close(dy)", "close(wk)", "ma5(dy)", "ma5(wk)", "ma10(dy)", "ma10(wk)"]
    ]
)

print('')

# >>> Use Pandas Filter The Final Target <<<
# ''''''''''''''''''''''''''''''''''''''''''

PandasDF_Filter = PandasDF_Merged[PandasDF_Merged["code"].isin(PandasDF_TOP100["code"])]
PandasDF_Result = Pandas_1_x_Stock_Conditional_Picking(PandasDF_Filter)
print(
    PandasDF_Result[
        ["source", "date", "code", "open(dy)", "open(wk)", "...", "close(dy)", "close(wk)", "ma5(dy)", "ma5(wk)", "ma10(dy)", "ma10(wk)"]
    ]
)

# EOF Signed by GF.
