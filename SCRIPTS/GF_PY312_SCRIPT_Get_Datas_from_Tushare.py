# GF_PY312_SCRIPT_Get_Datas_from_Tushare.py
# Environment: Python 3.12.0
# Create By GF 2024-04-13 20:00

import os

# Get Specific System Environment Variables.
dbHost:str       = os.environ.get("GF_DB_HOST")
dbUser:str       = os.environ.get("GF_DB_USER")
dbPassword:str   = os.environ.get("GF_DB_PASSWORD")
LIBRARY_PATH:str = os.environ.get("GF_LIBRARY_PATH")

import sys

# Add "Library Path" to The Working Path of Python.
sys.path.append(LIBRARY_PATH)

# ##################################################

import pandas
import pymysql
import sqlalchemy
import tushare

# ##################################################

COMMAND_LINE_ARGS = sys.argv

print("[Message] Get Datas from Tushare -> Starting......")

# >>> Use PyMySQL 1.x to Read The API Key <<<
# '''''''''''''''''''''''''''''''''''''''''''

pymysqlConnection = pymysql.connect(host = dbHost,
                                    user = dbUser,
                                    password = dbPassword,
                                    database = 'dataset',
                                    charset = 'utf8mb4',
                                    cursorclass = pymysql.cursors.DictCursor)
pymysqlCursor = pymysqlConnection.cursor()

sqlStatment = "SELECT f_key FROM dataset_api_accounts WHERE f_site = 'tushare.pro' AND f_id = 1;"
pymysqlCursor.execute(sqlStatment)
pymysqlQueryResult = pymysqlCursor.fetchone()
Tushare_API_Key = pymysqlQueryResult["f_key"]

pymysqlConnection.close()

# >>> Use Pandas 2.x to Read The Columns Name Mapping Table <<<
# '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

csvColMapPath = LIBRARY_PATH + "\\Columns-Name-Mapping.csv"

df = pandas.read_csv(csvColMapPath)

dfColMapToCSV   = df[(df["major"] == "finance") & (df["use_to"] == "csv")]
dictColMapToCSV = {}
dfColMapToCSV.apply(lambda row: dictColMapToCSV.update({row["confused"]:row["standard"]}), axis=1)

dfColMapToDB   = df[(df["major"] == "finance") & (df["use_to"] == "database")]
dictColMapToDB = {}
dfColMapToDB.apply(lambda row: dictColMapToDB.update({row["confused"]:row["standard"]}), axis=1)

df = None

# ##################################################

# >>> Related Configurations of Tushare <<<
# '''''''''''''''''''''''''''''''''''''''''

configuredAPI = tushare.pro_api(Tushare_API_Key)

stocksAdjusted:str = None # 前复权 "qfq" / 后复权 "hfq" / 不复权 None

# ##################################################

datesList:list = []
# Caution: In Python 3.12, Slice "list", Including Head Index but not Tail Index.
if (len(COMMAND_LINE_ARGS) > 1): datesList = COMMAND_LINE_ARGS[1:len(COMMAND_LINE_ARGS)]

Received:int = 0
Restricted:int = 200000
i:int = 0

while ((Received < Restricted) and (i < len(datesList))):
    
    # >>> Code for Database Data Validation: Pandas 1.4.1 + SQLAlchemy 1.x <<<
    # ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    sqlalchemyEngine = sqlalchemy.create_engine(f"mysql+pymysql://{dbUser}:{dbPassword}@{dbHost}/dataset")

    SQL_Statement:str = """
    SELECT
        *
    FROM
        dataset_stocks_log
    WHERE
        f_source   = 'Tushare'     AND
        f_site     = 'tushare.pro' AND
        f_time_gl  = 'Daily'       AND
        f_adjusted = '不复权'      AND
        f_date     = '%s'          AND
        f_insert   > 0;
    """  % (datesList[i])
    
    PandasDF_dataset_stocks_log = pandas.read_sql_query(SQL_Statement, sqlalchemyEngine)

    Insert_History = PandasDF_dataset_stocks_log["f_insert"].count()
    
    # ..............................................
    
    if (Insert_History > 0):
    
        print("[Caution] Get Datas from Tushare -> Target: %s, Already has %s Record." % (datesList[i], Insert_History))
    
        i = i + 1
    
        continue

    # >>> Code for data acquisition: Tushare 1.4.17 <<<
    # '''''''''''''''''''''''''''''''''''''''''''''''''

    dateX = datesList[i].replace('-', '')

    PandasDF_API = configuredAPI.daily(adj = stocksAdjusted, start_date = dateX, end_date = dateX)
    
    New_Records_Number = PandasDF_API["ts_code"].count()
    
    # ..............................................
    
    if ((Received + New_Records_Number) > Restricted):
    
        print("[Caution] %d + %d Will Exceed %d, Reception has been Stopped." % (Received, New_Records_Number, Restricted))
    
        break
    
    # >>> Code for data correction section: Pandas 2.x <<<
    # ''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    PandasDF_API = PandasDF_API.rename(columns = dictColMapToCSV) # Pandas' column names are mapped to type "CSV".
    
    PandasDF_API['date']     = pandas.to_datetime(PandasDF_API['date'], format='%Y%m%d')
    PandasDF_API["adjusted"] = "不复权" # 添加 "复权(Adjusted)" 列。
    PandasDF_API["source"]   = "Tushare"
    PandasDF_API["site"]     = "tushare.pro"
    PandasDF_API["time_gl"]  = "Daily"

    # >>> Code for writing to the database: Pandas 2.x + SQLAlchemy <<<
    # '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    PandasDF_API = PandasDF_API.rename(columns = dictColMapToDB) # Pandas' column names are mapped to type "Database".
    
    # Write the obtained data to the corresponding table in the database (将获取到的数据写入数据库对应的表)。
    
    PandasDF_API.to_sql('dataset_stocks', con=sqlalchemyEngine, index=False, if_exists='append', method='multi')
    
    # Write data to the log table in the database (向数据库中的日志表写入数据)。
    
    PandasDF_Log = pandas.DataFrame({"f_source"   : ["Tushare"],
                                     "f_site"     : ["tushare.pro"],
                                     "f_time_gl"  : ["Daily"],
                                     "f_adjusted" : ["不复权"],
                                     "f_date"     : [datesList[i]],
                                     "f_insert"   : [New_Records_Number]})
    PandasDF_Log.to_sql('dataset_stocks_log', con=sqlalchemyEngine, index=False, if_exists='append', method='multi')
    
    print("[Message] Get Datas from Tushare -> Target: %s, Received: %d, Restrictions: %d." % (datesList[i], (Received + New_Records_Number), Restricted))

    # ..............................................

    Received = Received + New_Records_Number
    
    i = i + 1

# EOF Signed by GF.
