/*
 * GF_MYSQL_SCRIPT_VIEW_Stocks_Overall.sql
 * Create by GF 2024-10-14 17:46
 */

CREATE OR REPLACE VIEW view_stocks_overall as
SELECT
    (SELECT MAX(f_date) FROM `dataset_stocks`) AS f_date,
    "Number of Stocks" AS f_indicator,
    (SELECT COUNT(*) FROM `dataset_stocks` WHERE f_date = (SELECT MAX(f_date) FROM `dataset_stocks`)) AS f_value

UNION

SELECT
    (SELECT MAX(f_date) FROM `dataset_stocks`) AS f_date,
    "Number of Rise" AS f_indicator,
    (SELECT COUNT(*) FROM `dataset_stocks` WHERE f_date = (SELECT MAX(f_date) FROM `dataset_stocks`) AND f_change > 0) AS f_value

UNION

SELECT
    (SELECT MAX(f_date) FROM `dataset_stocks`) AS f_date,
    "Number of Fall" AS f_indicator,
    (SELECT COUNT(*) FROM `dataset_stocks` WHERE f_date = (SELECT MAX(f_date) FROM `dataset_stocks`) AND f_change < 0) AS f_value

UNION

SELECT
    (SELECT MAX(f_date) FROM `dataset_stocks`) AS f_date,
    "Number of Flat" AS f_indicator,
    (SELECT COUNT(*) FROM `dataset_stocks` WHERE f_date = (SELECT MAX(f_date) FROM `dataset_stocks`) AND f_change = 0) AS f_value

;
