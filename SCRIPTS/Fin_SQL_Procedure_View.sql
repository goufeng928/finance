-- Fin_SQL_Procedure_View.sql

-- Create By GF 2023-09-05 15:21

/* -------------------------------------------------- */

/*
 * #######################################################################################
 * ##                                                                                   ##
 * ## Due to configuration reasons, MySQL does not distinguish between Upper and Lower. ##
 * ##                                                                                   ##
 * ## Beware of the parameter names of functions being the same as table field names.   ##
 * ##                                                                                   ##
 * #######################################################################################
 */

/* -------------------------------------------------- */

/*
 * Requirement : Fin_SQL_Procedure_Container.sql
 */

/* -------------------------------------------------- */

-- 结束符修改。
DELIMITER //

/* -------------------------------------------------- */
/* Finance View Calculate Procedure */

/* 删除存储过程: 视图 - 金融数据计算 (View - Finance Data Calculate) */
DROP PROCEDURE IF EXISTS PROC_VIEW_FIN_DATA_CALCULATE//

/* 创建存储过程: 视图 - 金融数据计算 (View - Finance Data Calculate) */
CREATE PROCEDURE PROC_VIEW_FIN_DATA_CALCULATE(IN In_Table_Name CHAR(80))
BEGIN

    /*
     * Requirement : MySQL 8.0+ (OLAP : Online Anallytical Processing).
     */
    
    /* 声明局部变量 : 保存视图(View)名称的变量 */
    DECLARE Inner_View_Name CHAR(80);

    /* **************************************** */

    /* 拼接字符串 : View + 表名 */
    SET Inner_View_Name = CONCAT("view_",In_Table_Name);

    /* 创建视图语句 - 以字符串的形式保存到会话变量 */
    SET @Session_View_Fin_Calculate = CONCAT("
        CREATE OR REPLACE VIEW ",Inner_View_Name," as
        SELECT
            _index,

            _date, _open, _high, _low, _close, _rise_fall_amt, _rise_fall_rate, _volume,

            SUM_FUTURE_20_RISE_FALL,

            SMA5, SMA10, SMA20,

            MACD_DIF, MACD_DEA, MACD_STICK,

            RSI1, RSI2, RSI3,

            FUNC_FIN_EST_2_LINE_CROSS(_index, 'Up', 'SMA5_CU_10', SMA5, SMA10) as SMA5_CU_SMA10,
            FUNC_FIN_EST_2_LINE_CROSS(_index, 'Up', 'SMA5_CU_20', SMA5, SMA20) as SMA5_CU_SMA20,
            FUNC_FIN_EST_2_LINE_CROSS(_index, 'Up', 'SMA10_CU_20', SMA10, SMA20) as SMA10_CU_SMA20,
            FUNC_FIN_EST_2_LINE_CROSS(_index, 'Down', 'SMA5_CD_10', SMA5, SMA10) as SMA5_CD_SMA10,
            FUNC_FIN_EST_2_LINE_CROSS(_index, 'Down', 'SMA5_CD_20', SMA5, SMA20) as SMA5_CD_SMA20,
            FUNC_FIN_EST_2_LINE_CROSS(_index, 'Down', 'SMA10_CD_20', SMA10, SMA20) as SMA10_CD_SMA20,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Larger', SMA5, SMA10) as SMA5_A_SMA10,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Larger', SMA5, SMA20) as SMA5_A_SMA20,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Larger', SMA10, SMA20) as SMA10_A_SMA20,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Smaller', SMA5, SMA10) as SMA5_B_SMA10,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Smaller', SMA5, SMA20) as SMA5_B_SMA20,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Smaller', SMA10, SMA20) as SMA10_B_SMA20,
            
            FUNC_FIN_EST_2_LINE_CROSS(_index, 'Up', 'MACD_DIF_CU_DEA', MACD_DIF, MACD_DEA) as MACD_DIF_CU_DEA,
            FUNC_FIN_EST_2_LINE_CROSS(_index, 'Down', 'MACD_DIF_CD_DEA', MACD_DIF, MACD_DEA) as MACD_DIF_CD_DEA,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Larger', MACD_DIF, MACD_DEA) as MACD_DIF_A_DEA,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Smaller', MACD_DIF, MACD_DEA) as MACD_DIF_B_DEA,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Larger', MACD_DIF, 0) as MACD_DIF_A_0,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Larger', MACD_DEA, 0) as MACD_DEA_A_0,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Smaller', MACD_DIF, 0) as MACD_DIF_B_0,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Smaller', MACD_DEA, 0) as MACD_DEA_B_0,
            
            FUNC_FIN_EST_2_LINE_CROSS(_index, 'Up', 'RSI1_CU_RSI2', RSI1, RSI2) as RSI1_CU_RSI2,
            FUNC_FIN_EST_2_LINE_CROSS(_index, 'Down', 'RSI1_CD_RSI2', RSI1, RSI2) as RSI1_CD_RSI2,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Larger', RSI1, RSI2) as RSI1_A_RSI2,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Smaller', RSI1, RSI2) as RSI1_B_RSI2,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Larger', RSI1, 50) as RSI1_A_50,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Larger', RSI2, 50) as RSI2_A_50,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Smaller', RSI1, 50) as RSI1_B_50,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Smaller', RSI2, 50) as RSI2_B_50,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Larger', RSI1, 70) as RSI1_A_70,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Larger', RSI2, 70) as RSI2_A_70,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Smaller', RSI1, 70) as RSI1_B_70,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Smaller', RSI2, 70) as RSI2_B_70,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Larger', RSI1, 30) as RSI1_A_30,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Larger', RSI2, 30) as RSI2_A_30,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Smaller', RSI1, 30) as RSI1_B_30,
            FUNC_FIN_EST_2_VALUE_COMPARE_SIZE('Smaller', RSI2, 30) as RSI2_B_30
        FROM
        
        ( /* Driver_2 : Calling Custom Finance Calculate Function */
        
        SELECT
            _index,

            _date, _open, _high, _low, _close, _rise_fall_amt, _rise_fall_rate, _volume,

            SUM_FUTURE_20_RISE_FALL,

            FUNC_FIN_SMA(_index, 5, _close) as SMA5,
            FUNC_FIN_SMA(_index, 10, _close) as SMA10,
            FUNC_FIN_SMA(_index, 20, _close) as SMA20,
            FUNC_FIN_MACD_DIF(_index, 12, 26, _close) as MACD_DIF,
            FUNC_FIN_MACD_DEA(_index, 12, 26, _close) as MACD_DEA,
            FUNC_FIN_MACD_STICK(_index, 12, 26, _close) as MACD_STICK,
            FUNC_FIN_RSI(_index, 6, _close) as RSI1,
            FUNC_FIN_RSI(_index, 12, _close) as RSI2,
            FUNC_FIN_RSI(_index, 24, _close) as RSI3
        FROM
        
        ( /* Driver_1 : Filter & Reorder & Calling System Function */
        
        SELECT
            ROW_NUMBER() OVER(ORDER BY _date ASC) as _index, /* 为原表添加行号 */
            _date, _open, _high, _low, _close, _rise_fall_amt, _rise_fall_rate, _volume,
            SUM(_rise_fall_rate) OVER(ORDER BY _date ASC ROWS BETWEEN 1 FOLLOWING AND 20 FOLLOWING) as SUM_FUTURE_20_RISE_FALL
        FROM
            ",In_Table_Name,"
        WHERE
            _volume <> 0.0
        
        ) as Driver_1
        ) as Driver_2;");
    
    /* 预处理语句 - 创建视图语句 */
    PREPARE View_Fin_Calculate_Stmt FROM @Session_View_Fin_Calculate;
    EXECUTE View_Fin_Calculate_Stmt;
    DEALLOCATE PREPARE View_Fin_Calculate_Stmt;

END//

/* -------------------------------------------------- */
/* Data Test View Calculate Procedure */

/* 删除存储过程: 视图 - 数据切片视图清除 (View - Data Slice View Drop) */
DROP PROCEDURE IF EXISTS PROC_VIEW_DATA_SLICE_DROP//

/* 创建存储过程: 视图 - 数据切片视图清除 (View - Data Slice View Drop) */
CREATE PROCEDURE PROC_VIEW_DATA_SLICE_DROP(IN In_Table_Name CHAR(80))
BEGIN

    DELETE FROM container WHERE cntr_class = "View" AND cntr_name = In_Table_Name;

END//

/* 删除存储过程: 视图 - 数据切片视图创建 (View - Data Slice View Create) */
DROP PROCEDURE IF EXISTS PROC_VIEW_DATA_SLICE_CREATE//

/* 创建存储过程: 视图 - 数据切片视图创建 (View - Data Slice View Create) */
CREATE PROCEDURE PROC_VIEW_DATA_SLICE_CREATE(IN In_Table_Name CHAR(80), IN In_Num_of_Slice INT(8))
BEGIN

    /* Requirement : MySQL 5.5+ */

    /* **************************************** */

    DECLARE Inner_Table_Num_of_Rows BIGINT(12);
    DECLARE Inner_Table_Num_of_Rows_Per_Slice BIGINT(12);
    DECLARE Inner_Cycle INT(8);
    DECLARE Inner_Bgn_Line_Num INT(8); /* Inner Variable : Begin Line Number */
    DECLARE Inner_End_Line_Num INT(8); /* Inner Variable : Ends Line Number */

    /* **************************************** */

    /* Calling Other Store Procedure */
    CALL PROC_VIEW_DATA_SLICE_DROP(In_Table_Name);

    /* **************************************** */
    
    /*
     * Prepare SQL Syntax : Calculate The Table Number of Rows.
     *
     * SELECT COUNT(*) INTO @Session_Result FROM In_Table_Name;
     */
    SET @Session_Table_Num_of_Rows = CONCAT("SELECT COUNT(*) INTO @Session_Result FROM ",In_Table_Name,";");

    /* Prepared Statements */
    PREPARE Stmt FROM @Session_Table_Num_of_Rows;
    EXECUTE Stmt;
    DEALLOCATE PREPARE Stmt;

    /* Variable Assignment */
    SET Inner_Table_Num_of_Rows = @Session_Result;

    /* **************************************** */
    
    /* CEILING() 向上取整 : 将一个数值向上取整到最接近的整数 */
    /* FLOOR() 向下取整 : 将一个数值向下取整到最接近的整数 */
    
    /* Calculate Number of Rows Per Data Slice */
    SET Inner_Table_Num_of_Rows_Per_Slice = FLOOR(Inner_Table_Num_of_Rows / In_Num_of_Slice); /* 如果总行数不能被整除则总会漏掉几行 */

    /* **************************************** */

    SET Inner_Cycle = 1;
    SET Inner_Bgn_Line_Num = 0;
    SET Inner_End_Line_Num = Inner_Table_Num_of_Rows_Per_Slice;
    WHILE Inner_Cycle <= In_Num_of_Slice DO
        /*
         * Prepare SQL Syntax : Create Data Slice View.
         *
         * SCREATE OR REPLACE VIEW view_slice_In_Table_Name_1 as
         * SELECT * FROM In_Table_Name LIMIT 0, 2000;
         */
        SET @Session_View_Slice_Prepare_SQL = CONCAT("CREATE OR REPLACE VIEW view_slice_",In_Table_Name,"_",CONVERT(Inner_Cycle, CHAR)," as
                                                      SELECT * FROM ",In_Table_Name," LIMIT ",Inner_Bgn_Line_Num,", ",Inner_End_Line_Num,";");
        /* Prepared Statements */
        PREPARE Stmt FROM @Session_View_Slice_Prepare_SQL;
        EXECUTE Stmt;
        DEALLOCATE PREPARE Stmt;
        /* ************************************ */
        INSERT INTO container(cntr_class, cntr_name, cntr_number) VALUES ("View", In_Table_Name, Inner_Cycle);
        /* ************************************ */
        SET Inner_Cycle = (Inner_Cycle + 1);
        SET Inner_Bgn_Line_Num = (Inner_Bgn_Line_Num + Inner_Table_Num_of_Rows_Per_Slice); /* Setting : The Begin Line Number + The Table Number of Rows Per Slice */
    END WHILE;

END//

-- 改回默认结束符。
DELIMITER ;

/* -------------------------------------------------- */
/* EOF */
