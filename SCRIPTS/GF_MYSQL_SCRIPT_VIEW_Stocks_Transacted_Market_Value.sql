/*
 * GF_MYSQL_SCRIPT_VIEW_Stocks_Transacted_Market_Value.sql
 * Create by GF 2024-10-14 17:46
 */

CREATE OR REPLACE VIEW view_stocks_transacted_market_value as

    WITH Drive_Calculate_Transacted_Market_Value as (

        SELECT DISTINCT
            f_date,
            f_code,
            f_close,
            f_volume,
            (f_close * f_volume) as f_transacted_market_value
        FROM
            dataset_stocks
        WHERE
            f_source    = "Tushare"     AND
            f_site      = "tushare.pro" AND
            f_time_gl   = "Daily"       AND
            f_adjusted  = "不复权"
        ORDER BY
            f_date ASC
    )
    
    , Drive_Aggregate as (

        SELECT
            f_date,
            AVG(f_close) as f_close_avg,
            SUM(f_volume) as f_volume_sum,
            SUM(f_transacted_market_value) as f_transacted_market_value_sum
        FROM
            Drive_Calculate_Transacted_Market_Value
        GROUP BY
            f_date
    )
    
    , Drive_Calculate_Moving_Average as (
    
        SELECT
            f_date,
            f_close_avg,
            f_volume_sum,
            f_transacted_market_value_sum,
            AVG(f_transacted_market_value_sum) OVER (ORDER BY f_date ASC ROWS BETWEEN 4 PRECEDING AND CURRENT ROW) AS f_transacted_market_value_ma5,
            AVG(f_transacted_market_value_sum) OVER (ORDER BY f_date ASC ROWS BETWEEN 9 PRECEDING AND CURRENT ROW) AS f_transacted_market_value_ma10
        FROM
            Drive_Aggregate
    
    )
    
    SELECT * FROM Drive_Calculate_Moving_Average ORDER BY f_date DESC LIMIT 0, 30

;
