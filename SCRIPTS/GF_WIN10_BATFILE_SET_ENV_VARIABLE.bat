@echo off

REM 使用 SET 命令临时设置环境变量
REM 示例: SET MY_VAR=SomeValue
REM 这些变量仅在当前命令行会话中有效, 不会影响到系统的全局环境变量

REM --------------------------------------------------

REM 使用 SETX 命令设置全局环境变量
REM 示例: SETX MY_GLOBAL_VAR "SomeGlobalValue" /M
REM 其中 /M 参数表示将变量添加到机器级别的环境变量 (对所有用户有效)
REM 如果你只想对当前用户有效, 可以省略 /M 参数

REM --------------------------------------------------

SETX GF_DB_HOST "178.97.110.191"
SETX GF_DB_USER "ExampleUser"
SETX GF_DB_PASSWORD "12345678"
SETX GF_LIBRARY_PATH "D:\LIBRARY"

pause

@echo on

REM "EOF Signed by GF."
