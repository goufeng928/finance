# GF_PY38_SCRIPT_Get_Datas_from_Tushare.py
# Environment: Python 3.8.0
# Create By GF 2024-04-13 20:00

# ##################################################

import os
ENV_VAR_GF_INFO = os.environ.get("GF_INFO") # Get Specific System Environment Variables.
# ..................................................
import sys
COMMAND_LINE_ARGS = sys.argv
# --------------------------------------------------
import pandas
import pymysql
import sqlalchemy
import tushare

# ##################################################

# >>> Get The Values of System Environment Variables <<<
# ''''''''''''''''''''''''''''''''''''''''''''''''''''''

GF_INFO = ENV_VAR_GF_INFO.split(':')
# ..................................................
dbHostAddr = 'localhost'
dbUsername = GF_INFO[0]
dbPassword = GF_INFO[1]

# >>> Establishing a MySQL Connection Through PyMySQL <<<
# '''''''''''''''''''''''''''''''''''''''''''''''''''''''

pymysqlConnection = pymysql.connect(host = dbHostAddr,
                                    user = dbUsername,
                                    password = dbPassword,
                                    database = 'dataset',
                                    charset = 'utf8mb4',
                                    cursorclass = pymysql.cursors.DictCursor)
pymysqlCursor = pymysqlConnection.cursor()

# >>> Use Pandas 1.4.1 + SQLAlchemy 1.x to Read The Basic Information Data Table <<<
# ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

SQL_Statment = "SELECT f_key FROM dataset_api_accounts WHERE f_site = 'tushare.pro' AND f_password = '********';"
pymysqlCursor.execute(SQL_Statment)
pymysqlQueryResult = pymysqlCursor.fetchone()
Tushare_API_Key = pymysqlQueryResult["f_key"]
# -------------------------------------------------
SQL_Statment = "SELECT f_confused, f_standard FROM dataset_columns_name_mapping WHERE f_major = 'Finance' AND f_use_to = 'CSV';"
pymysqlCursor.execute(SQL_Statment)
pymysqlQueryResult = pymysqlCursor.fetchall()
columnsNameMappingCSV = {i["f_confused"]: i["f_standard"] for i in pymysqlQueryResult}
# -------------------------------------------------
SQL_Statment = "SELECT f_confused, f_standard FROM dataset_columns_name_mapping WHERE f_major = 'Finance' AND f_use_to = 'Database';"
pymysqlCursor.execute(SQL_Statment)
pymysqlQueryResult = pymysqlCursor.fetchall()
columnsNameMappingDatabase = {i["f_confused"]: i["f_standard"] for i in pymysqlQueryResult}
# -------------------------------------------------
pymysqlConnection.close()

# ##################################################

configuredAPI = tushare.pro_api(Tushare_API_Key)

stocksAdjusted:str = None # 前复权 "qfq" | 后复权 "hfq" | 不复权 None

datesList:list = []
# Caution: In Python 3.8, Slice "list", Including Head Index but not Tail Index.
if (len(COMMAND_LINE_ARGS) > 1): datesList = COMMAND_LINE_ARGS[1:len(COMMAND_LINE_ARGS)]

Received:int = 0
Restricted:int = 200000
i:int = 0

while ((Received < Restricted) and (i < len(datesList))):
    
    # >>> Code for Database Data Validation: Pandas 1.4.1 + SQLAlchemy 1.x <<<
    # ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    sqlalchemyEngine = sqlalchemy.create_engine(f"mysql+pymysql://{dbUsername}:{dbPassword}@110.41.178.142/dataset")

    SQL_Statement:str = """
    SELECT
        *
    FROM
        dataset_stocks_log
    WHERE
        f_source   = 'Tushare'     AND
        f_site     = 'tushare.pro' AND
        f_time_gl  = 'Daily'       AND
        f_adjusted = '不复权'      AND
        f_date     = '%s'          AND
        f_insert   > 0;
    """  % (datesList[i])
    
    PandasDF_dataset_stocks_log = pandas.read_sql_query(SQL_Statement, sqlalchemyEngine)

    Insert_History = PandasDF_dataset_stocks_log["f_insert"].count()
    
    # ..............................................
    
    if (Insert_History > 0):
    
        print("[Caution] The Target %s of The Operation Already has %s Record." % (datesList[i], Insert_History))
    
        i = i + 1
    
        continue

    # >>> Code for data acquisition: Tushare 1.2.89 <<<
    # '''''''''''''''''''''''''''''''''''''''''''''''''

    dateX = datesList[i].replace('-', '')

    PandasDF_API = configuredAPI.daily(adj = stocksAdjusted, start_date = dateX, end_date = dateX)
    
    New_Records_Number = PandasDF_API["ts_code"].count()
    
    # ..............................................
    
    if ((Received + New_Records_Number) > Restricted):
    
        print("[Caution] %d + %d Will Exceed %d, Reception has been Stopped." % (Received, New_Records_Number, Restricted))
    
        break
    
    # >>> Code for data correction section: Pandas 1.4.1 <<<
    # ''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    PandasDF_API = PandasDF_API.rename(columns = columnsNameMappingCSV) # Pandas' column names are mapped to type "CSV".
    
    PandasDF_API['date']     = pandas.to_datetime(PandasDF_API['date'], format='%Y-%m-%d')
    PandasDF_API["adjusted"] = "不复权" # 添加 "复权(Adjusted)" 列。
    PandasDF_API["source"]   = "Tushare"
    PandasDF_API["site"]     = "tushare.pro"
    PandasDF_API["time_gl"]  = "Daily"

    # >>> Code for writing to the database: Pandas 1.4.1 + SQLAlchemy <<<
    # '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    PandasDF_API = PandasDF_API.rename(columns = columnsNameMappingDatabase) # Pandas' column names are mapped to type "Database".
    
    # Write the obtained data to the corresponding table in the database (将获取到的数据写入数据库对应的表)。
    
    PandasDF_API.to_sql('dataset_stocks', con=sqlalchemyEngine, index=False, if_exists='append', method='multi')
    
    # Write data to the log table in the database (向数据库中的日志表写入数据)。
    
    PandasDF_Log = pandas.DataFrame({"f_source"   : ["Tushare"],
                                     "f_site"     : ["tushare.pro"],
                                     "f_time_gl"  : ["Daily"],
                                     "f_adjusted" : ["不复权"],
                                     "f_date"     : [datesList[i]],
                                     "f_insert"   : [New_Records_Number]})
    PandasDF_Log.to_sql('dataset_stocks_log', con=sqlalchemyEngine, index=False, if_exists='append', method='multi')
    
    print("[Message] Get Datas from Tushare -> Target: %s, Received: %d, Restrictions: %d." % (datesList[i], (Received + New_Records_Number), Restricted))

    # ..............................................

    Received = Received + New_Records_Number
    
    i = i + 1

# ##################################################
# EOF
