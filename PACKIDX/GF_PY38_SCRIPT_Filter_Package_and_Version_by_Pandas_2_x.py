# GF_PY38_SCRIPT_Filter_Package_and_Version_by_Pandas_2_x.py
# Environment: Python 3.8.0
# Create by GF 2025-01-16 18:36

import pandas

# ##################################################

pickingPackNameLst:list = [
    "seaborn",
    "streamlit",
    "pygwalker"
]

pickingPackVerList:list = [
    "null",
    "null",
    "null"
]

# ##################################################

df = pandas.read_csv("D:\Packages-Version-Compatibility.csv")
df[   "package"] = df[  "package"].fillna(value="null")
df[  "package2"] = df[  "package"].apply(lambda x: x.replace('_', '-'))
df[ "dependent"] = df["dependent"].fillna(value="null")
df["dependent2"] = df["dependent"].apply(lambda x: x.replace('_', '-'))
df[   "matched"] = df[  "matched"].fillna(value="null")
df[  "matched2"] = df[  "matched"].apply(lambda x: x.replace('_', '-'))

# ..................................................

dfPickResu = df.drop(df.index, inplace=False)
dfPickResu = dfPickResu[["dependent", "d_version"]]

# ..................................................

dfPickPack = df.drop(df.index, inplace=False)
for name, ver in zip(pickingPackNameLst, pickingPackVerList):
    if (ver == "null"):
        dfVariable = df[(df["package"] == name)]
        dfPickPack = pandas.concat([dfPickPack, dfVariable])
    if (ver != "null"):
        dfVariable = df[(df["package"] == name) & (df["p_version"] == ver)]
        dfPickPack = pandas.concat([dfPickPack, dfVariable])

dfPickDepe = dfPickPack
dfPickDepe = dfPickDepe["dependent2"].drop_duplicates()
numOfDepen = dfPickDepe.count()
check001   = numOfDepen # -> Check Point.

# ..................................................

# 筛选 match

dfLftTable = df[df["matched2"] != "null"].copy()
dfLftTable["pv"] = dfLftTable["package2"] + '-' + dfLftTable["p_version"]
dfLftTable = dfLftTable[["pv", "package", "p_version", "matched", "matched2", "m_version"]]
check002   = dfLftTable[["pv", "package", "p_version", "matched", "m_version"]] # -> Check Point.

dfRgtTable = dfPickPack.copy()
dfRgtTable["pv"] = dfRgtTable["dependent2"] + '-' + dfRgtTable["d_version"]
dfRgtTable = dfRgtTable[["pv", "dependent", "dependent2", "d_version"]]
check003   = dfRgtTable[["pv", "dependent", "d_version"]] # -> Check Point.

dfMrgTable = pandas.merge(dfLftTable, dfRgtTable, how="inner", on="pv")
dfMrgTable = dfMrgTable.sort_values(["matched2", "m_version"], ascending=[True, False])
dfMrgTable = dfMrgTable.drop_duplicates(subset="matched2", keep='first')
check004   = dfMrgTable[["pv", "package", "p_version", "matched", "m_version"]]

dfVerAdjst = dfMrgTable[["matched", "matched2", "m_version"]]

dfLftTable = None
dfRgtTable = None
dfMrgTable = None

# ..................................................

# Aggregate the number of repetitions of "dependent",
# and if multiple "package" require the same "dependent",
# then there will be more repetitions of "dependent".
# 聚合 "dependent" 的重复次数,
# 如果多个 "package" 需要同一个 "dependent",
# 那么 "dependent" 的重复次数就会更多。

dfVariable = dfPickPack
dfVariable["counter"] = 1
dfVariable = dfVariable[["dependent2", "d_version", "counter"]]
dfVariable = dfVariable.groupby(["dependent2", "d_version"], as_index=False).sum()

# counter == 1
# counter == 1 的 "dependent" 不存在交叉需求 (不被 2 个或以上程序共同依赖)。
# 属于 "刚性需求", 必须保留。

dfFiltered = dfVariable[dfVariable["counter"] == 1]
dfFiltered = dfFiltered.sort_values(["dependent2", "d_version"], ascending=[True, False])
dfFiltered = dfFiltered.drop_duplicates(subset="dependent2", keep='first')
dfFiltered = dfFiltered.drop("counter", axis=1)
dfPickResu = pandas.concat([dfPickResu, dfFiltered])

# counter >= 2
# counter >= 2 的 "dependent" 存在交叉需求 (被 2 个或以上程序共同依赖)。
# 属于 "选择性需求", 选择性保留 (默认保留 "version" 更高的)。

dfFiltered = dfVariable[dfVariable["counter"] >= 2]
dfFiltered = dfFiltered.sort_values(["dependent2", "d_version"], ascending=[True, False])
dfFiltered = dfFiltered.drop_duplicates(subset="dependent2", keep='first')
dfFiltered = dfFiltered.drop("counter", axis=1)

# counter == 1 和 counter >= 2 合并
# counter == 1 和 counter >= 2 合并后需要清除重复的 "dependent" (默认保留 "version" 更高的)。

dfPickResu = pandas.concat([dfPickResu, dfFiltered])
dfPickResu = dfPickResu.sort_values(["dependent2", "d_version"], ascending=[True, False])
dfPickResu = dfPickResu.drop_duplicates(subset="dependent2", keep='first')

# ..................................................

dfPickResu = pandas.merge(dfPickResu, dfVerAdjst, how="left", left_on="dependent2", right_on="matched2")
dfPickResu = dfPickResu.fillna(value="null")
for i in dfPickResu.index:
    if (dfPickResu.loc[i, "m_version"] != "null"):
        dfPickResu.loc[i, "d_version"] = dfPickResu.loc[i, "m_version"]
dfPickResu = dfPickResu[["dependent2", "d_version"]]

dfPickResu.to_csv("D:\\dep.csv", index=False)

x = pandas.DataFrame({"X":["B", "C"], "Y":[1, 2]})
y = pandas.DataFrame({"X":["A", "B", "C"], "Z":[4, 5, 6]})

# EOF. Signed by GF.
