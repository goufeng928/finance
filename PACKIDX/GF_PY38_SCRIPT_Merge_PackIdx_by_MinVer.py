# GF_PY38_SCRIPT_Merge_PackIdx_by_MinVer.py
# Environment: Python 3.8.0
# Creat by GF 2025-01-14 12:37

import os

def Read_Text_to_List(textPath:str) -> list:

    fileObject = open(textPath)
    txtLinList = fileObject.readlines()
    return txtLinList

def Version_Num_Compare(versionA:str, versionB:str) -> str:

    verA:list = versionA.split('.')
    verB:list = versionB.split('.')
    verLenA:int = len(verA)
    verLenB:int = len(verB)

    minLen = verLenA
    if (verLenA > verLenB): minLen = verLenB

    maxIdx = minLen - 1

    result = "null"
    index = 0
    while (index <= maxIdx and result == "null"):
        if (int(verA[index])  > int(verB[index])): result = "A > B"
        if (int(verA[index])  < int(verB[index])): result = "A < B"
        index = index + 1

    if (result == "null" and verLenA  > verLenB): result = "A > B"
    if (result == "null" and verLenA  < verLenB): result = "A < B"
    if (result == "null" and verLenA == verLenB): result = "A = B"

    return result

def List_2D_Fetch_1_Group(list2D:list, byField:str, value:str) -> list:

    fieldIndex = None
    for i in range(0, len(list2D[0]), 1):
        if (list2D[0][i] == byField):
            fieldIndex = i

    if (fieldIndex == None):
        print("[Caution] List_2D_Fetch_1_Group -> The Corresponding Filed does not Exist.")
        return []

    result = [list2D[0]]
    for i in list2D:
        if (i[fieldIndex] == value):
            result.append(i)

    return result

def List_2D_Fetch_1_Line_as_minVer(list2D:list, valueField:str) -> list:

    minValue:str = None
    resIndex:int = None

    valueFieldIndex:int = None
    for i in range(0, len(list2D[0]), 1):
        if (list2D[0][i] == valueField):
            valueFieldIndex = i

    if (valueFieldIndex == None):
        print("[Caution] List_2D_Fetch_1_Line_as_minVer -> The \"Value Filed\" does not Exist.")
        return []

    colIdx:int = valueFieldIndex

    for rowIdx in range(0, len(list2D), 1):

        if (rowIdx == 1):
            minValue = str(list2D[rowIdx][colIdx])
            resIndex = 1

        if (rowIdx  > 1) and (Version_Num_Compare(str(list2D[rowIdx][colIdx]), minValue) == "A < B"):
            minValue = str(list2D[rowIdx][colIdx])
            resIndex = rowIdx

    result = [list2D[0], list2D[resIndex]]

    return result

def List_2D_Group_Aggregate_for_minVer(list2D:list, groupField:str, aggreField:str) -> list:

    groupFieldIndex:int = None
    aggreFieldIndex:int = None
    for i in range(0, len(list2D[0]), 1):
        if (list2D[0][i] == groupField):
            groupFieldIndex = i
        if (list2D[0][i] == aggreField):
            aggreFieldIndex = i

    if (groupFieldIndex == None):
        print("[Caution] List_2D_Fetch_1_Line_as_minVer -> The \"Group Filed\" does not Exist.")
        return []
    if (aggreFieldIndex == None):
        print("[Caution] List_2D_Fetch_1_Line_as_minVer -> The \"Aggregate Filed\" does not Exist.")
        return []

    groupNameList:list = []
    colIdx = groupFieldIndex
    for rowIdx in range(1, len(list2D), 1):
        if (list2D[rowIdx][colIdx] not in groupNameList): groupNameList.append(list2D[rowIdx][colIdx])

    result:list = [list2D[0]]
    for name in groupNameList:
        oneGrouped = List_2D_Fetch_1_Group(list2D, groupField, name)
        minVerLine = List_2D_Fetch_1_Line_as_minVer(oneGrouped, aggreField)
        result.append(minVerLine[1])

    return result

# ##################################################

inputDirPath = "D:\\PACKIDX"
outputTxtPath = "D:\\packidx.txt"

fileList = os.listdir(inputDirPath)

list2D = [["name", "version"]]

for file in fileList:

    # Filter File Name
    # ''''''''''''''''

    if (file == "GF_PY38_SCRIPT_Filter_Package_and_Version_by_Pandas_2_x.py"): continue
    if (file == "GF_PY38_SCRIPT_Merge_PackIdx_by_MinVer.py"): continue
    if (file == "PyPI-Packages-Version-Compatibility.csv"): continue

    filePath = "%s\\%s" % (inputDirPath, file)
    txtLinList = Read_Text_to_List(filePath)

    list2DSub = []

    for textLine in txtLinList:

        x = textLine

        # Filter "Text Line"
        # ''''''''''''''''''

        if (x[0] ==  '#'): continue
        if (x[0] == '\n'): continue

        # Wash "Text Line" Character
        # ''''''''''''''''''''''''''

        x = x.replace('\n', '')

        # Convert "Text Line" to "Value List"
        # '''''''''''''''''''''''''''''''''''

        valueList = x.split("==")

        list2DSub.append(valueList)

    # Append "Text Line List" to "List 2D"
    # ''''''''''''''''''''''''''''''''''''
    
    list2D.extend(list2DSub)

# ##################################################

print("[Testing] Function -> List_2D_Fetch_1_Group(list2D:list, byField:str, value:str) -> list")

oneGroupList2D = List_2D_Fetch_1_Group(list2D, "name", "zipp")
for i in oneGroupList2D: print(i)

print("--------------------------------------------------")

print("[Testing] Function -> List_2D_Fetch_1_Line_as_minVer(list2D:list, valueField:str) -> list")

minVerLineList2D = List_2D_Fetch_1_Line_as_minVer(oneGroupList2D, "version")
for i in minVerLineList2D: print(i)

print("--------------------------------------------------")

print("[Testing] Function -> List_2D_Group_Aggregate_for_minVer(list2D:list, groupField:str, aggreField:str) -> list")

groupAggList2D = List_2D_Group_Aggregate_for_minVer(list2D, "name", "version")
for i in groupAggList2D: print(i)

# ##################################################

textLineList = groupAggList2D
for line in textLineList: line.insert(1, "==")
for line in textLineList: line.append("\n")

outTextFile = open(outputTxtPath, mode='a')
for line in textLineList: outTextFile.write(''.join(line))
outTextFile.close()
