<?php
/*
 * Create by GF 2022-10-09
 *
 * 在 PHP 环境下以 mysql_connect 方式连接数据库。
 * mysql_connect() 用于 PHP 3, PHP 4 兼容 PHP 5+。
 */

include "./GF_PHP_GLOBAL_VARIABLE.php";

// 禁止以下错误输出: 
// Deprecated: mysql_connect(): The mysql extension is deprecated and will be removed in the future.
error_reporting(0);

class PHP_5_x_mysql_connect {

    public function construct_html_tr_th_for_stocks() {

        global $GV_DATABASE_TABLE_FIELD_ADJUSTED;
        global $GV_DATABASE_TABLE_FIELD_TIME_GL;
        global $GV_DATABASE_TABLE_FIELD_DATE;
        global $GV_DATABASE_TABLE_FIELD_CODE;
        global $GV_DATABASE_TABLE_FIELD_OPEN;
        global $GV_DATABASE_TABLE_FIELD_HIGH;
        global $GV_DATABASE_TABLE_FIELD_LOW;
        global $GV_DATABASE_TABLE_FIELD_CLOSE;
        global $GV_DATABASE_TABLE_FIELD_VOLUME;
        /* .......................................... */
        echo "<tr>";
        echo "    <th>" . $GV_DATABASE_TABLE_FIELD_ADJUSTED . "</th>";
        echo "    <th>" . $GV_DATABASE_TABLE_FIELD_TIME_GL  . "</th>";
        echo "    <th>" . $GV_DATABASE_TABLE_FIELD_DATE     . "</th>";
        echo "    <th>" . $GV_DATABASE_TABLE_FIELD_CODE     . "</th>";
        echo "    <th>" . $GV_DATABASE_TABLE_FIELD_OPEN     . "</th>";
        echo "    <th>" . $GV_DATABASE_TABLE_FIELD_HIGH     . "</th>";
        echo "    <th>" . $GV_DATABASE_TABLE_FIELD_LOW      . "</th>";
        echo "    <th>" . $GV_DATABASE_TABLE_FIELD_CLOSE    . "</th>";
        echo "    <th>" . $GV_DATABASE_TABLE_FIELD_VOLUME   . "</th>";
        echo "</tr>";
    }
    
    public function construct_html_tr_td_for_stocks($Associative_Array) {

       /*
        * Associative Array Examples:
        * $Associative_Array = array("apple" => "fruit", "carrot" => "vegetable", "chicken" => "meat");
        */

        global $GV_DATABASE_TABLE_FIELD_ADJUSTED;
        global $GV_DATABASE_TABLE_FIELD_TIME_GL;
        global $GV_DATABASE_TABLE_FIELD_DATE;
        global $GV_DATABASE_TABLE_FIELD_CODE;
        global $GV_DATABASE_TABLE_FIELD_OPEN;
        global $GV_DATABASE_TABLE_FIELD_HIGH;
        global $GV_DATABASE_TABLE_FIELD_LOW;
        global $GV_DATABASE_TABLE_FIELD_CLOSE;
        global $GV_DATABASE_TABLE_FIELD_VOLUME;
        /* .......................................... */
        while ($Row = mysql_fetch_array($Associative_Array)) {

            echo "<tr>";
            echo "    <td>" . $Row[$GV_DATABASE_TABLE_FIELD_TIME_GL]  . "</td>";
            echo "    <td>" . $Row[$GV_DATABASE_TABLE_FIELD_ADJUSTED] . "</td>";
            echo "    <td>" . $Row[$GV_DATABASE_TABLE_FIELD_DATE]     . "</td>";
            echo "    <td>" . $Row[$GV_DATABASE_TABLE_FIELD_CODE]     . "</td>";
            echo "    <td>" . $Row[$GV_DATABASE_TABLE_FIELD_OPEN]     . "</td>";
            echo "    <td>" . $Row[$GV_DATABASE_TABLE_FIELD_HIGH]     . "</td>";
            echo "    <td>" . $Row[$GV_DATABASE_TABLE_FIELD_LOW]      . "</td>";
            echo "    <td>" . $Row[$GV_DATABASE_TABLE_FIELD_CLOSE]    . "</td>";
            echo "    <td>" . $Row[$GV_DATABASE_TABLE_FIELD_VOLUME]   . "</td>";
            echo "</tr>";
        }
    }
    
    public function create_connect() {

        global $GV_MYSQL_HOST;
        global $GV_MYSQL_USER;
        global $GV_MYSQL_PASSWORD;
        /* .......................................... */
        $Host     = $GV_MYSQL_HOST;
        $User     = $GV_MYSQL_USER;
        $Password = $GV_MYSQL_PASSWORD;
        /* ------------------------------------------ */
        $conn = mysql_connect($Host . ":3306", $User, $Password);
        /* ------------------------------------------ */
        if (!$conn) {

            die("Database Connect Failed: " . mysql_error() . "<br />");
            
        } else {
            
            echo "Database Connect Successful.<br />";
            /* ...................................... */
            return $conn;
        }
    }
    
    public function sql_statement_select_from_stocks($Limit_Start, $Limit_End) {

        global $GV_MYSQL_DATABASE;
        /* .......................................... */
        global $GV_DATABASE_TABLE_FIELD_ALL;
        /* .......................................... */
        $conn     = $this->create_connect();
        $Database = $GV_MYSQL_DATABASE;
        /* ------------------------------------------ */
        mysql_select_db($Database, $conn);

        $sql_statement = "SELECT " . $GV_DATABASE_TABLE_FIELD_ALL .
                         " FROM dataset_stocks " .
                         "LIMIT " . $Limit_Start . ", ". $Limit_End .";";

        $result = mysql_query($sql_statement);
        /* ------------------------------------------ */
        echo "<table border='1'>";
        $this->construct_html_tr_th_for_stocks();
        $this->construct_html_tr_td_for_stocks($result);
        echo "</table>";
        /* ------------------------------------------ */
        mysql_close($conn);
    }
}
?>

