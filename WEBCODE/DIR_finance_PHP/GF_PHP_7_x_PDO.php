<?php
/*
 * Create by GF 2022-10-09
 *
 * 在 PHP 环境下以 PDO 方式连接数据库。
 */

include "./GF_PHP_GLOBAL_VARIABLE.php";

class PHP_7_x_PDO {

    public function Construct_HTML_tr_th_for_stocks() {

        global $GV_DATABASE_TABLE_FIELD_ADJUSTED;
        global $GV_DATABASE_TABLE_FIELD_TIME_GL;
        global $GV_DATABASE_TABLE_FIELD_DATE;
        global $GV_DATABASE_TABLE_FIELD_CODE;
        global $GV_DATABASE_TABLE_FIELD_OPEN;
        global $GV_DATABASE_TABLE_FIELD_HIGH;
        global $GV_DATABASE_TABLE_FIELD_LOW;
        global $GV_DATABASE_TABLE_FIELD_CLOSE;
        global $GV_DATABASE_TABLE_FIELD_VOLUME;
        /* .......................................... */
        $HTML_tr_th_for_stocks =
            "<tr>" . PHP_EOL .
            "    <th>" . $GV_DATABASE_TABLE_FIELD_ADJUSTED . "</th>" . PHP_EOL .
            "    <th>" . $GV_DATABASE_TABLE_FIELD_TIME_GL  . "</th>" . PHP_EOL .
            "    <th>" . $GV_DATABASE_TABLE_FIELD_DATE     . "</th>" . PHP_EOL .
            "    <th>" . $GV_DATABASE_TABLE_FIELD_CODE     . "</th>" . PHP_EOL .
            "    <th>" . $GV_DATABASE_TABLE_FIELD_OPEN     . "</th>" . PHP_EOL .
            "    <th>" . $GV_DATABASE_TABLE_FIELD_HIGH     . "</th>" . PHP_EOL .
            "    <th>" . $GV_DATABASE_TABLE_FIELD_LOW      . "</th>" . PHP_EOL .
            "    <th>" . $GV_DATABASE_TABLE_FIELD_CLOSE    . "</th>" . PHP_EOL .
            "    <th>" . $GV_DATABASE_TABLE_FIELD_VOLUME   . "</th>" . PHP_EOL .
            "</tr>" . PHP_EOL;
        
        return $HTML_tr_th_for_stocks;
    }

    public function Construct_HTML_tr_td_for_stocks($Associative_Array) {

        /*
         * Associative Array Examples:
         * $Associative_Array = array("apple" => "fruit", "carrot" => "vegetable", "chicken" => "meat");
         */

        global $GV_DATABASE_TABLE_FIELD_ADJUSTED;
        global $GV_DATABASE_TABLE_FIELD_TIME_GL;
        global $GV_DATABASE_TABLE_FIELD_DATE;
        global $GV_DATABASE_TABLE_FIELD_CODE;
        global $GV_DATABASE_TABLE_FIELD_OPEN;
        global $GV_DATABASE_TABLE_FIELD_HIGH;
        global $GV_DATABASE_TABLE_FIELD_LOW;
        global $GV_DATABASE_TABLE_FIELD_CLOSE;
        global $GV_DATABASE_TABLE_FIELD_VOLUME;
        /* .......................................... */
        $HTML_tr_td_for_stocks = '';
        
        while ($Row = $Associative_Array->fetch()) {

            $HTML_tr_td_for_stocks = $HTML_tr_td_for_stocks .
                "<tr>" . PHP_EOL .
                "    <td>" . $Row[$GV_DATABASE_TABLE_FIELD_TIME_GL]  . "</td>" . PHP_EOL .
                "    <td>" . $Row[$GV_DATABASE_TABLE_FIELD_ADJUSTED] . "</td>" . PHP_EOL .
                "    <td>" . $Row[$GV_DATABASE_TABLE_FIELD_DATE]     . "</td>" . PHP_EOL .
                "    <td>" . $Row[$GV_DATABASE_TABLE_FIELD_CODE]     . "</td>" . PHP_EOL .
                "    <td>" . $Row[$GV_DATABASE_TABLE_FIELD_OPEN]     . "</td>" . PHP_EOL .
                "    <td>" . $Row[$GV_DATABASE_TABLE_FIELD_HIGH]     . "</td>" . PHP_EOL .
                "    <td>" . $Row[$GV_DATABASE_TABLE_FIELD_LOW]      . "</td>" . PHP_EOL .
                "    <td>" . $Row[$GV_DATABASE_TABLE_FIELD_CLOSE]    . "</td>" . PHP_EOL .
                "    <td>" . $Row[$GV_DATABASE_TABLE_FIELD_VOLUME]   . "</td>" . PHP_EOL .
                "</tr>" .  PHP_EOL;
        }
        
        return $HTML_tr_td_for_stocks;
    }
    
    public function Construct_HTML_tr_th_for_stocks_picked() {

        $HTML_tr_th_for_stocks =
            "<tr>" . PHP_EOL .
            "    <th>来源</th>"     . PHP_EOL .
            "    <th>站点</th>"     . PHP_EOL .
            "    <th>时间粒度</th>" . PHP_EOL .
            "    <th>复权</th>"     . PHP_EOL .
            "    <th>日期</th>"     . PHP_EOL .
            "    <th>代码</th>"     . PHP_EOL .
            "    <th>开盘</th>"     . PHP_EOL .
            "    <th>最高</th>"     . PHP_EOL .
            "    <th>最低</th>"     . PHP_EOL .
            "    <th>收盘</th>"     . PHP_EOL .
            "    <th>涨跌幅</th>"   . PHP_EOL .
            "    <th>涨跌率</th>"   . PHP_EOL .
            "    <th>成交量</th>"   . PHP_EOL .
            "</tr>" . PHP_EOL;
        
        return $HTML_tr_th_for_stocks;
    }
    
    public function Construct_HTML_tr_td_for_stocks_picked($Associative_Array) {

        /*
         * Associative Array Examples:
         * $Associative_Array = array("apple" => "fruit", "carrot" => "vegetable", "chicken" => "meat");
         */

        $HTML_tr_td_for_stocks = '';
        
        while ($Row = $Associative_Array->fetch()) {

            $HTML_tr_td_for_stocks = $HTML_tr_td_for_stocks .
                "<tr>" . PHP_EOL .
                "    <td class=\"DATA_TYPE_TEXT\">"    . $Row["f_source"]   . "</td>" . PHP_EOL .
                "    <td class=\"DATA_TYPE_TEXT\">"    . $Row["f_site"]     . "</td>" . PHP_EOL .
                "    <td class=\"DATA_TYPE_TEXT\">"    . $Row["f_time_gl"]  . "</td>" . PHP_EOL .
                "    <td class=\"DATA_TYPE_TEXT\">"    . $Row["f_adjusted"] . "</td>" . PHP_EOL .
                "    <td class=\"DATA_TYPE_DATE\">"    . $Row["f_date"]     . "</td>" . PHP_EOL .
                "    <td class=\"DATA_TYPE_DECIMAL\">" . $Row["f_code"]     . "</td>" . PHP_EOL .
                "    <td class=\"DATA_TYPE_DECIMAL\">" . $Row["f_open"]     . "</td>" . PHP_EOL .
                "    <td class=\"DATA_TYPE_DECIMAL\">" . $Row["f_high"]     . "</td>" . PHP_EOL .
                "    <td class=\"DATA_TYPE_DECIMAL\">" . $Row["f_low"]      . "</td>" . PHP_EOL .
                "    <td class=\"DATA_TYPE_DECIMAL\">" . $Row["f_close"]    . "</td>" . PHP_EOL .
                "    <td class=\"DATA_TYPE_DECIMAL\">" . $Row["f_change"]   . "</td>" . PHP_EOL .
                "    <td class=\"DATA_TYPE_DECIMAL\">" . $Row["f_chg_pct"]  . "</td>" . PHP_EOL .
                "    <td class=\"DATA_TYPE_DECIMAL\">" . $Row["f_volume"]   . "</td>" . PHP_EOL .
                "</tr>" .  PHP_EOL;
        }
        
        return $HTML_tr_td_for_stocks;
    }

    public function Create_Connect_MySQL() {

        global $GV_MYSQL_HOST;
        global $GV_MYSQL_USER;
        global $GV_MYSQL_PASSWORD;
        global $GV_MYSQL_DATABASE;
        global $GV_MYSQL_CHARSET;
        /* .......................................... */
        $Host     = $GV_MYSQL_HOST;
        $User     = $GV_MYSQL_USER;
        $Password = $GV_MYSQL_PASSWORD;

        $DSN = "mysql:host=$GV_MYSQL_HOST;dbname=$GV_MYSQL_DATABASE;charset=$GV_MYSQL_CHARSET";
        /* .......................................... */
        $Options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];

        try {

            $pdo = new PDO($DSN, $GV_MYSQL_USER, $GV_MYSQL_PASSWORD, $Options);
            /* ...................................... */
            return $pdo;

        } catch (\PDOException $e) {

            throw new \PDOException($e->getMessage());
        }
    }

    public function SQL_Statement_SELECT_from_stocks($Limit_Start, $Limit_End) {

        global $GV_DATABASE_TABLE_FIELD_ALL;

        $My_PDO = $this->Create_Connect_MySQL();

        $sql_statement = "SELECT " . $GV_DATABASE_TABLE_FIELD_ALL .
                         " FROM dataset_stocks " .
                         "LIMIT " . $Limit_Start . ", ". $Limit_End .";";

        $result = $My_PDO->query($sql_statement);
        /* ------------------------------------------ */
        $HTML_table_for_stocks =
            "<table border='1'>" . PHP_EOL .
            $this->Construct_HTML_tr_th_for_stocks() . PHP_EOL .
            $this->Construct_HTML_tr_td_for_stocks($result) . PHP_EOL .
            "</table>" .  PHP_EOL;
        /* ------------------------------------------ */
        $My_PDO = null; /* 关闭 PDO 连接 */
        
        return $HTML_table_for_stocks;
    }
    
    public function SQL_Statement_SELECT_from_stocks_picked($Limit_Start, $Limit_End) {

        $My_PDO = $this->Create_Connect_MySQL();

        $sql_statement = "SELECT " .
                         "    f_source, "   .
                         "    f_site, "     .
                         "    f_time_gl, "  .
                         "    f_adjusted, " .
                         "    f_date, "     .
                         "    f_code, "     .
                         "    f_open, "     .
                         "    f_high, "     .
                         "    f_low, "      .
                         "    f_close, "    .
                         "    f_change, "   .
                         "    f_chg_pct, "  .
                         "    f_volume "    .
                         "FROM " .
                         "    dataset_stocks_picked " .
                         "ORDER BY " .
                         "    f_date DESC " .
                         "LIMIT " . $Limit_Start . ", " . $Limit_End . ";";

        $result = $My_PDO->query($sql_statement);
        /* ------------------------------------------ */
        $HTML_table_for_stocks_picked =
            "<table border='1'>" . PHP_EOL .
            $this->Construct_HTML_tr_th_for_stocks_picked() . PHP_EOL .
            $this->Construct_HTML_tr_td_for_stocks_picked($result) . PHP_EOL .
            "</table>" .  PHP_EOL;
        /* ------------------------------------------ */
        $My_PDO = null; /* 关闭 PDO 连接 */
        
        return $HTML_table_for_stocks_picked;
    }
}
?>
