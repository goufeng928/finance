<?php
/*
 * Create by GF 2022-10-09
 */
 
header("Content-Type: text/html;charset=utf-8");

/* ************************************************** */

include "../GF_PHP_7_x_HTML_PRINT_Simple_Black_and_White.php";

include "./GF_PHP_7_x_PDO.php";

/* ************************************************** */

$My_PHP_7_x_PDO = new PHP_7_x_PDO();
$HTML_table_for_stocks_picked = $My_PHP_7_x_PDO->SQL_Statement_SELECT_from_stocks_picked(0, 9);

/* ************************************************** */

PHP_7_x_HTML_PRINT_HEAD();

PHP_7_x_HTML_PRINT_BODY($HTML_COMMON_NAVIGATER,
                        $HTML_COMMON_CONTENT_DELIMITER,
                        $HTML_table_for_stocks_picked);

PHP_7_x_HTML_PRINT_FOOTER();

?>
