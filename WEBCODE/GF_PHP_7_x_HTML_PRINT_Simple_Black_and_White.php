<?php

/*
 * GF_PHP_7_x_HTML_PRINT_Simple_Black_and_White.php
 * Create by GF 2024-09-24
 */

/* ************************************************** */

$HTML_COMMON_CONTENT_DELIMITER =
    "<p id=\"CONTENT_DELIMITER\">&ensp;</p>" . PHP_EOL;

$HTML_COMMON_NAVIGATER =
    "<nav>" . PHP_EOL .
    "    <ul>" . PHP_EOL .
    "        <li><a href=\"index.html\">首页</a></li>" . PHP_EOL .
    "        <li><a href=\"articles.html\">文章</a></li>" . PHP_EOL .
    "        <li><a href=\"about.html\">关于我</a></li>" . PHP_EOL .
    "        <li><a href=\"contact.html\">联系我</a></li>" . PHP_EOL .
    "    </ul>" . PHP_EOL .
    "</nav>" .  PHP_EOL;

/* ************************************************** */

function PHP_7_x_HTML_PRINT_HEAD() {
    
    echo "<!DOCTYPE html>" . "\n";
    echo "<html>" . "\n";
    echo "<head>" . "\n";
    echo "    <meta charset=\"UTF-8\"/>" . "\n";
    echo "    <link rel=\"stylesheet\" href=\"finance.navigater.css\">" . "\n";
    echo "    <link rel=\"stylesheet\" href=\"finance.table.css\">" . "\n";
    echo "    <link rel=\"stylesheet\" href=\"uploads.navigater.css\">" . "\n";
    echo "    <title>Uploads</title>" . "\n";
    echo "</head>" . "\n";
}

function PHP_7_x_HTML_PRINT_HEADER() {

    echo "Empty";
}

function HTML_PRINT_UPLOADS_FILES_LIST() {

    echo "<table border=\"1\">" . "\n";
    echo "    <tr id=\"FILES_LIST_ROW\">" . "\n";
    echo "        <th>名称</th>" . "\n";
    echo "        <th>分类</th>" . "\n";
    echo "        <th>类型</th>" . "\n";
    echo "    </tr>" . "\n";

    $FILES_ARRAY = scandir("/var/www/html/uploads");
    $FILES_ARRAY = array_diff($FILES_ARRAY, array('.', '..')); /* 排除 ( . ) 和 ( .. ) */
    $FILES_ARRAY = array_diff($FILES_ARRAY, array('uploads.common.css',
                                                  'uploads.files.list.css',
                                                  'uploads.navigater.css'));
    $FILES_ARRAY = array_diff($FILES_ARRAY, array('index.php',
                                                  'uploads.process.php'));
    foreach ($FILES_ARRAY as $FILE) {
        $FILE_NAME = $FILE;
        $FILE_TYPE = strtolower(pathinfo($FILE, PATHINFO_EXTENSION));
    
        echo "    <tr id=\"FILES_LIST_ROW\">" . "\n";
        echo "        <td id=\"FILES_LIST_NAME\">"  . $FILE_NAME . "</td>" . "\n";
        echo "        <td id=\"FILES_LIST_CLASS\">" . "" . "</td>" . "\n";
        echo "        <td id=\"FILES_LIST_TYPE\">"  . $FILE_TYPE . "</td>" . "\n";
        echo "    </tr>" . "\n";
    }

    echo "</table>" . "\n";
}

function HTML_PRINT_UPLOADS_BOTTOM() {

    echo "<form action=\"uploads.process.php\" method=\"post\" enctype=\"multipart/form-data\">" . "\n";
    echo "    选择文件:" . "\n";
    echo "    <input type=\"file\" name=\"fileToUpload\" id=\"fileToUpload\">" . "\n";
    echo "    <input type=\"submit\" value=\"上传文件\" name=\"submit\">" . "\n";
    echo "</form>" . "\n";
}

function PHP_7_x_HTML_PRINT_BODY(...$Elements) {
    
    echo "<body>" . PHP_EOL;
    
    foreach ($Elements as $Element) {
        echo $Element;
    }
    
    echo "</body>" . PHP_EOL;
}

function PHP_7_x_HTML_PRINT_FOOTER() {

    echo "Empty";
}
?>
