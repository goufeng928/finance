# GF_PY3_FUNCTION_COLLECTION.py
# Environment: Python
# Create By GF 2025-01-20 20:00

# ################################################################################
# Beautifualsoup4 to Version 3.12.0 of Python.

from bs4 import BeautifulSoup

def Beautofulsoup_4_x_Picking_HTML_Tag(htmlString:str, htmlTag:str) -> str:

    # Example:
    # 查找所有 <li> 标签。
    # soup.find_all('li')
    
    soup = BeautifulSoup(htmlString, 'html.parser')
    htmlTagObjectList = soup.find_all(htmlTag)
    # ..............................................
    htmlTagStringList = []
    iterator = map(lambda x: htmlTagStringList.append(x.prettify()), htmlTagObjectList)
    list(iterator)
    result = str('').join(htmlTagStringList)
    # ..............................................
    return result

def Beautofulsoup_4_x_Picking_HTML_Class(htmlString:str, htmlClass:str) -> str:

    # Example:
    # 选择所有 class 为 "story" 的标签。
    # soup.select('.story')
    
    soup = BeautifulSoup(htmlString, 'html.parser')
    htmlTagObjectList = soup.select(htmlClass)
    # ..............................................
    htmlTagStringList = []
    iterator = map(lambda x: htmlTagStringList.append(x.prettify()), htmlTagObjectList)
    list(iterator)
    result = str('').join(htmlTagStringList)
    # ..............................................
    return result

def Beautofulsoup_4_x_HTML_Get_Text(htmlString:str) -> str:

    # Example:
    # 获取 <div> 标签及其子标签的所有文本内容, 并将它们合并成一个字符串‌。
    # text = soup.find('div').get_text()
    
    soup = BeautifulSoup(htmlString, 'html.parser')
    # ..............................................
    result = soup.get_text()
    # ..............................................
    return result

# ################################################################################
# Datetime Functions Related to Version 3.8.0 of Python.

import datetime

# 一年中的第几周(Which Week of The Year)。
def Which_Week_of_The_Year(self, Year:int, Month:int, Day:int) -> int:

    # Tips: 如何获取指定日期是所在年份中的第几周?
    # 以 "2023年1月11日" 为例
    # 1. 获取 [所在年份], 如 "2023"。
    # 2. 获取 [所在年份的第 1 天], 如 "2023-01-01"。
    # 3. 计算 [指定日期] 是 [所在年份] 中的第几天, 如 "[指定日期: 2023-01-11] - [所在年份的第 1 天: 2023-01-01] + 1 = 11 天"。
    # 4. 计算 [指定日期] 是 [所在年份] 中的第几周, 如 "[指定日期是所在年份的第 11 天: 11] // 7 + 1 = 2 天"。
    # (其中 "//" 双斜杠符号是 Python 中的整除运算符, 3 // 2 = 1, 约去了 0.5)
    # ..........................................
    # Tips: 每年第 1 天是星期几? (数据参考)
    # 2022-01-01,星期六(Saturday)
    # 2023-01-01,星期日(Sunday)
    # 2024-01-01,星期一(Monday)

    Current_Date = datetime.datetime(Year, Month, Day)
    # ..............................................
    First_Day_of_The_Year = datetime.datetime(Year, 1, 1)
    # ..............................................
    Days_Difference = (Current_Date - First_Day_of_The_Year).days
    # ..............................................
    Result = (Days_Difference + 1) // 7 + 1
    # ..............................................
    return Result

# 字符串类型日期标准化(String Type Date Standardization)。
def String_Type_Date_Standardization(Input:str) -> str:

    # Parameters:
    # Input:str -> String Type Date.

    # 日期基本单位换算:
    # 1 年 = 12 个月。
    # 1, 3, 5, 7, 8, 10, 12 月 = 31 天。
    # 4, 6, 9, 11 月 = 30 天。
    # 2 月(平年) = 28 天。
    # 2 月(闰年) = 29 天。

    # ----------------------------------------------
    Old_Str_Date:str = Input
    New_Str_Date:str = None
    
    # ----------------------------------------------
    if ('.' in Input): Old_Str_Date = Input.replace('.', '/')
    if ('-' in Input): Old_Str_Date = Input.replace('-', '/')
    # ..............................................
    Lst_Date:list = [Char for Char in Old_Str_Date]

    # ----------------------------------------------
    # 情况1: 字符串类型日期共 10 个字符, 且前 4 位是年份, 如 "2024/06/23" 等。
    if (len(Old_Str_Date) == 10): New_Str_Date = Old_Str_Date
    if (len(Old_Str_Date) == 10): New_Str_Date = Old_Str_Date
    
    # ----------------------------------------------
    # 情况2: 字符串类型日期共 9 个字符, 且前 4 位是年份, 如 "2024/6/23", "2023/12/1" 等。
    if ((len(Lst_Date) == 9) and (Lst_Date[6] == '/')):
        
        Lst_Date.insert(5, '0')
        # ..........................................
        New_Str_Date = str('').join(Lst_Date)
    
    if ((len(Lst_Date) == 9) and (Lst_Date[7] == '/')):
        
        Lst_Date.insert(8, '0')
        # ..........................................
        New_Str_Date = str('').join(Lst_Date)
    
    # ----------------------------------------------
    # 情况3: 字符串类型日期共 8 个字符, 且前 4 位是年份, 如 "2024/6/8", "20231201" 等。
    if ((len(Lst_Date) == 8) and (Lst_Date[4] == '/') and (Lst_Date[6] == '/')):
    
        # Tips: 列表(List)插入(Insert)元素是在指定索引(Index)的前面添加元素。
        # Tips: Inserting Elements into a List is Adding Elements Before a Specified Index.
        Lst_Date.insert(5, '0')
        Lst_Date.insert(7 + 1, '0') # -> 连续插入, 需要在后续插入动作的索引上加 1 (前一次插入改变了列表的长度及后续元素的位置)。
        # ..........................................
        New_Str_Date = str('').join(Lst_Date)
    
    if ((len(Lst_Date) == 8) and (Lst_Date[4].isdigit() == True) and (Lst_Date[6].isdigit() == True)):
    
        # Tips: 列表(List)插入(Insert)元素是在指定索引(Index)的前面添加元素。
        # Tips: Inserting Elements into a List is Adding Elements Before a Specified Index.
        Lst_Date.insert(4, '/')
        Lst_Date.insert(6 + 1, '/') # -> 连续插入, 需要在后续插入动作的索引上加 1 (前一次插入改变了列表的长度及后续元素的位置)。
        # ..........................................
        New_Str_Date = str('').join(Lst_Date)
    
    # ----------------------------------------------
    # 情况4: 字符串类型日期共 7 个字符, 且前 4 位是年份, 如 "2024618", "2024131" 等 (特殊情况如: "2024101", "2024111", "2024121")。
    if ((len(Lst_Date) == 7) and (int("%s%s" % (Lst_Date[4], Lst_Date[5])) > 12)):
    
        Lst_Date.insert(4, '0')
        Lst_Date.insert(4, '/')
        Lst_Date.insert(5 + 2, '/')
        # ..........................................
        New_Str_Date = str('').join(Lst_Date)

    if ((len(Lst_Date) == 7) and (int("%s%s" % (Lst_Date[4], Lst_Date[5])) == 10)):
    
        Lst_Date.insert(6, '0')
        Lst_Date.insert(6, '/')
        Lst_Date.insert(4, '/') # -> 倒序连续插入 (前一次插入不影响后续定位)。
        # ..........................................
        New_Str_Date = str('').join(Lst_Date)

    if ((len(Lst_Date) == 7) and ((int("%s%s" % (Lst_Date[4], Lst_Date[5])) == 11) or (int("%s%s" % (Lst_Date[4], Lst_Date[5])) == 12))):
    
        # Tips: For Now, Let's Process Items Like "2024111" and "2024121" as "2024/01/11" and "2024/01/21".
        # Tips: 暂且将诸如 "2024111" 和 "2024121" 处理为 "2024/01/11" 和 "2024/01/21"。
    
        Lst_Date.insert(6, '/')
        Lst_Date.insert(4, '0')
        Lst_Date.insert(4, '/') # -> 倒序连续插入 (前一次插入不影响后续定位)。
        # ..........................................
        New_Str_Date = str('').join(Lst_Date)
    
    # ==============================================
    return New_Str_Date

# ################################################################################
# List 1D Functions Related to Version 3.8.0 of Python.

def List_1D_Interlace_Extract_Row1_Row2(inputList:list) -> list:

    # Example:
    #
    # Input:
    # general.architecture
    # qwen2
    # general.basename
    # DeepSeek-R1-Distill-Qwen
    # general.file_type
    # 15
    # general.name
    # DeepSeek R1 Distill Qwen 14B
    # ......
    #
    # Output:
    # general.architecture  qwen2
    # general.basename      DeepSeek-R1-Distill-Qwen
    # general.file_type     15
    # general.name          DeepSeek R1 Distill Qwen 14B
    # ......

    listLength = len(inputList)
    headIndex = 0
    tailIndex = listLength - 1

    result:list = [[], []]
    
    row_1_idx = headIndex
    row_2_idx = row_1_idx + 1

    while (row_1_idx <= tailIndex):
        result[0].append(inputList[row_1_idx])
        row_1_idx = row_1_idx + 2

    while (row_2_idx <= tailIndex):
        result[1].append(inputList[row_2_idx])
        row_2_idx = row_2_idx + 2

    return result

def List_1D_Interlace_Extract_Row1_Row2_Row3(inputList:list) -> list:

    # Example:
    #
    # Input:
    # Name
    # Type
    # Shape
    # token_embd.weight
    # Q4_K
    # [5120, 152064]
    # blk.0.attn_k.bias
    # F32
    # [1024]
    # ......
    #
    # Output:
    # Name               Type  Shape
    # token_embd.weight  Q4_K  [5120, 152064]
    # blk.0.attn_k.bias  F32   [1024]
    # ......
    
    listLength = len(inputList)
    headIndex = 0
    tailIndex = listLength - 1

    result:list = [[], [], []]
    
    row_1_idx = headIndex
    row_2_idx = row_1_idx + 1
    row_3_idx = row_1_idx + 2

    while (row_1_idx <= tailIndex):
        result[0].append(inputList[row_1_idx])
        row_1_idx = row_1_idx + 3

    while (row_2_idx <= tailIndex):
        result[1].append(inputList[row_2_idx])
        row_2_idx = row_2_idx + 3

    while (row_3_idx <= tailIndex):
        result[2].append(inputList[row_3_idx])
        row_3_idx = row_3_idx + 3

    return result

# ################################################################################
# Mathematics Functions Related to Version 3.8.0 of Python.

def Safe_Multiply(self, multiplicand:float, Multiplier:float) -> float:

    if ((multiplicand == None) or (Multiplier == None)):
        # 如果被乘数为 None 或者乘数为 None。
        # ..........................................
        return None
    else:
        Result:float = (multiplicand * Multiplier)
        # ..........................................
        return Result

def Safe_Divide(self, Divisor:float, Dividend:float) -> float:

    if ((Dividend == 0.0) or (Dividend == None)):
        # 如果分母为 0 或者分母为 None。
        # ..........................................
        return None
    else:
        Result:float = (Divisor / Dividend)
        # ..........................................
        return Result

def List_Average(self, Lst:list) -> float:

    Result = self.Safe_Divide(sum(Lst), len(Lst))
    # ..............................................
    return Result

# ################################################################################
# JSON Functions Related to Version 3.8.0 of Python.

import json

def Read_JSONFile_and_Remove_Comments(JSONFilePath:str):

    JSONFile = open(JSONFilePath, 'r', encoding="utf-8")
    JSONString = JSONFile.read()
    
    # Define Regular Expressions to Match C-Style Comments.
    # (定义正则表达式来匹配 C 风格的注释)
    Comment_Pattern_1 = re.compile(r"\/\*.*\*\/")
    Comment_Pattern_2 = re.compile(r"\/\/.*")

    # Replace Comments with Regular Expressions.
    # (使用正则表达式替换掉注释)
    New_JSON_String = re.sub(Comment_Pattern_1, str(''), JSONString)
    New_JSON_String = re.sub(Comment_Pattern_2, str(''), New_JSON_String)

    # Parse JSON String.
    # (解析 JSON 字符串)
    ParsedJSON = json.loads(New_JSON_String)
    # ..............................................
    return ParsedJSON

def JSONString_Extract_Element_By_Index(JSONString:str, Index:int):

    # Convert "JSON String" to "Python List" (将 JSON 字符串转为 Python 列表)
    List_Obj = json.loads(JSONString)

    Element = List_Obj[Index]
    # ..............................................
    return Element

def JSONString_Replace_Element_By_Index(JSONString:str, Index:int, New_Element):

    # Convert "JSON String" to "Python List" (将 JSON 字符串转为 Python 列表)
    List_Obj = json.loads(JSONString)

    List_Obj[Index] = New_Element

    # 例如: Python 列表 [1, 2, 3] 将输出 '[1, 2, 3]'
    # (注意这里的输出实际上是带有双引号的, 并且数字之间有空格, 符合 JSON 标准)
    New_JSON_String = json.dumps(List_Obj)
    # ..............................................
    return New_JSON_String

# EOF Signed by GF.
