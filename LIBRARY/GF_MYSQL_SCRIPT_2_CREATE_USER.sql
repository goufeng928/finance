-- source /home/goufeng/GF_MYSQL_SCRIPT_2_CREATE_USER.sql

-- Create by GF 2023-06-20

/* -------------------------------------------------- */
/* 创建用户 */

-- 创建 "goufeng" 用户 (如果不存在才创建).
CREATE USER IF NOT EXISTS 'goufeng'@'%' IDENTIFIED WITH mysql_native_password BY '12345678';

/* -------------------------------------------------- */
/* 用户授权 */

/* "WITH GRANT OPTION" 这个选项表示该用户可以将自己拥有的权限授权给别人 */

/* 授予 "goufeng" 用户对数据库 "dataset" 的 "SELECT" 权限 */
GRANT SELECT ON dataset.* TO 'goufeng'@'%' WITH GRANT OPTION;

/* 授予 "goufeng" 用户对数据库 "working" 的 "所有" 权限 */
GRANT ALL PRIVILEGES ON working.* TO 'goufeng'@'%' WITH GRANT OPTION;

/* -------------------------------------------------- */
/* 刷新权限 */

FLUSH PRIVILEGES;

/* -------------------------------------------------- */
/* EOF */
