# GF_PY3_DIY_Tushare_1_2_x_API_Get_Data.py
# Environment: Python 3.8.0
# Create By GF 2024-04-13 20:00

# ##################################################

# Dep: datetime ------- From Python 3.8.0 Own.
# Dep: pandas 1.4.1 --- From Third-Party Library.
# Dep: tushare 1.2.89 - From Third-Party Library.

# ##################################################

import sys
sys.path.append('./')
sys.path.append('./COLLECTION')

import os
import datetime
import pandas
import tushare
# ..................................................
from GF_PY3_GLOBAL_VARIABLE import *
from GF_PY3_FUNCTION_SQLAlchemy_1_x import *
from GF_PY3_DIY_String_Type_Date_and_Time_Standardization import *

# ##################################################
# Global 选项:

CACHE_DISK = "F:"

# ##################################################

def Tushare_1_2_x_API_Get_All_Stock_Daily_Data(Configured_API:object, Adjusted:str, Specific_Date:object) -> pandas.core.frame.DataFrame:

    # Configured_API: "Configured_API" Refers to The Tushare API Interface That Has Already Been Configured with Token.
    
    # ----------------------------------------------
    Sp_Date_Str:object = Specific_Date
    # ..............................................
    if (type(Specific_Date) is datetime.datetime): Sp_Date_Str = Specific_Date.strftime("%Y%m%d")
    # ..............................................
    if (type(Specific_Date) is str): Sp_Date_Str = String_Type_Date_Standardization(Specific_Date).replace('/', str(''))
    
    # ----------------------------------------------
    MY_ADJUSTED:str = None # Default: "None" Equals "不复权(Unadjusted)"
    if (Adjusted ==  "Forward"): MY_ADJUSTED = "qfq"
    if (Adjusted == "Backward"): MY_ADJUSTED = "hfq"

    # ----------------------------------------------
    All_Cache_File_List = os.listdir(CACHE_DISK + "\\DATASET\\API_CACHE")
    
    if (("tushare_1_2_x_api_get_all_stock_daily_data_" + Sp_Date_Str + ".csv") not in All_Cache_File_List):
        
        # ------------------------------------------
        # Tushare Pro 查询当前所有正常上市交易的股票列表。
        #
        # 方法 1: ts.pro_api("[KEY]").stock_basic():
        # All_Stocks_List = Configured_API.stock_basic(exchange='', list_status='L', fields='ts_code,symbol,name,area,industry,list_date')
        #
        # 方法 2: ts.pro_api("[KEY]").query():
        # All_Stocks_List = Configured_API.query('stock_basic', exchange='', list_status='L', fields='ts_code,symbol,name,area,industry,list_date')
        #
        # Tushare Pro 查询当前所有正常上市交易的股票列表 (数据样例):
        #      ts_code  symbol      name  area  industry  list_date
        # 0  000001.SZ  000001  平安银行  深圳      银行   19910403
        # 1  000002.SZ  000002     万科A  深圳  全国地产   19910129
        # 2  000004.SZ  000004  国农科技  深圳  生物制药   19910114
        # 3  000005.SZ  000005  世纪星源  深圳  房产服务   19901210
        # 4  000006.SZ  000006   深振业A  深圳  区域地产   19920427
        # 5  000007.SZ  000007    全新好  深圳  酒店餐饮   19920413
        # ......
        
        # ------------------------------------------
        # Function Tips: tushare.pro_api("[KEY]").daily():
        # - Parameter: adj=None ----> 不复权: None (只针对股票, Default)
        # - Parameter: adj="qfq" ---> 前复权: qfq (只针对股票)
        # - Parameter: adj="hfq" ---> 后复权: hfq (只针对股票)
        
        # ------------------------------------------
        # 前复权:
        # 前复权即就是保持现有价位不变, 将以前的价格缩减, 将除权前的K线向下平移, 使图形吻合, 保持股价走势的连续性。
        #
        # 后复权:
        # 后复权就是在 K 线图上以除权前的价格为基准来测算除权后股票的市场成本价。就是把除权后的价格按以前的价格换算过来。
        # 简单的说, 就是保持先前的价格不变, 而将以后的价格增加。
        
        # ------------------------------------------
        # Tushare Pro 免费版单次读取数据条数限制:
        # 2024年06月26日测试: 单次读取条数限制为 6000 条数据。
        
        #Fetched_Data_PDF = Configured_API.daily(ts_code=[601933.SH, 601088, ..., 601398.SH], start_date=20240708, end_date=20240708) # -> 报错: 超过 1000 条数据!
        Fetched_Data_PDF = Configured_API.daily(adj=MY_ADJUSTED, start_date=Sp_Date_Str, end_date=Sp_Date_Str)
    
        # Pandas 列名映射 (Columns Name Mapping)。
        # ------------------------------------------
        Fetched_Data_PDF = Fetched_Data_PDF.rename(columns=GV_JSON_MAPPING_HEADER_TO_CSV[0]) # -> 调用 Pandas 的 .rename() 方法更改列名。
        # ..........................................
        Fetched_Data_PDF['date'] = pandas.to_datetime(Fetched_Data_PDF['date'], format='%Y-%m-%d')
        
        # 添加 "复权(Adjusted)" 列。
        # ------------------------------------------
        Fetched_Data_PDF["adjusted"] = None
        # ..........................................
        if (Adjusted == "Unadjusted"): Fetched_Data_PDF["adjusted"] = "不复权"
        if (Adjusted ==    "Forward"): Fetched_Data_PDF["adjusted"] = "前复权"
        if (Adjusted ==   "Backward"): Fetched_Data_PDF["adjusted"] = "后复权"
        
        Write_Path = (CACHE_DISK + "\\DATASET\\API_CACHE\\tushare_1_2_x_api_get_all_stock_daily_data_" +  Sp_Date_Str + ".csv")
        Fetched_Data_PDF.to_csv(Write_Path, mode='w', index=False)

        return Fetched_Data_PDF
    
    else:
        
        Read_Path = (CACHE_DISK + "\\DATASET\\API_CACHE\\tushare_1_2_x_api_get_all_stock_daily_data_" +  Sp_Date_Str + ".csv")
        
        # Tips: 读取 CSV 文件, 不自动解析日期列为日期时间格式, 而保持为字符串格式。
        # PDF = pd.read_csv('your_file.csv', parse_dates=['date_column'])
    
        Cache_Data_PDF = pandas.read_csv(Read_Path)
        Cache_Data_PDF['date'] = pandas.to_datetime(Cache_Data_PDF['date'], format='%Y-%m-%d')
        
        # ==========================================
        return Cache_Data_PDF

def Tushare_1_2_x_API_Get_All_Stocks_Single_Date_Daily_Data_to_MySQL(Configured_API:object, Adjusted:str, Date:object) -> int:

    # Configured_API: "Configured_API" Refers to The Tushare API Interface That Has Already Been Configured with Token.
    
    # ----------------------------------------------
    INPUT_DATE:object = Date
    # ..............................................
    if (type(INPUT_DATE) is datetime.datetime): INPUT_DATE = INPUT_DATE.strftime("%Y-%m-%d")
    # ..............................................
    if (type(INPUT_DATE) is str): INPUT_DATE = String_Type_Date_Standardization(INPUT_DATE).replace('/', '-')
    
    # ----------------------------------------------
    INPUT_ADJUSTED:str = None # Default: "None" Equals "不复权(Unadjusted)"
    if (Adjusted ==  "Forward"): INPUT_ADJUSTED = "qfq"
    if (Adjusted == "Backward"): INPUT_ADJUSTED = "hfq"

    # ----------------------------------------------
    Engine = SQLAlchemy_1_x_Create_Engine_MySQL()
    # ..............................................
    SQL_Statement:str = \
    """
    SELECT
        *
    FROM
        dataset_stocks_log
    WHERE
        f_source   = "Tushare"     AND
        f_site     = "tushare.pro" AND
        f_time_gl  = "Daily"       AND
        f_adjusted = "不复权"      AND
        f_date     = "%s"          AND
        f_insert   > 0;
    """ % (INPUT_DATE)
    
    PandasDF_dataset_stocks_log = pandas.read_sql_query(SQL_Statement, Engine)
    # ..............................................
    Insert_History = PandasDF_dataset_stocks_log["f_insert"].count()
    
    # ----------------------------------------------
    if (Insert_History == 0):
        
        # ------------------------------------------
        # Tushare Pro 查询当前所有正常上市交易的股票列表。
        #
        # 方法 1: ts.pro_api("[KEY]").stock_basic():
        # All_Stocks_List = Configured_API.stock_basic(exchange='', list_status='L', fields='ts_code,symbol,name,area,industry,list_date')
        #
        # 方法 2: ts.pro_api("[KEY]").query():
        # All_Stocks_List = Configured_API.query('stock_basic', exchange='', list_status='L', fields='ts_code,symbol,name,area,industry,list_date')
        #
        # Tushare Pro 查询当前所有正常上市交易的股票列表 (数据样例):
        #      ts_code  symbol      name  area  industry  list_date
        # 0  000001.SZ  000001  平安银行  深圳      银行   19910403
        # 1  000002.SZ  000002     万科A  深圳  全国地产   19910129
        # 2  000004.SZ  000004  国农科技  深圳  生物制药   19910114
        # 3  000005.SZ  000005  世纪星源  深圳  房产服务   19901210
        # 4  000006.SZ  000006   深振业A  深圳  区域地产   19920427
        # 5  000007.SZ  000007    全新好  深圳  酒店餐饮   19920413
        # ......
        
        # ------------------------------------------
        # Function Tips: tushare.pro_api("[KEY]").daily():
        # - Parameter: adj=None ----> 不复权: None (只针对股票, Default)
        # - Parameter: adj="qfq" ---> 前复权: qfq (只针对股票)
        # - Parameter: adj="hfq" ---> 后复权: hfq (只针对股票)
        
        # ------------------------------------------
        # 前复权:
        # 前复权即就是保持现有价位不变, 将以前的价格缩减, 将除权前的K线向下平移, 使图形吻合, 保持股价走势的连续性。
        #
        # 后复权:
        # 后复权就是在 K 线图上以除权前的价格为基准来测算除权后股票的市场成本价。就是把除权后的价格按以前的价格换算过来。
        # 简单的说, 就是保持先前的价格不变, 而将以后的价格增加。
        
        # ------------------------------------------
        # Tushare Pro 免费版单次读取数据条数限制:
        # 2024年06月26日测试: 单次读取条数限制为 6000 条数据。
        
        #Fetched_Data_PDF = Configured_API.daily(ts_code=[601933.SH, 601088, ..., 601398.SH], start_date=20240708, end_date=20240708) # -> 报错: 超过 1000 条数据!
        PandasDF_API = Configured_API.daily(
            adj=INPUT_ADJUSTED,
            start_date=INPUT_DATE.replace('-', str('')),
            end_date=INPUT_DATE.replace('-', str(''))
        )
    
        # Pandas 列名映射 (Columns Name Mapping)。
        # ------------------------------------------
        PandasDF_API = PandasDF_API.rename(columns=GV_JSON_MAPPING_HEADER_TO_CSV[0]) # -> 调用 Pandas 的 .rename() 方法更改列名。
        # ..........................................
        PandasDF_API['date'] = pandas.to_datetime(PandasDF_API['date'], format='%Y-%m-%d')
        
        # 添加 "复权(Adjusted)" 列。
        # ------------------------------------------
        PandasDF_API["adjusted"] = None
        # ..........................................
        if (Adjusted == "Unadjusted"): PandasDF_API["adjusted"] = "不复权"
        if (Adjusted ==    "Forward"): PandasDF_API["adjusted"] = "前复权"
        if (Adjusted ==   "Backward"): PandasDF_API["adjusted"] = "后复权"
        # ..........................................
        PandasDF_API["source"]   = "Tushare"
        PandasDF_API["site"]     = "tushare.pro"
        PandasDF_API["time_gl"]  = "Daily"
        
        PandasDF_API = PandasDF_API.rename(columns=GV_JSON_MAPPING_HEADER_TO_DATABASE[0])
        
        # ------------------------------------------
        TABLE_NAME:str = "dataset_stocks"
        WIRITING_RECORDS_NUM:int = PandasDF_API["f_date"].count()
        print("[Working] Tushare_1_2_x_API_Get_All_Stocks_Single_Date_Daily_Data_to_MySQL:")
        print("[Message] Writing to Database %s Table, Number of Records Written: %d." % (TABLE_NAME, WIRITING_RECORDS_NUM))
        # ..........................................
        PandasDF_API.to_sql('dataset_stocks', con=Engine, index=False, if_exists='append', method='multi')
        
        # ------------------------------------------
        PandasDF_Log = pandas.DataFrame({"f_source"   : ["Tushare"],
                                         "f_site"     : ["tushare.pro"],
                                         "f_time_gl"  : ["Daily"],
                                         "f_adjusted" : ["不复权"],
                                         "f_date"     : [INPUT_DATE],
                                         "f_insert"   : [WIRITING_RECORDS_NUM]})
        PandasDF_Log.to_sql('dataset_stocks_log', con=Engine, index=False, if_exists='append', method='multi')
        # ==========================================
        return 1
    
    else:
        
        TABLE_NAME:str = "dataset_stocks"
        print("[Working] Tushare_1_2_x_API_Get_All_Stocks_Single_Date_Daily_Data_to_MySQL:")
        print("[Message] Not Written to Database %s Table, There is %d Insert History." % (TABLE_NAME, Insert_History))
        # ==========================================
        return 0

# ##################################################
# EOF
