# GF_PY3_FUNCTION_Pandas_1_x.py
# Environment: Python 3.8.0
# Create By GF 2024-04-13 20:00

# ##################################################

# Dep: pandas 1.4.1 ----- From Third-Party Library.

# ##################################################

import sys
sys.path.append('./')
sys.path.append('./COLLECTION')
# ..................................................
import pandas
# ..................................................
from GF_PY3_GLOBAL_VARIABLE import *
from GF_PY3_FUNCTION_SQLAlchemy_1_x import *

# ##################################################

def Pandas_Read_Data_from_MySQL_SQL_Statement(SQL_Statement:str) ->  pandas.core.frame.DataFrame:

    Engine   = SQLAlchemy_1_x_Create_Engine_MySQL()
    PandasDF = pandas.read_sql_query(SQL_Statement, Engine)
    # ==============================================
    return PandasDF

# Pandas 1.x: 数据分桶 - 简单 (Data Bucketing - Simple)
def Pandas_1_x_Data_Bucketing_Simple(Input, Number_of_Buckets:int, Data_Volume_Per_Bucket:int) -> pandas.core.frame.DataFrame:

    df = Input.copy()
    # ----------------------------------------------
    Num_of_BKT = Number_of_Buckets
    Vol_of_BKT = Data_Volume_Per_Bucket
    # ----------------------------------------------
    Number_of_Rows = df.shape[0]
    # ..............................................
    if (Number_of_Buckets == None):
        Remainder = Number_of_Rows % Vol_of_BKT # -> 余数。
        Num_of_BKT = (Number_of_Rows - Remainder) / Vol_of_BKT + 1
    
    Step = 1
    Starting_Index = 0
    # ..............................................
    while (Step <= (Num_of_BKT - 1)):
        Ending_Index = (Starting_Index + Vol_of_BKT - 1)
        df.loc[(Starting_Index <= df.index) & (df.index <= Ending_Index), 'bucket'] = Step
        # ..........................................
        Starting_Index = Ending_Index + 1
        Step = Step + 1
    
    if (Remainder == 0): Ending_Index = (Starting_Index + Vol_of_BKT - 1)
    if (Remainder  > 0): Ending_Index = (Starting_Index +  Remainder - 1)
    # ..............................................
    df.loc[(Starting_Index <= df.index) & (df.index <= Ending_Index), 'bucket'] = Num_of_BKT
    # ----------------------------------------------
    return df

# ##################################################
# EOF
