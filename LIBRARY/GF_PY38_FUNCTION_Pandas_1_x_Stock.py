# GF_PY38_FUNCTION_Pandas_1_x_Stock.py
# Environment: Python 3.8.0
# Create By GF 2024-04-13 20:00

import os

# Get Specific System Environment Variables.
LIBRARY_PATH:str = os.environ.get("GF_LIBRARY_PATH")

import sys

# Add "Library Path" to The Working Path of Python.
sys.path.append(LIBRARY_PATH)

# ##################################################

import pandas
# ..................................................
import GF_PY38_CLASS_Mapping
# ..................................................
Mapping_FinInd = GF_PY38_CLASS_Mapping.Mapping_FinInd() # -> Mapping_FinInd Class Preparation.
Mapping_EtgInd = GF_PY38_CLASS_Mapping.Mapping_EtgInd() # -> Mapping_EtgInd Class Preparation.
Mapping_SeqOpr = GF_PY38_CLASS_Mapping.Mapping_SeqOpr() # -> Mapping_SeqOpr Class Preparation.

# ##################################################
    
def Pandas_1_x_Stock_Calculate_Indicator_FinInd(PandasDF:pandas.core.frame.DataFrame, COL_NAME_SUFFIX=None) -> pandas.core.frame.DataFrame:

    df = PandasDF.copy()
    # ..........................................
    Columns_Names:list = PandasDF.columns
    
    # ------------------------------------------
    if (COL_NAME_SUFFIX != None):
        COL_NAME_row_num =      "row_num(%s)" % COL_NAME_SUFFIX
        COL_NAME_date =         "date" # The Date has Uniqueness.
        COL_NAME_high =         "high(%s)" % COL_NAME_SUFFIX
        COL_NAME_low =          "low(%s)" % COL_NAME_SUFFIX
        COL_NAME_close =        "close(%s)" % COL_NAME_SUFFIX
        COL_NAME_change =       "change(%s)" % COL_NAME_SUFFIX
        COL_NAME_ma5 =          "ma5(%s)" % COL_NAME_SUFFIX
        COL_NAME_ma10 =         "ma10(%s)" % COL_NAME_SUFFIX
        COL_NAME_ema12 =        "ema12(%s)" % COL_NAME_SUFFIX
        COL_NAME_ema26 =        "ema26(%s)" % COL_NAME_SUFFIX
        COL_NAME_macd_dif =     "macd_dif(%s)" % COL_NAME_SUFFIX
        COL_NAME_macd_dea =     "macd_dea(%s)" % COL_NAME_SUFFIX
        COL_NAME_macd_stick =   "macd_stick(%s)" % COL_NAME_SUFFIX
        COL_NAME_rsi6 =         "rsi6(%s)" % COL_NAME_SUFFIX
        COL_NAME_rsi12 =        "rsi12(%s)" % COL_NAME_SUFFIX
        COL_NAME_rsi24 =        "rsi24(%s)" % COL_NAME_SUFFIX
        COL_NAME_kdj_k =        "kdj_k(%s)" % COL_NAME_SUFFIX
        COL_NAME_kdj_d =        "kdj_d(%s)" % COL_NAME_SUFFIX
        COL_NAME_kdj_j =        "kdj_j(%s)" % COL_NAME_SUFFIX
    else:
        COL_NAME_row_num =      "row_num"
        COL_NAME_date =         "date"
        COL_NAME_high =         "high"
        COL_NAME_low =          "low"
        COL_NAME_close =        "close"
        COL_NAME_change =       "change"
        COL_NAME_ma5 =          "ma5"
        COL_NAME_ma10 =         "ma10"
        COL_NAME_ema12 =        "ema12"
        COL_NAME_ema26 =        "ema26"
        COL_NAME_macd_dif =     "macd_dif"
        COL_NAME_macd_dea =     "macd_dea"
        COL_NAME_macd_stick =   "macd_stick"
        COL_NAME_rsi6 =         "rsi6"
        COL_NAME_rsi12 =        "rsi12"
        COL_NAME_rsi24 =        "rsi24"
        COL_NAME_kdj_k =        "kdj_k"
        COL_NAME_kdj_d =        "kdj_d"
        COL_NAME_kdj_j =        "kdj_j"

    # ------------------------------------------
    df[COL_NAME_date] = pandas.to_datetime(df[COL_NAME_date], format='%Y-%m-%d')
    
    # ------------------------------------------
    # 分配行号。
    # Sorting Must be Performed Before Assigning Line Numbers, Otherwise the Line Numbers May not be Arranged Incrementally.
    df = df.sort_values(["code", COL_NAME_date], ascending=[True, True])
    # ..............................................
    df[COL_NAME_row_num] = df.groupby("code")[COL_NAME_date].rank(method='first', ascending=True).astype("int64")
    
    # ------------------------------------------
    # 计算简单移动平均值(Simple Moving Average)。
    PandasDF_Calculate = df.groupby("code", as_index=False)[COL_NAME_close].rolling(window=5 ).mean()
    df[COL_NAME_ma5]  = PandasDF_Calculate[COL_NAME_close]
    PandasDF_Calculate = df.groupby("code", as_index=False)[COL_NAME_close].rolling(window=10).mean()
    df[COL_NAME_ma10] = PandasDF_Calculate[COL_NAME_close]
    
    # ------------------------------------------
    # 计算金融指标(Finance Indicator)。
    df[COL_NAME_ema12] = df.apply(lambda ROW: Mapping_FinInd.EMA(Index=ROW[COL_NAME_row_num], Period=12, Close=ROW[COL_NAME_close]), axis=1)
    df[COL_NAME_ema26] = df.apply(lambda ROW: Mapping_FinInd.EMA(Index=ROW[COL_NAME_row_num], Period=26, Close=ROW[COL_NAME_close]), axis=1)
    df[COL_NAME_macd_dif] = df.apply(lambda ROW: Mapping_FinInd.MACD_DIF(EMA12=ROW[COL_NAME_ema12], EMA26=ROW[COL_NAME_ema26]), axis=1)
    df[COL_NAME_macd_dea] = df.apply(lambda ROW: Mapping_FinInd.MACD_DEA(Index=ROW[COL_NAME_row_num], MACD_DIF=ROW[COL_NAME_macd_dif]), axis=1)
    df[COL_NAME_macd_stick] = df.apply(lambda ROW: Mapping_FinInd.MACD_STICK(MACD_DIF=ROW[COL_NAME_macd_dif], MACD_DEA=ROW[COL_NAME_macd_dea]), axis=1)
    df[COL_NAME_rsi6] = df.apply(lambda ROW: Mapping_FinInd.RSI(Index=ROW[COL_NAME_row_num], Period=6, Change=ROW[COL_NAME_change]), axis=1)
    df[COL_NAME_rsi12] = df.apply(lambda ROW: Mapping_FinInd.RSI(Index=ROW[COL_NAME_row_num], Period=12, Change=ROW[COL_NAME_change]), axis=1)
    df[COL_NAME_rsi24] = df.apply(lambda ROW: Mapping_FinInd.RSI(Index=ROW[COL_NAME_row_num], Period=24, Change=ROW[COL_NAME_change]), axis=1)
    df[COL_NAME_kdj_k] = df.apply(lambda ROW: Mapping_FinInd.KDJ_K(Index=ROW[COL_NAME_row_num], RSV_Prd=9, K_Prd=3, High=ROW[COL_NAME_high], Low=ROW[COL_NAME_low], Close=ROW[COL_NAME_close]), axis=1)
    df[COL_NAME_kdj_d] = df.apply(lambda ROW: Mapping_FinInd.KDJ_D(Index=ROW[COL_NAME_row_num], RSV_Prd=9, D_Prd=3, K_Val=ROW[COL_NAME_kdj_k]), axis=1)
    df[COL_NAME_kdj_j] = df.apply(lambda ROW: Mapping_FinInd.KDJ_J(K_Val=ROW[COL_NAME_kdj_k], D_Val=ROW[COL_NAME_kdj_d]), axis=1)
    
    # ==========================================
    return df

def Pandas_1_x_Stock_Calculate_Indicator_EtgInd(PandasDF:pandas.core.frame.DataFrame, COL_NAME_SUFFIX=None) -> pandas.core.frame.DataFrame:

    df = PandasDF.copy()
    # ..........................................
    Columns_Names:list = PandasDF.columns
    
    # ------------------------------------------
    if (COL_NAME_SUFFIX != None):
        COL_NAME_row_num =      "row_num(%s)" % COL_NAME_SUFFIX
        COL_NAME_date =         "date" # The Date has Uniqueness.
        COL_NAME_high =         "high(%s)" % COL_NAME_SUFFIX
        COL_NAME_low =          "low(%s)" % COL_NAME_SUFFIX
        COL_NAME_etg_trs =      "etg_trs(%s)" % COL_NAME_SUFFIX
        COL_NAME_etg_brs =      "etg_brs(%s)" % COL_NAME_SUFFIX
        COL_NAME_etg_tbrs_i =   "etg_tbrs_i(%s)" % COL_NAME_SUFFIX
        COL_NAME_etg_tbrs_f =   "etg_tbrs_f(%s)" % COL_NAME_SUFFIX
    else:
        COL_NAME_row_num =      "row_num"
        COL_NAME_date =         "date"
        COL_NAME_high =         "high"
        COL_NAME_low =          "low"
        COL_NAME_etg_trs =      "etg_trs"
        COL_NAME_etg_brs =      "etg_brs"
        COL_NAME_etg_tbrs_i =   "etg_tbrs_i"
        COL_NAME_etg_tbrs_f =   "etg_tbrs_f"

    # ------------------------------------------
    df[COL_NAME_date] = pandas.to_datetime(df[COL_NAME_date], format='%Y-%m-%d')
    
    # ------------------------------------------
    # 分配行号。
    # Sorting Must be Performed Before Assigning Line Numbers, Otherwise the Line Numbers May not be Arranged Incrementally.
    df = df.sort_values(["code", COL_NAME_date], ascending=[True, True])
    # ..............................................
    df[COL_NAME_row_num] = df.groupby("code")[COL_NAME_date].rank(method='first', ascending=True).astype("int64")
    
    # ------------------------------------------
    df[COL_NAME_etg_trs]    = df.apply(lambda ROW: Mapping_EtgInd.Top_Reversal_Shape(Index=ROW[COL_NAME_row_num], IN_for_UpperEdge=ROW[COL_NAME_high], IN_for_LowerEdge=ROW[COL_NAME_low]), axis=1)
    df[COL_NAME_etg_brs]    = df.apply(lambda ROW: Mapping_EtgInd.Bottom_Reversal_Shape(Index=ROW[COL_NAME_row_num], IN_for_UpperEdge=ROW[COL_NAME_high], IN_for_LowerEdge=ROW[COL_NAME_low]), axis=1)
    df[COL_NAME_etg_tbrs_i] = df.apply(lambda ROW: Mapping_EtgInd.TBRS_Integer_Coding(TRS=ROW[COL_NAME_etg_trs], BRS=ROW[COL_NAME_etg_brs]), axis=1)
    df[COL_NAME_etg_tbrs_f] = df.apply(lambda ROW: Mapping_SeqOpr.Fill_Backward_and_Count(Index=ROW[COL_NAME_row_num], Value=ROW[COL_NAME_etg_tbrs_i]), axis=1)
    
    # ==========================================
    return df

# Pandas 1.x Stock: Identify DataFrame as Daily.
def Pandas_1_x_Stock_Identify_DataFrame_as_Daily(Input:pandas.core.frame.DataFrame) -> pandas.core.frame.DataFrame:

    # date
    # code
    # open    ---> Rename: open(dy)
    # high    ---> Rename: high(dy)
    # low     ---> Rename: low(dy)
    # close   ---> Rename: close(dy)
    # change  ---> Rename: change(dy)
    # chg_pct ---> Rename: chg_pct(dy)
    # volume  ---> Rename: volume(dy)
    
    df = Input.copy()
    df = df.rename(columns={
            "row_num": "row_num(dy)",
            "open"   : "open(dy)"   ,
            "high"   : "high(dy)"   ,
            "low"    : "low(dy)"    ,
            "close"  : "close(dy)"  ,
            "change" : "change(dy)" ,
            "chg_pct": "chg_pct(dy)",
            "volume" : "volume(dy)"
        }
    )
    # ==========================================
    return df

# Pandas 1.x Stock: Aggregate DataFrame Daily to Weekly.
def Pandas_1_x_Stock_Aggregate_DataFrame_Daily_to_Weekly(Input:pandas.core.frame.DataFrame) -> pandas.core.frame.DataFrame:

    PDF = Input.copy()
    # ------------------------------------------
    # 计算指定日期是所在年份中的第几周。
    PDF["date"]     = pandas.to_datetime(PDF["date"], format='%Y-%m-%d')
    # ..........................................
    PDF["year"]     = PDF["date"].dt.year
    PDF["week_num"] = PDF["date"].dt.strftime("%U") # -> 其中 %U 表示一年中的第几周。
    PDF["week_num"] = PDF["week_num"].astype("int64")
    # ------------------------------------------
    # Prepare The Left Table as "refer".
    ReferPDF = PDF[["code", "year", "week_num"]].copy()
    ReferPDF = ReferPDF.drop_duplicates()
    # ..........................................
    # Grouping Data.
    GroupedPDF = PDF.copy()
    GroupedPDF = GroupedPDF.groupby(["code", "year", "week_num"], as_index=True)
    # ..........................................
    ReferPDF = ReferPDF.join(other=GroupedPDF["date"].max(),        on=["code", "year", "week_num"], how="left")
    ReferPDF = ReferPDF.join(other=GroupedPDF["open(dy)"].nth(0),   on=["code", "year", "week_num"], how="left")
    ReferPDF = ReferPDF.join(other=GroupedPDF["high(dy)"].max(),    on=["code", "year", "week_num"], how="left")
    ReferPDF = ReferPDF.join(other=GroupedPDF["low(dy)"].min(),     on=["code", "year", "week_num"], how="left")
    ReferPDF = ReferPDF.join(other=GroupedPDF["close(dy)"].nth(-1), on=["code", "year", "week_num"], how="left")
    ReferPDF = ReferPDF.join(other=GroupedPDF["change(dy)"].sum(),  on=["code", "year", "week_num"], how="left")
    ReferPDF = ReferPDF.join(other=GroupedPDF["volume(dy)"].sum(),  on=["code", "year", "week_num"], how="left")
    # ..........................................
    ReferPDF["date(wk)"] = ReferPDF["date"] # -> To Avoid Confusion with Daily Data, The "date" Column is Copied.
    # ..........................................
    ReferPDF = ReferPDF.rename(columns={
            "open(dy)"  : "open(wk)"  ,
            "high(dy)"  : "high(wk)"  ,
            "low(dy)"   : "low(wk)"   ,
            "close(dy)" : "close(wk)" ,
            "change(dy)": "change(wk)",
            "volume(dy)": "volume(wk)"
        }
    )
    
    # ------------------------------------------
    # 分配行号。
    # Sorting Must be Performed Before Assigning Line Numbers, Otherwise the Line Numbers May not be Arranged Incrementally.
    ReferPDF = ReferPDF.sort_values(["code", "date"], ascending=[True, True])
    # ..............................................
    ReferPDF["row_num(wk)"] = ReferPDF.groupby("code")["date(wk)"].rank(method='first', ascending=True).astype("int64")
    # ==========================================
    return ReferPDF

# Pandas 1.x Stock: Conditional Picking.
def Pandas_1_x_Stock_Conditional_Picking(Input:pandas.core.frame.DataFrame) -> pandas.core.frame.DataFrame:

    df = Input.copy()
    
    df = df[df["ma5(dy)"] > df["ma10(dy)"]]
    #df = df[df["ma5(WK)"] > df["ma10(WK)"]]
    # ..................................................
    df = df[(df["macd_dif(dy)"] > 0) & (df["macd_dea(dy)"] >= 0)]
    #df = df[(df["macd_dif(wk)"] > 0) & (df["macd_dea(wk)"] >= 0)]
    # ..................................................
    df = df[df["etg_tbrs_i(dy)"] == '1']
    df = df[df["etg_tbrs_i(wk)"] == '1']
    # ..................................................
    df = df.sort_values(by="date", ascending=False)
    
    # ==========================================
    return df

# EOF Signed by GF.
