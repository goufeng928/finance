# GF_PY3_DIY_Tushare_1_2_x_API_Segmented_Get_Data.py
# Environment: Python 3.8.0
# Create By GF 2024-04-13 20:00

# ##################################################

# Dep: datetime ------- From Python 3.8.0 Own.
# Dep: pandas 1.4.1 --- From Third-Party Library.
# Dep: tushare 1.2.89 - From Third-Party Library.

# ##################################################

import sys
sys.path.append('./')
sys.path.append('./COLLECTION')

import os
import datetime
import pandas
import tushare
# ..................................................
from GF_PY3_GLOBAL_VARIABLE import *
from GF_PY3_FUNCTION_SQLAlchemy_1_x import *
from GF_PY3_DIY_String_Type_Date_and_Time_Standardization import *

# ##################################################
# Global 选项:

CACHE_DISK = "F:"

# ##################################################

# Tushare 1.2.x API 分段获取数据并将其合并。
def Tushare_1_2_x_API_Segmented_Get_Data_and_Merge_Them(Configured_API:object, Adjusted, Stocks_List:list, Begin_Date:object, Ends_Date:object) -> pandas.core.frame.DataFrame:

    # Configured_API: "Configured_API" Refers to The Tushare API Interface That Has Already Been Configured with Token.

    # ----------------------------------------------
    Length_of_Picking_Stocks = len(Stocks_List)
    Quantity_Per_Piece = 10
    Remainder_of_Segment = Length_of_Picking_Stocks % 5
    Number_of_Segment = (Length_of_Picking_Stocks - Remainder_of_Segment) / Quantity_Per_Piece
    # ..............................................
    print("Length of Stocks List:", Length_of_Picking_Stocks)
    print("Quantity Per Piece:", Quantity_Per_Piece)
    print("Remainder of Segment:", Remainder_of_Segment)
    print("Number of Segment:", Number_of_Segment)
    
    # ----------------------------------------------
    BgnDateStr:object = Begin_Date
    EndDateStr:object = Ends_Date
    # ..............................................
    if (type(Begin_Date) is datetime.datetime): BgnDateStr = Begin_Date.strftime("%Y%m%d")
    if (type(Ends_Date)  is datetime.datetime): EndDateStr = Ends_Date.strftime("%Y%m%d")
    # ..............................................
    if (type(Begin_Date) is str): BgnDateStr = String_Type_Date_Standardization(Begin_Date).replace('/', str(''))
    if (type(Ends_Date)  is str): EndDateStr = String_Type_Date_Standardization(Ends_Date).replace('/', str(''))
    
    # ----------------------------------------------
    MY_ADJUSTED:str = None # Default: "None" Equals "不复权(Unadjusted)"
    if (Adjusted ==  "Forward"): MY_ADJUSTED = "qfq"
    if (Adjusted == "Backward"): MY_ADJUSTED = "hfq"

    # ----------------------------------------------
    All_Cache_File_List = os.listdir(CACHE_DISK + "\\DATASET\\API_CACHE\\")
    
    if (("tushare_1_2_x_api_segmented_get_data_and_merge_them_" + BgnDateStr + '_' + EndDateStr + ".csv") not in All_Cache_File_List):
    
        Start_Position_of_Slice = 0
        End_Position_of_Slice = Quantity_Per_Piece
        # ..........................................
        Combined_Data_PDF:pandas.core.frame.DataFrame = None
        
        for Seq in range(0, int(Number_of_Segment)):
            
            if (Seq == 0):
                
                First_Fetch_Data_PDF = Configured_API.daily(
                                           adj=MY_ADJUSTED,
                                           ts_code=str(',').join(Stocks_List[Start_Position_of_Slice:End_Position_of_Slice]),
                                           start_date=BgnDateStr,
                                           end_date=EndDateStr
                                       )
                print("%dth Piece:\n" % Seq, Stocks_List[Start_Position_of_Slice:End_Position_of_Slice])
    
                # Pandas 列名映射 (Columns Name Mapping)。
                # ----------------------------------
                First_Fetch_Data_PDF = First_Fetch_Data_PDF.rename(columns=GV_JSON_MAPPING_HEADER_TO_CSV[0]) # -> 调用 Pandas 的 .rename() 方法更改列名。
                # ..................................
                First_Fetch_Data_PDF['date'] = pandas.to_datetime(First_Fetch_Data_PDF['date'], format='%Y-%m-%d')
                
                # 添加 "复权(Adjusted)" 列。
                # ----------------------------------
                First_Fetch_Data_PDF["adjusted"] = None
                # ..........................................
                if (Adjusted == "Unadjusted"): First_Fetch_Data_PDF["adjusted"] = "不复权"
                if (Adjusted ==    "Forward"): First_Fetch_Data_PDF["adjusted"] = "前复权"
                if (Adjusted ==   "Backward"): First_Fetch_Data_PDF["adjusted"] = "后复权"
                
                Write_Path = (CACHE_DISK + "\\DATASET\\API_CACHE\\tushare_1_2_x_api_segmented_get_data_and_merge_them_" + BgnDateStr + "_" + EndDateStr + ".csv")
                First_Fetch_Data_PDF.to_csv(Write_Path, mode='w', index=False)
                
                Start_Position_of_Slice = Start_Position_of_Slice + Quantity_Per_Piece
                End_Position_of_Slice = End_Position_of_Slice + Quantity_Per_Piece
    
            if (Seq  > 0):
                
                Combining_Data_PDF = Configured_API.daily(
                                         adj=MY_ADJUSTED,
                                         ts_code=str(',').join(Stocks_List[Start_Position_of_Slice:End_Position_of_Slice]),
                                         start_date=BgnDateStr,
                                         end_date=EndDateStr
                                     )
                print("%dth Piece:\n" % Seq, Stocks_List[Start_Position_of_Slice:End_Position_of_Slice])
    
                # Pandas 列名映射 (Columns Name Mapping)。
                # ----------------------------------
                Combining_Data_PDF = Combining_Data_PDF.rename(columns=GV_JSON_MAPPING_HEADER_TO_CSV[0]) # -> 调用 Pandas 的 .rename() 方法更改列名。
                # ..................................
                Combining_Data_PDF['date'] = pandas.to_datetime(Combining_Data_PDF['date'], format='%Y-%m-%d')
                
                # 添加 "复权(Adjusted)" 列。
                # ----------------------------------
                Combining_Data_PDF["adjusted"] = None
                # ..................................
                if (Adjusted == "Unadjusted"): Combining_Data_PDF["adjusted"] = "不复权"
                if (Adjusted ==    "Forward"): Combining_Data_PDF["adjusted"] = "前复权"
                if (Adjusted ==   "Backward"): Combining_Data_PDF["adjusted"] = "后复权"
                
                Read_Path = (CACHE_DISK + "\\DATASET\\API_CACHE\\tushare_1_2_x_api_segmented_get_data_and_merge_them_" + BgnDateStr + "_" + EndDateStr + ".csv")
                Previous_Save_Data_PDF = pandas.read_csv(Read_Path)
                Previous_Save_Data_PDF['date'] = pandas.to_datetime(Previous_Save_Data_PDF['date'], format='%Y-%m-%d')
                
                Combined_Data_PDF = pandas.concat([Previous_Save_Data_PDF, Combining_Data_PDF])
                
                Write_Path = (CACHE_DISK + "\\DATASET\\API_CACHE\\tushare_1_2_x_api_segmented_get_data_and_merge_them_" + BgnDateStr + "_" + EndDateStr + ".csv")
                Combined_Data_PDF.to_csv(Write_Path, mode='w', index=False)
                
                Start_Position_of_Slice = Start_Position_of_Slice + Quantity_Per_Piece
                End_Position_of_Slice = End_Position_of_Slice + Quantity_Per_Piece

        # ==========================================
        return Combined_Data_PDF
    
    else:
        
        Read_Path = (CACHE_DISK + "\\DATASET\\API_CACHE\\tushare_1_2_x_api_segmented_get_data_and_merge_them_" + BgnDateStr + "_" + EndDateStr + ".csv")
        
        # Tips: 读取 CSV 文件, 不自动解析日期列为日期时间格式, 而保持为字符串格式。
        # PDF = pd.read_csv('your_file.csv', parse_dates=['date_column'])
    
        Cache_Data_PDF = pandas.read_csv(Read_Path)
        Cache_Data_PDF['date'] = pandas.to_datetime(Cache_Data_PDF['date'], format='%Y-%m-%d')
        
        # ==========================================
        return Cache_Data_PDF

def Tushare_1_2_x_API_Segmented_Get_Stocks_Multi_Date_Daily_Data_to_MySQL(Configured_API:object, Adjusted, Stocks_List:list, Start_Date:object, End_Date:object) -> int:

    # Configured_API: "Configured_API" Refers to The Tushare API Interface That Has Already Been Configured with Token.

    # ----------------------------------------------
    Length_of_Picking_Stocks = len(Stocks_List)
    Quantity_Per_Piece = 10
    Remainder_of_Segment = Length_of_Picking_Stocks % 5
    Number_of_Segment = (Length_of_Picking_Stocks - Remainder_of_Segment) / Quantity_Per_Piece
    # ..............................................
    print("[Working] Tushare_1_2_x_API_Segmented_Get_Stocks_Multi_Date_Daily_Data_to_MySQL:")
    print("[Message] Length of Stocks List:", Length_of_Picking_Stocks)
    print("[Message] Quantity Per Piece:", Quantity_Per_Piece)
    print("[Message] Remainder of Segment:", Remainder_of_Segment)
    print("[Message] Number of Segment:", Number_of_Segment)
    
    # ----------------------------------------------
    SDATE:object = Start_Date
    EDATE:object = End_Date
    # ..............................................
    if (type(Start_Date) is datetime.datetime): SDATE = Start_Date.strftime("%Y-%m-%d")
    if (type(End_Date)   is datetime.datetime): EDATE = End_Date.strftime("%Y-%m-%d")
    # ..............................................
    if (type(Start_Date) is str): SDATE = String_Type_Date_Standardization(Start_Date).replace('/', '-')
    if (type(End_Date)   is str): EDATE = String_Type_Date_Standardization(End_Date).replace('/', '-')
    
    # ----------------------------------------------
    INPUT_ADJUSTED:str = None # Default: "None" Equals "不复权(Unadjusted)"
    if (Adjusted ==  "Forward"): INPUT_ADJUSTED = "qfq"
    if (Adjusted == "Backward"): INPUT_ADJUSTED = "hfq"

    # ----------------------------------------------
    Engine = SQLAlchemy_1_x_Create_Engine_MySQL()
    # ..............................................
    SQL_Statement:str = \
    """
    SELECT
        *
    FROM
        dataset_stocks_log
    WHERE
        f_source   = "Tushare"     AND
        f_site     = "tushare.pro" AND
        f_time_gl  = "Daily"       AND
        f_adjusted = "不复权"      AND
        f_date     = "%s"          AND
        f_insert   > 0             AND
        f_memo     = "Segmented";
    """ % (EDATE)
    
    PandasDF_dataset_stocks_log = pandas.read_sql_query(SQL_Statement, Engine)
    # ..............................................
    Insert_History = PandasDF_dataset_stocks_log["f_insert"].count()
    
    if (Insert_History == 0):
    
        Start_Position_of_Slice = 0
        End_Position_of_Slice = Quantity_Per_Piece
        # ..........................................
        TOTAL_RECORDS_NUM:int = 0
        
        for Seq in range(0, int(Number_of_Segment)):
            
            PandasDF_API = Configured_API.daily(
                adj=INPUT_ADJUSTED,
                ts_code=str(',').join(Stocks_List[Start_Position_of_Slice:End_Position_of_Slice]),
                start_date=SDATE.replace('-', str('')),
                end_date=EDATE.replace('-', str(''))
            )
    
            # Pandas 列名映射 (Columns Name Mapping)。
            # --------------------------------------
            PandasDF_API = PandasDF_API.rename(columns=GV_JSON_MAPPING_HEADER_TO_CSV[0]) # -> 调用 Pandas 的 .rename() 方法更改列名。
            # ......................................
            PandasDF_API['date'] = pandas.to_datetime(PandasDF_API['date'], format='%Y-%m-%d')
            
            # 添加 "复权(Adjusted)" 列。
            # --------------------------------------
            PandasDF_API["adjusted"] = None
            # ......................................
            if (Adjusted == "Unadjusted"): PandasDF_API["adjusted"] = "不复权"
            if (Adjusted ==    "Forward"): PandasDF_API["adjusted"] = "前复权"
            if (Adjusted ==   "Backward"): PandasDF_API["adjusted"] = "后复权"
            # ......................................
            PandasDF_API["source"]   = "Tushare"
            PandasDF_API["site"]     = "tushare.pro"
            PandasDF_API["time_gl"]  = "Daily"
            
            PandasDF_API = PandasDF_API.rename(columns=GV_JSON_MAPPING_HEADER_TO_DATABASE[0])
            
            # --------------------------------------
            TABLE_NAME:str = "dataset_stocks"
            WIRITING_RECORDS_NUM:int = PandasDF_API["f_date"].count()
            print("[Message] Segmented Writing to Database %s Table, Number of Records Written: %d." % (TABLE_NAME, WIRITING_RECORDS_NUM))
            # ......................................
            PandasDF_API.to_sql("dataset_stocks", con=Engine, index=False, if_exists='append', method='multi')
            
            # --------------------------------------
            TOTAL_RECORDS_NUM = TOTAL_RECORDS_NUM + WIRITING_RECORDS_NUM
            # ......................................
            Start_Position_of_Slice = Start_Position_of_Slice + Quantity_Per_Piece
            End_Position_of_Slice = End_Position_of_Slice + Quantity_Per_Piece
            
        # ------------------------------------------
        PandasDF_Log = pandas.DataFrame({"f_source"   : ["Tushare"],
                                         "f_site"     : ["tushare.pro"],
                                         "f_time_gl"  : ["Daily"],
                                         "f_adjusted" : ["不复权"],
                                         "f_date"     : [EDATE],
                                         "f_insert"   : [TOTAL_RECORDS_NUM],
                                         "f_memo"     : ["Segmented"]})
        PandasDF_Log.to_sql('dataset_stocks_log', con=Engine, index=False, if_exists='append', method='multi')
        # ==========================================
        return 1
    
    else:
        
        TABLE_NAME:str = "dataset_stocks"
        print("[Working] Tushare_1_2_x_API_Segmented_Get_Stocks_Multi_Date_Daily_Data_to_MySQL:")
        print("[Message] Segmented Not Written to Database %s Table, There is %d Insert History." % (TABLE_NAME, Insert_History))
        # ==========================================
        return 0

# ##################################################
# EOF
