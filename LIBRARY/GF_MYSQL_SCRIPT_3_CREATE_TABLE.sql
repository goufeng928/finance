-- source /home/goufeng/GF_MYSQL_SCRIPT_3_CREATE_TABLE.sql

-- Create by GF 2023-06-20

/* -------------------------------------------------- */

/* Delete Data Table (Only Delete if it Exists) */
DROP TABLE IF EXISTS dataset_variable;

/* Create Data Table (Only if it does not Exist) */
CREATE TABLE IF NOT EXISTS dataset_variable(f_id      INT NOT NULL AUTO_INCREMENT,
                                            f_date    DATE,
                                            f_name    VARCHAR(32),
                                            f_type    VARCHAR(32),
                                            f_index   BIGINT(16),
                                            f_string  VARCHAR(32),
                                            f_integer BIGINT(16),
                                            f_decimal DOUBLE(16,4),
                                            PRIMARY KEY (f_id));

/* -------------------------------------------------- */

GRANT INSERT, DELETE, UPDATE ON dataset.dataset_variable TO 'goufeng'@'%' WITH GRANT OPTION;

/* -------------------------------------------------- */

/* Delete Data Table (Only Delete if it Exists) */
DROP TABLE IF EXISTS dataset_stocks;

/* Create Data Table (Only if it does not Exist) */
CREATE TABLE IF NOT EXISTS dataset_stocks(f_id               INT NOT NULL AUTO_INCREMENT,
                                          f_source           VARCHAR(32),   -- 来源 (Source)
                                          f_site             VARCHAR(32),   -- 地址 (Site)
                                          f_time_gl          VARCHAR(16),   -- 时间粒度 (Time Granularity Level)
                                          f_adjusted         VARCHAR(16),   -- 复权 (Adjusted)
                                          f_date             DATE,          -- 日期 (Date)
                                          f_code             VARCHAR(16),   -- 代码 (Code)
                                          f_name             VARCHAR(16),   -- 名称 (Name)
                                          f_open             DOUBLE(16,4),  -- 开盘价 (Open)
                                          f_high             DOUBLE(16,4),  -- 最高价 (High)
                                          f_low              DOUBLE(16,4),  -- 最低价 (Low)
                                          f_close            DOUBLE(16,4),  -- 收盘价 (Close)
                                          f_pre_close        DOUBLE(16,4),  -- 前收盘 (Previous Close)
                                          f_change           DOUBLE(16,4),  -- 涨跌额 (Change)
                                          f_chg_pct          DOUBLE(16,4),  -- 涨跌率 (Change Percent)
                                          f_turnover_rate    DOUBLE(16,4),  -- 换手率 (Turnover Rate)
                                          f_volume           DOUBLE(16,4),  -- 成交量 (Volume)
                                          f_amount           DOUBLE(16,4),  -- 成交额 (Amount)
                                          f_total_market_val DOUBLE(16,4),  -- 总市值 (Total Market Value)
                                          f_circu_market_val DOUBLE(16,4),  -- 流通市值 (Circulation Market Value)
                                          f_transaction_num  DOUBLE(16,4),  -- 成交笔数 (Transaction Number)
                                          f_memo             VARCHAR(32),
                                          PRIMARY KEY (f_id));

/* -------------------------------------------------- */

GRANT INSERT ON dataset.dataset_stocks TO 'goufeng'@'%' WITH GRANT OPTION;

/* -------------------------------------------------- */

/* Delete Data Table (Only Delete if it Exists) */
DROP TABLE IF EXISTS dataset_stocks_log;

/* Create Data Table (Only if it does not Exist) */
CREATE TABLE IF NOT EXISTS dataset_stocks_log(f_id       INT NOT NULL AUTO_INCREMENT,
                                              f_source   VARCHAR(32),
                                              f_site     VARCHAR(32),
                                              f_time_gl  VARCHAR(16),
                                              f_adjusted VARCHAR(16),
                                              f_date     DATE,
                                              f_before   BIGINT(16),
                                              f_insert   BIGINT(16),
                                              f_delete   BIGINT(16),
                                              f_after    BIGINT(16),
                                              f_memo     VARCHAR(32),
                                              PRIMARY KEY (f_id));

/* -------------------------------------------------- */

GRANT INSERT ON dataset.dataset_stocks_log TO 'goufeng'@'%' WITH GRANT OPTION;

/* -------------------------------------------------- */

/* Delete Data Table (Only Delete if it Exists) */
DROP TABLE IF EXISTS dataset_stocks_picked;

/* Create Data Table (Only if it does not Exist) */
CREATE TABLE IF NOT EXISTS dataset_stocks_picked(f_id            INT NOT NULL AUTO_INCREMENT,
                                                 f_source        VARCHAR(32),
                                                 f_site          VARCHAR(32),
                                                 f_time_gl       VARCHAR(16),
                                                 f_adjusted      VARCHAR(16),
                                                 f_date          DATE,
                                                 f_code          VARCHAR(16),
                                                 f_name          VARCHAR(16),
                                                 f_open          DOUBLE(16,4),
                                                 f_high          DOUBLE(16,4),
                                                 f_low           DOUBLE(16,4),
                                                 f_close         DOUBLE(16,4),
                                                 f_pre_close     DOUBLE(16,4),
                                                 f_change        DOUBLE(16,4),
                                                 f_chg_pct       DOUBLE(16,4),
                                                 f_volume        DOUBLE(16,4),
                                                 f_memo          VARCHAR(32),
                                                 PRIMARY KEY (f_id));

/* -------------------------------------------------- */

GRANT INSERT ON dataset.dataset_stocks_picked TO 'goufeng'@'%' WITH GRANT OPTION;

/* -------------------------------------------------- */
/* EOF */
