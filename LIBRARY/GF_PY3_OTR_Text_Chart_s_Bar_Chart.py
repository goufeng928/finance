
def Text_Chart_s_Bar_Chart(Value:float, Posi_Total:float, Nega_Total:float, Tag:str):

    # 使用符号: ■ / □
    
    if (Value >= 0): Posi_Percentage:float = Value / Posi_Total
    if (Value  < 0): Nega_Percentage:float = Value / Nega_Total * -1

    # [    0,     1,     2,     3,     4,     5,     6,     7,     8,     9,     10]
    # [-1.00, -0.80, -0.60, -0.40, -0.20,     0,   0.20,  0.40,  0.60,  0.80,  1.00]

    Line:list = [-1.00, -0.95, -0.90, -0.85, -0.80, -0.75, -0.70, -0.65, -0.60, -0.55,
                 -0.50, -0.45, -0.40, -0.35, -0.30, -0.25, -0.20, -0.15, -0.10, -0.05,
                     0,
                  0.05,  0.10,  0.15,  0.20,  0.25,  0.30,  0.35,  0.40,  0.45,  0.50,
                  0.55,  0.60,  0.65,  0.70,  0.75,  0.80,  0.85,  0.90,  0.95,  1.00]
    
    List_Length:int = len(Line)
    i:int = 0
    while (i <= (List_Length - 1)):
    
        # 如果传入值为正数。
        if (Value >= 0):
            
            if ((0  < Line[i]) and (Line[i] <= Posi_Percentage)):
                Line[i] = str("■")
            else:
                if (Line[i] != 0): Line[i] = str('')

        # 如果传入值为负数。
        if (Value  < 0):
            
            if ((Nega_Percentage <= Line[i]) and (Line[i]  < 0)):
                Line[i] = str("■")
            else:
                if (Line[i] != 0): Line[i] = str('')
        
        if (Line[i] == 0): Line[i] = str("|")
        
        i = i + 1

    Bar_Chart_Line:str = str('').join(Line) + str(' ') + Tag
    
    return Bar_Chart_Line

print(Text_Chart_s_Bar_Chart( 3, 15, -15, "C"))
print(Text_Chart_s_Bar_Chart( 1, 15, -15, "C"))
print(Text_Chart_s_Bar_Chart( 2, 15, -15, "C"))
print(Text_Chart_s_Bar_Chart( 4, 15, -15, "C"))
print(Text_Chart_s_Bar_Chart( 5, 15, -15, "C"))
print(Text_Chart_s_Bar_Chart(-5, 15, -15, "C"))
print(Text_Chart_s_Bar_Chart(-2, 15, -15, "C"))
print(Text_Chart_s_Bar_Chart(-1, 15, -15, "C"))
print(Text_Chart_s_Bar_Chart(-3, 15, -15, "C"))
print(Text_Chart_s_Bar_Chart(-4, 15, -15, "C"))
