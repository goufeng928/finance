# Environment: Python 3.8.0
# Script Name: GF_Matplotlib_CLASS.py

# Create By GF 2024-04-13 20:00

# Dep: pandas 1.4.1 ----- From Third-Party Library.
# Dep: matplotlib 3.6.3 - From Third-Party Library.

# ####################################################################################################

# Matplotlib.pyplot 中 subplots() 函数说明:
# ..................................................
# 函数签名:
#     subplots(nrows=1, ncols=1, sharex=False, sharey=False, squeeze=True, **fig_kw)
# 参数说明:
#     nrows: 表示子图布局的行数, 默认为 1。
#     ncols: 表示子图布局的列数, 默认为 1。
#     sharex: 设置是否共享 X 轴刻度, 默认为 False。
#     sharey: 设置是否共享 Y 轴刻度, 默认为 False。
#     squeeze: 设置是否自动压缩子图布局, 默认为 True。

# ####################################################################################################

import pandas
import matplotlib.pyplot

# ####################################################################################################

class GMatplotlib(object):

    def __init__(self):
    
        # 占位变量。
        self.PlaceholderVariable = None
    
    def InitRcParams():

        # 配置 matplotlib.pyplot 的 rcParams 指定默认字体 (解决中文无法显示的问题)。
        matplotlib.pyplot.rcParams['font.sans-serif'] = ['SimHei']
        # ..........................................
        # 配置 matplotlib.pyplot 的 rcParams 解决保存图像时 负号(-) 显示方块的问题。
        matplotlib.pyplot.rcParams['axes.unicode_minus'] = False

    def DrawStockCandleChart(self, OpenCol:pandas.core.series.Series, HighCol:pandas.core.series.Series, LowCol:pandas.core.series.Series, CloseCol:pandas.core.series.Series, ChangeCol:pandas.core.series.Series):
    
        # 调用 matplotlib.pyplot 的 .figure 方法创建并配置画布分辨率。
        fig = matplotlib.pyplot.figure(figsize=(16, 9), dpi=96)
        # ..........................................
        # 为创建的画布添加轴域(Axes)。
        ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
        
        # ------------------------------------------
        # 股票蜡烛图(Candle Chart)数据: 红绿蜡烛线的底部(Bottom), 高度(Height), 垂直线(Vertical Line)。
        Candle_Red_Idx = (ChangeCol[ChangeCol >= 0.0]).index
        Candle_Red_Bottom = OpenCol[Candle_Red_Idx]
        Candle_Red_Height = CloseCol[Candle_Red_Idx] - OpenCol[Candle_Red_Idx]
        Candle_Red_VLine_Min = LowCol[Candle_Red_Idx]
        Candle_Red_VLine_Max = HighCol[Candle_Red_Idx]
        # ..........................................
        Candle_Gre_Idx = (ChangeCol[ChangeCol < 0.0]).index
        Candle_Gre_Bottom = CloseCol[Candle_Gre_Idx]
        Candle_Gre_Height = OpenCol[Candle_Gre_Idx] - CloseCol[Candle_Gre_Idx]
        Candle_Gre_VLine_Min = LowCol[Candle_Gre_Idx]
        Candle_Gre_VLine_Max = HighCol[Candle_Gre_Idx]
    
        # ------------------------------------------
        # 绘制股票蜡烛图(Candle Chart)。
        ax.bar(x=Candle_Red_Idx, height=Candle_Red_Height, width=0.6, bottom=Candle_Red_Bottom, color="#FF4500")
        ax.bar(x=Candle_Gre_Idx, height=Candle_Gre_Height, width=0.6, bottom=Candle_Gre_Bottom, color="#228B22")
        # ..................................................
        ax.vlines(x=Candle_Red_Idx, ymin=Candle_Red_VLine_Min, ymax=Candle_Red_VLine_Max, colors="#FF4500", linewidth=1)
        ax.vlines(x=Candle_Gre_Idx, ymin=Candle_Gre_VLine_Min, ymax=Candle_Gre_VLine_Max, colors="#228B22", linewidth=1)
        
        # ------------------------------------------
        matplotlib.pyplot.show()

# ####################################################################################################
# EOF
