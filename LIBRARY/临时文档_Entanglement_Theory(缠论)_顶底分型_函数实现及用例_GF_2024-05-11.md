# DOC_Entanglement_Theory(缠论)_顶底分型_函数实现及用例_GF_2024-05-11.md

Create By GF 2024-05-11 17:35

----------------------------------------------------------------------------------------------------

# 1 Entanglement Theory(缠论) - Top Reversal Shape (顶分型形态)

## 1.1 Previous Steps(前置步骤):

* 计算索引: 按日期从小到大排序逐行分配 1 - N 的整数。

* 计算 K 线上沿: K 线上沿 = MAX(开盘价, 收盘价) | K 线上沿 = MAX(最高价, 最低价)。

* 计算 K 线下沿: K 线下沿 = MIN(开盘价, 收盘价) | K 线下沿 = MIN(最高价, 最低价)。

## 1.2 Parameter Requirements(参数要求)

* 索引

* K 线上沿

* K 线下沿

## 1.3 Calculation Procedure(计算步骤)

### 1.3.1 Define Global Variables(定义全局变量)

1. 定义简称 Top Reversal Shape -> TRS。

2. 初始化 TRS_Left_Exists(布尔值) = 0

3. 初始化 TRS_Left_UpperEdge(数值) = NULL。

4. 初始化 TRS_Left_LowerEdge(数值) = NULL。

5. 初始化 TRS_Top_Exists(布尔值) = 0

6. 初始化 TRS_Top_UpperEdge(数值) = NULL。

7. 初始化 TRS_Top_LowerEdge(数值) = NULL。

### 1.3.2 当 "Index(索引) == 1 或者 TRS_Left_Exists == 0" 时

```python
if ((Index == 1) or (TRS_Left_Exists == 0)): # -> 当前为第 1 根 K 线, 或者 分型左侧不存在。

    CURR_for_UpperEdge:float = Input_UpperEdge # -> 可能多余的步骤(仅用于表达传递关系)。
    CURR_for_LowerEdge:float = Input_LowerEdge # -> 可能多余的步骤(仅用于表达传递关系)。

    TRS_Left_UpperEdge = CURR_for_UpperEdge
    TRS_Left_LowerEdge = CURR_for_LowerEdge

    TRS_Left_Exists = 1
    TRS_Top_Exists = 0

    return None
```

### 1.3.3 当 "Index(索引) == 2 或者 (TRS_Left_Exists == 1 并且 TRS_Top_Exists(布尔值) == 0)" 时

当前置条件为 `[C 上边沿]` > `[L 上边沿]` 时, 再列出 `[C 下边沿]` 和 `[L 下边沿]` 的关系。

![图_1_3_3_P1](./DOC_Entanglement_Theory_缠论_顶分型_函数实现及用例_图_1_3_3_P1.png)

当前置条件为 `[C 上边沿]` = `[L 上边沿]` 时, 再列出 `[C 下边沿]` 和 `[L 下边沿]` 的关系。

![图_1_3_3_P2](./DOC_Entanglement_Theory_缠论_顶分型_函数实现及用例_图_1_3_3_P2.png)

当前置条件为 `[C 上边沿]` < `[L 上边沿]` 时, 再列出 `[C 下边沿]` 和 `[L 下边沿]` 的关系。

![图_1_3_3_P3](./DOC_Entanglement_Theory_缠论_顶分型_函数实现及用例_图_1_3_3_P3.png)

**代码实现**:

```python
if ((Index == 2) or ((TRS_Left_Exists == 1) and (TRS_Top_Exists == 0))): # -> 当前为第 2 根 K 线, 或者 (分型左侧存在 并且 分型顶部不存在)。

    CURR_for_UpperEdge:float = Input_UpperEdge
    CURR_for_LowerEdge:float = Input_LowerEdge

    if (CURR_for_UpperEdge  > TRS_Left_UpperEdge): # -> 当前输入 K 线上沿 大于 顶分型左侧上沿。

        if (CURR_for_LowerEdge  > TRS_Left_LowerEdge): # -> 分型顶部成立。

            TRS_Top_UpperEdge = CURR_for_UpperEdge
            TRS_Top_LowerEdge = CURR_for_LowerEdge
            TRS_Top_Exists = 1

            return None

        if (CURR_for_LowerEdge <= TRS_Left_LowerEdge): # -> 包含关系成立, 当前输入 K 线 包含了 顶分型左侧。

            TRS_Left_UpperEdge = CURR_for_UpperEdge
            TRS_Left_LowerEdge = CURR_for_LowerEdge

            return None

    if (CURR_for_UpperEdge == TRS_Left_UpperEdge): # -> 当前输入 K 线上沿 等于 顶分型左侧上沿。

        if (CURR_for_LowerEdge >= TRS_Left_LowerEdge): # -> 被包含关系成立, 当前输入 K 线 被 顶分型左侧 包含。

            pass

            return None

        if (CURR_for_LowerEdge  < TRS_Left_LowerEdge): # -> 包含关系成立, 当前输入 K 线 包含了 顶分型左侧。

            TRS_Left_UpperEdge = CURR_for_UpperEdge # -> 可能多余的步骤(仅用于表达逻辑关系)。
            TRS_Left_LowerEdge = CURR_for_LowerEdge

            return None

    if (CURR_for_UpperEdge  < TRS_Left_UpperEdge): # -> 当前输入 K 线上沿 小于 顶分型左侧上沿。

        if (CURR_for_LowerEdge >= TRS_Left_LowerEdge): # -> 被包含关系成立, 当前输入 K 线 被 顶分型左侧 包含。

            pass

            return None

        if (CURR_for_LowerEdge  < TRS_Left_LowerEdge): # -> 分型底部成立, 重置分型左侧。

            # 在判断顶分型的函数中, 排除底分型相关结论, 重置分型左侧 (重置上下沿而不是重置状态)。
            # TRS_Top_Exists = 0 # -> 此例为重置状态的例子。
            TRS_Left_UpperEdge = CURR_for_UpperEdge
            TRS_Left_LowerEdge = CURR_for_LowerEdge

            return None
```

### 1.3.4 当 "Index(索引) >= 3 并且 (TRS_Left_Exists == 1 并且 TRS_Top_Exists(布尔值) == 1)" 时

当前置条件为 `[C 上边沿]` > `[T 上边沿]` 时, 再列出 `[C 下边沿]` 和 `[T 下边沿]` 以及 `[L 下边沿]` 的关系。

![图_1_3_4_P1](./DOC_Entanglement_Theory_缠论_顶分型_函数实现及用例_图_1_3_4_P1.png)

当前置条件为 `[C 上边沿]` = `[T 上边沿]` 时, 再列出 `[C 下边沿]` 和 `[T 下边沿]` 以及 `[L 下边沿]` 的关系。

![图_1_3_4_P2](./DOC_Entanglement_Theory_缠论_顶分型_函数实现及用例_图_1_3_4_P2.png)

当前置条件为 `[C 上边沿]` < `[T 上边沿]` 时, 再列出 `[C 下边沿]` 和 `[T 下边沿]` 以及 `[L 下边沿]` 的关系。

![图_1_3_4_P3](./DOC_Entanglement_Theory_缠论_顶分型_函数实现及用例_图_1_3_4_P3.png)

**代码实现**:

```python
if ((Index >= 3) and ((TRS_Left_Exists == 1) and (TRS_Top_Exists == 1))): # -> 当前为第 3 根或之后的 K 线, 并且 分型左侧 和 分型顶部 都存在。

    CURR_for_UpperEdge:float = Input_UpperEdge
    CURR_for_LowerEdge:float = Input_LowerEdge

    if (CURR_for_UpperEdge  > TRS_Top_UpperEdge): # -> 当前输入 K 线上沿 大于 顶分型顶部上沿。

        if (CURR_for_LowerEdge  > TRS_Top_LowerEdge) and (CURR_for_LowerEdge  > TRS_Left_LowerEdge): # -> 新的 分型顶部上边沿 和 分型顶部下边沿 以及 新的 分型左侧上边沿 和 分型左侧下边沿。

            TRS_Left_UpperEdge = TRS_Top_UpperEdge
            TRS_Left_LowerEdge = TRS_Top_LowerEdge
            # ..............................
            TRS_Top_UpperEdge = CURR_for_UpperEdge
            TRS_Top_LowerEdge = CURR_for_LowerEdge

            return None

        if (CURR_for_LowerEdge == TRS_Top_LowerEdge) and (CURR_for_LowerEdge  > TRS_Left_LowerEdge): # -> 新的 分型顶部上边沿。

            TRS_Top_UpperEdge = CURR_for_UpperEdge

            return None

        if (CURR_for_LowerEdge  < TRS_Top_LowerEdge) and (CURR_for_LowerEdge >= TRS_Left_LowerEdge): # -> 包含关系, 新的 分型顶部上边沿 和 分型顶部下边沿。

            TRS_Top_UpperEdge = CURR_for_UpperEdge
            TRS_Top_LowerEdge = CURR_for_LowerEdge

            return None

        if (CURR_for_LowerEdge  < TRS_Top_LowerEdge) and (CURR_for_LowerEdge  < TRS_Left_LowerEdge): # -> 顶分型成立。

            TRS_Left_Exists = 0
            TRS_Top_Exists = 0 # -> 可能多余的步骤(仅用于表达逻辑关系)。

            return "Top Reversal Shape"

    if (CURR_for_UpperEdge == TRS_Top_UpperEdge): # -> 当前输入 K 线上沿 等于 顶分型顶部上沿。

        if (CURR_for_LowerEdge >= TRS_Top_LowerEdge) and (CURR_for_LowerEdge  > TRS_Left_LowerEdge): # -> 被包含关系成立, 当前输入 K 线 被 顶分型顶部 包含。

            pass

            return None

        if (CURR_for_LowerEdge  < TRS_Top_LowerEdge) and (CURR_for_LowerEdge >= TRS_Left_LowerEdge): # -> 包含关系, 新的 分型顶部上边沿 和 分型顶部下边沿。

            TRS_Top_UpperEdge = CURR_for_UpperEdge # -> 可能多余的步骤(仅用于表达逻辑关系)。
            TRS_Top_LowerEdge = CURR_for_LowerEdge

            return None

        if (CURR_for_LowerEdge  < TRS_Top_LowerEdge) and (CURR_for_LowerEdge  < TRS_Left_LowerEdge): # -> 顶分型成立。

            TRS_Left_Exists = 0
            TRS_Top_Exists = 0 # -> 可能多余的步骤(仅用于表达逻辑关系)。

            return "Top Reversal Shape"

    if (CURR_for_UpperEdge  < TRS_Top_UpperEdge): # -> 当前输入 K 线上沿 小于 顶分型顶部上沿。

        if (CURR_for_LowerEdge >= TRS_Top_LowerEdge) and (CURR_for_LowerEdge  > TRS_Left_LowerEdge): # -> 被包含关系成立, 当前输入 K 线 被 顶分型顶部 包含。

            pass

            return None

        if (CURR_for_LowerEdge  < TRS_Top_LowerEdge) and (CURR_for_LowerEdge >= TRS_Left_LowerEdge): # -> 新的 分型顶部下边沿。

            TRS_Top_LowerEdge = CURR_for_LowerEdge

            return None

        if (CURR_for_LowerEdge  < TRS_Top_LowerEdge) and (CURR_for_LowerEdge  < TRS_Left_LowerEdge): # -> 顶分型成立。

            TRS_Left_Exists = 0
            TRS_Top_Exists = 0 # -> 可能多余的步骤(仅用于表达逻辑关系)。

            return "Top Reversal Shape"
```

----------------------------------------------------------------------------------------------------

# 2 Entanglement Theory (缠论) - Bottom Reversal Shape (底分型形态)

## 2.1 Previous Steps(前置步骤):

* 计算索引: 按日期从小到大排序逐行分配 1 - N 的整数。

* 计算 K 线上沿: K 线上沿 = MAX(开盘价, 收盘价) | K 线上沿 = MAX(最高价, 最低价)。

* 计算 K 线下沿: K 线下沿 = MIN(开盘价, 收盘价) | K 线下沿 = MIN(最高价, 最低价)。

## 2.2 Parameter Requirements(参数要求)

* 索引

* K 线上沿

* K 线下沿

## 2.3 Calculation Procedure(计算步骤)

### 2.3.1 Define Global Variables(定义全局变量)

1. 定义简称 Bottom Reversal Shape -> BRS。

2. 初始化 BRS_Left_Exists(布尔值) = 0

3. 初始化 BRS_Left_UpperEdge(数值) = NULL。

4. 初始化 BRS_Left_LowerEdge(数值) = NULL。

5. 初始化 BRS_Bottom_Exists(布尔值) = 0

6. 初始化 BRS_Bottom_UpperEdge(数值) = NULL。

7. 初始化 BRS_Bottom_LowerEdge(数值) = NULL。

### 2.3.2 当 "Index(索引) == 1 或者 BRS_Left_Exists == 0" 时

```python
if ((Index == 1) or (BRS_Left_Exists == 0)): # -> 当前为第 1 根 K 线, 或者 分型左侧不存在。

    CURR_for_UpperEdge:float = Input_UpperEdge # -> 可能多余的步骤(仅用于表达传递关系)。
    CURR_for_LowerEdge:float = Input_LowerEdge # -> 可能多余的步骤(仅用于表达传递关系)。

    BRS_Left_UpperEdge = CURR_for_UpperEdge
    BRS_Left_LowerEdge = CURR_for_LowerEdge

    BRS_Left_Exists = 1
    BRS_Bottom_Exists = 0

    return None
```

### 2.3.3 当 "Index(索引) == 2 或者 (BRS_Left_Exists == 1 并且 BRS_Bottom_Exists(布尔值) == 0)" 时

当前置条件为 `[C 下边沿]` > `[L 下边沿]` 时, 再列出 `[C 上边沿]` 和 `[L 上边沿]` 的关系。

![图_2_3_3_P1](./DOC_Entanglement_Theory_缠论_底分型_函数实现及用例_图_2_3_3_P1.png)

当前置条件为 `[C 下边沿]` = `[L 下边沿]` 时, 再列出 `[C 上边沿]` 和 `[L 上边沿]` 的关系。

![图_2_3_3_P2](./DOC_Entanglement_Theory_缠论_底分型_函数实现及用例_图_2_3_3_P2.png)

当前置条件为 `[C 下边沿]` < `[L 下边沿]` 时, 再列出 `[C 上边沿]` 和 `[L 上边沿]` 的关系。

![图_2_3_3_P3](./DOC_Entanglement_Theory_缠论_底分型_函数实现及用例_图_2_3_3_P3.png)

**代码实现**:

```python
if ((Index == 2) or ((BRS_Left_Exists == 1) and (BRS_Bottom_Exists == 0))): # -> 当前为第 2 根 K 线, 或者 (分型左侧存在 并且 分型底部不存在)。

    CURR_for_UpperEdge:float = Input_UpperEdge
    CURR_for_LowerEdge:float = Input_LowerEdge

    if (CURR_for_LowerEdge  > BRS_Left_LowerEdge): # -> 当前输入 K 线下沿 大于 底分型左侧下沿。

        if (CURR_for_UpperEdge <= BRS_Left_UpperEdge): # -> 被包含关系成立, 当前输入 K 线 被 底分型左侧 包含。

            pass

            return None

        if (CURR_for_UpperEdge  > BRS_Left_UpperEdge): # -> 分型顶部成立, 重置分型左侧。

            # 在判断底分型的函数中, 排除顶分型相关结论, 重置分型左侧 (重置上下沿而不是重置状态)。
            # BRS_Bottom_Exists = 0 # -> 此例为重置状态的例子。
            BRS_Left_UpperEdge = CURR_for_UpperEdge
            BRS_Left_LowerEdge = CURR_for_LowerEdge

            return None

    if (CURR_for_LowerEdge == BRS_Left_LowerEdge): # -> 当前输入 K 线下沿 等于 底分型左侧下沿。

        if (CURR_for_UpperEdge <= BRS_Left_UpperEdge): # -> 被包含关系成立, 当前输入 K 线 被 底分型左侧 包含。
        
             pass
        
             return None

        if (CURR_for_UpperEdge  > BRS_Left_UpperEdge): # -> 包含关系成立, 当前输入 K 线 包含了 底分型左侧。

            BRS_Left_UpperEdge = CURR_for_UpperEdge
            BRS_Left_LowerEdge = CURR_for_LowerEdge # -> 可能多余的步骤(仅用于表达逻辑关系)。

            return None

    if (CURR_for_LowerEdge  < BRS_Left_LowerEdge): # -> 当前输入 K 线下沿 小于 底分型左侧下沿。

        if (CURR_for_UpperEdge  < BRS_Left_UpperEdge): # -> 分型底部成立。

            BRS_Bottom_UpperEdge = CURR_for_UpperEdge
            BRS_Bottom_LowerEdge = CURR_for_LowerEdge
            BRS_Bottom_Exists = 1

            return None

        if (CURR_for_UpperEdge >= BRS_Left_UpperEdge): # -> 包含关系成立, 当前输入 K 线 包含了 底分型左侧。

            BRS_Left_UpperEdge = CURR_for_UpperEdge
            BRS_Left_LowerEdge = CURR_for_LowerEdge

            return None
```

### 2.3.4 当 "Index(索引) >= 3 并且 (BRS_Left_Exists == 1 并且 BRS_Bottom_Exists(布尔值) == 1)" 时

当前置条件为 `[C 下边沿]` > `[B 下边沿]` 时, 再列出 `[C 上边沿]` 和 `[B 上边沿]` 以及 `[L 上边沿]` 的关系。

![图_2_3_4_P1](./DOC_Entanglement_Theory_缠论_顶分型_函数实现及用例_图_2_3_4_P1.png)

当前置条件为 `[C 下边沿]` = `[B 下边沿]` 时, 再列出 `[C 上边沿]` 和 `[B 上边沿]` 以及 `[L 上边沿]` 的关系。

![图_2_3_4_P2](./DOC_Entanglement_Theory_缠论_顶分型_函数实现及用例_图_2_3_4_P2.png)

当前置条件为 `[C 下边沿]` < `[B 下边沿]` 时, 再列出 `[C 上边沿]` 和 `[B 上边沿]` 以及 `[L 上边沿]` 的关系。

![图_2_3_4_P3](./DOC_Entanglement_Theory_缠论_顶分型_函数实现及用例_图_2_3_4_P3.png)

**代码实现**:

```python
if ((Index >= 3) and ((BRS_Left_Exists == 1) and (BRS_Bottom_Exists == 1))): # -> 当前为第 3 根或之后的 K 线, 并且 分型左侧 和 分型底部 都存在。

    CURR_for_UpperEdge:float = Input_UpperEdge
    CURR_for_LowerEdge:float = Input_LowerEdge
    
    if (CURR_for_LowerEdge  > BRS_Bottom_LowerEdge): # -> 当前输入 K 线下沿 大于 底分型底部下沿。

        if (CURR_for_UpperEdge <= BRS_Bottom_UpperEdge) and (CURR_for_UpperEdge  < BRS_Left_UpperEdge): # -> 被包含关系成立, 当前输入 K 线 被 底分型底部 包含。

            pass

            return None

        if (CURR_for_UpperEdge  > BRS_Bottom_UpperEdge) and (CURR_for_UpperEdge <= BRS_Left_UpperEdge): # -> 新的 分型底部上边沿。

            BRS_Bottom_UpperEdge = CURR_for_UpperEdge

            return None

        if (CURR_for_UpperEdge  > BRS_Bottom_UpperEdge) and (CURR_for_UpperEdge  > BRS_Left_UpperEdge): # -> 底分型成立。

            BRS_Left_Exists = 0
            BRS_Bottom_Exists = 0 # -> 可能多余的步骤(仅用于表达逻辑关系)。

            return "Bottom Reversal Shape"

    if (CURR_for_LowerEdge == BRS_Bottom_LowerEdge): # -> 当前输入 K 线上沿 等于 顶分型顶部上沿。

        if (CURR_for_UpperEdge <= BRS_Bottom_UpperEdge) and (CURR_for_UpperEdge  < BRS_Left_UpperEdge): # -> 被包含关系成立, 当前输入 K 线 被 底分型底部 包含。

            pass

            return None

        if (CURR_for_UpperEdge  > BRS_Bottom_UpperEdge) and (CURR_for_UpperEdge <= BRS_Left_UpperEdge): # -> 包含关系, 新的 分型底部上边沿 和 分型底部下边沿。

            BRS_Bottom_UpperEdge = CURR_for_UpperEdge
            BRS_Bottom_LowerEdge = CURR_for_LowerEdge # -> 可能多余的步骤(仅用于表达逻辑关系)。

            return None

        if (CURR_for_UpperEdge  > BRS_Bottom_UpperEdge) and (CURR_for_UpperEdge  > BRS_Left_UpperEdge): # -> 底分型成立。

            BRS_Left_Exists = 0
            BRS_Bottom_Exists = 0 # -> 可能多余的步骤(仅用于表达逻辑关系)。

            return "Bottom Reversal Shape"

    if (CURR_for_LowerEdge  < BRS_Bottom_LowerEdge): # -> 当前输入 K 线上沿 小于 顶分型顶部上沿。

        if (CURR_for_UpperEdge  < BRS_Bottom_UpperEdge) and (CURR_for_UpperEdge  < BRS_Left_UpperEdge): # -> 新的 分型底部上边沿 和 分型底部下边沿 以及 新的 分型左侧上边沿 和 分型左侧下边沿。

            BRS_Left_UpperEdge = BRS_Bottom_UpperEdge
            BRS_Left_LowerEdge = BRS_Bottom_LowerEdge
            # ..............................
            BRS_Bottom_UpperEdge = CURR_for_UpperEdge
            BRS_Bottom_LowerEdge = CURR_for_LowerEdge

            return None

        if (CURR_for_UpperEdge == BRS_Bottom_UpperEdge) and (CURR_for_UpperEdge  < BRS_Left_UpperEdge): # -> 新的 分型底部下边沿。

            BRS_Bottom_LowerEdge = CURR_for_LowerEdge

            return None

        if (CURR_for_UpperEdge  > BRS_Bottom_UpperEdge) and (CURR_for_UpperEdge <= BRS_Left_UpperEdge): # -> 包含关系, 新的 分型底部上边沿 和 分型底部下边沿。

            BRS_Bottom_UpperEdge = CURR_for_UpperEdge
            BRS_Bottom_LowerEdge = CURR_for_LowerEdge

            return None

        if (CURR_for_UpperEdge  > BRS_Bottom_UpperEdge) and (CURR_for_UpperEdge  > BRS_Left_UpperEdge): # -> 底分型成立。

            BRS_Left_Exists = 0
            BRS_Bottom_Exists = 0 # -> 可能多余的步骤(仅用于表达逻辑关系)。

            return "Bottom Reversal Shape"
```

----------------------------------------------------------------------------------------------------
EOF
