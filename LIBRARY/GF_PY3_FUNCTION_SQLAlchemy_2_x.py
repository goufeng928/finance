# GF_PY3_FUNCTION_SQLAlchemy_2_x.py
# Environment: Python 3.8.0
# Create By GF 2024-04-13 20:00

# ##################################################

import sys
sys.path.append('./')
sys.path.append('./COLLECTION')
# ..................................................
import sqlalchemy
import sqlalchemy.orm
# ..................................................
from GF_PY3_GLOBAL_VARIABLE import *

# ##################################################

def SQLAlchemy_2_x_Create_Session_MySQL():

    Engine      = sqlalchemy.create_engine(f"mysql+pymysql://{GV_MYSQL_USER}:{GV_MYSQL_PASSWORD}@{GV_MYSQL_HOST}/dataset")
    Session     = sqlalchemy.orm.sessionmaker(bind=Engine)
    Session_Obj = Session()
    # ----------------------------------------------
    return Session_Obj

# ##################################################

Base   = sqlalchemy.orm.declarative_base()
Column = sqlalchemy.Column

class dataset_stocks(Base):

    __tablename__ = 'dataset_stocks'

    f_id               = Column(sqlalchemy.Integer, primary_key=True)
    f_source           = Column(sqlalchemy.VARCHAR)
    f_site             = Column(sqlalchemy.VARCHAR)
    f_time_gl          = Column(sqlalchemy.VARCHAR)
    f_adjusted         = Column(sqlalchemy.VARCHAR)
    f_date             = Column(sqlalchemy.DATE)
    f_code             = Column(sqlalchemy.VARCHAR)
    f_name             = Column(sqlalchemy.VARCHAR)
    f_open             = Column(sqlalchemy.DOUBLE)
    f_high             = Column(sqlalchemy.DOUBLE)
    f_low              = Column(sqlalchemy.DOUBLE)
    f_close            = Column(sqlalchemy.DOUBLE)
    f_pre_close        = Column(sqlalchemy.DOUBLE)
    f_change           = Column(sqlalchemy.DOUBLE)
    f_chg_pct          = Column(sqlalchemy.DOUBLE)
    f_turnover_rate    = Column(sqlalchemy.DOUBLE)
    f_volume           = Column(sqlalchemy.DOUBLE)
    f_amount           = Column(sqlalchemy.DOUBLE)
    f_total_market_val = Column(sqlalchemy.DOUBLE)
    f_circu_market_val = Column(sqlalchemy.DOUBLE)
    f_transaction_num  = Column(sqlalchemy.DOUBLE)
    f_memo             = Column(sqlalchemy.VARCHAR)

# ..................................................

Base   = sqlalchemy.orm.declarative_base()
Column = sqlalchemy.Column

class dataset_stocks_log(Base):

    __tablename__ = 'dataset_stocks_log'

    f_id               = Column(sqlalchemy.Integer, primary_key=True)
    f_source           = Column(sqlalchemy.VARCHAR)
    f_site             = Column(sqlalchemy.VARCHAR)
    f_time_gl          = Column(sqlalchemy.VARCHAR)
    f_adjusted         = Column(sqlalchemy.VARCHAR)
    f_date             = Column(sqlalchemy.DATE)
    f_before           = Column(sqlalchemy.BIGINT)
    f_insert           = Column(sqlalchemy.BIGINT)
    f_delete           = Column(sqlalchemy.BIGINT)
    f_after            = Column(sqlalchemy.BIGINT)
    f_memo             = Column(sqlalchemy.VARCHAR)

# ##################################################

# Mapping 类(Class) - SQLAlchemy 2.x
class Mapping_SQLAlchemy_2_x(object):

    def __init__(self):

        self.URI_MySQL   = (f"mysql+pymysql://{GV_MYSQL_USER}:{GV_MYSQL_PASSWORD}@{GV_MYSQL_HOST}/dataset")
        self.Session_Obj = None
    
    def Create_Session_MySQL(self) -> object:

        Engine         = sqlalchemy.create_engine(self.URI_MySQL)
        Session        = sqlalchemy.orm.sessionmaker(bind=Engine)
        Session_Object = Session()
        # ..........................................
        self.Session_Obj = Session_Object
        # ------------------------------------------
        return self.Session_Obj

    def Add_Record_for_dataset_stocks(self, Row) -> int:

        New_Record = dataset_stocks(
            f_source    = Row["f_source"],
            f_site      = Row["f_site"],
            f_time_gl   = Row["f_time_gl"],
            f_adjusted  = Row["f_adjusted"],
            f_date      = Row["f_date"],
            f_code      = Row["f_code"],
            f_open      = Row["f_open"],
            f_high      = Row["f_high"],
            f_low       = Row["f_low"],
            f_close     = Row["f_close"],
            f_pre_close = Row["f_pre_close"],
            f_change    = Row["f_change"],
            f_chg_pct   = Row["f_chg_pct"],
            f_volume    = Row["f_volume"],
            f_amount    = Row["f_amount"]
        )
        # ..........................................
        self.Session_Obj.add(New_Record)
        # ------------------------------------------
        return 1

    def Commit(self):

        self.Session_Obj.commit()

    def Close(self):

        self.Session_Obj.close()
