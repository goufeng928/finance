-- source /home/goufeng/GF_MYSQL_SCRIPT_1_CREATE_DATABASE.sql

-- Create by GF 2023-06-20

/* -------------------------------------------------- */
/* 创建数据库 */

/* 创建 "dataset" 数据库 (如果不存在才创建) */
CREATE DATABASE IF NOT EXISTS dataset DEFAULT charset utf8 COLLATE utf8_general_ci;

/* 创建 "working" 数据库 (如果不存在才创建) */
CREATE DATABASE IF NOT EXISTS working DEFAULT charset utf8 COLLATE utf8_general_ci;

/* -------------------------------------------------- */
/* EOF */
