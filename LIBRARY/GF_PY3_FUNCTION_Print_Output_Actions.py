# GF_PY3_FUNCTION_Print_Output_Actions.py
# Environment: Python 3.8.0
# Create By GF 2024-08-17 22:00

# ##################################################

def Print_Output_to_File_with_Write(File_Path:str, Content:object):

    MY_FILE = open(File_Path, 'w')
    
    print(Content, file=MY_FILE)
    
    MY_FILE.close()

def Print_Output_to_File_with_Append(File_Path:str, Content:object):

    MY_FILE = open(File_Path, 'a')
    
    print(Content, file=MY_FILE)
    
    MY_FILE.close()

def List_Format_Print(Input:list):

    List_Length = len(Input)
    
    print('[', end=str(''))
    print(Input[0], end=str(''))
    print(", ", end=str(''))
    print(Input[1], end=str(''))
    print(", ", end=str(''))
    print(Input[2], end=str(''))
    print(", ", end=str(''))
    print("...", end=str(''))
    print(", ", end=str(''))
    print(Input[List_Length - 3], end=str(''))
    print(", ", end=str(''))
    print(Input[List_Length - 2], end=str(''))
    print(", ", end=str(''))
    print(Input[List_Length - 1], end=str(''))
    print("] Length: %d" % List_Length)
