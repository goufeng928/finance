# Script Name: GF_PY38_CLASS_Mapping.py
# Environment: Python 3.8.0
# Create By GF 2024-01-22 18:16

import os

# Get Specific System Environment Variables.
LIBRARY_PATH:str = os.environ.get("GF_LIBRARY_PATH")

import sys

# Add "Library Path" to The Working Path of Python.
sys.path.append(LIBRARY_PATH)

# ################################################################################
# Mathematics Functions Related to Version 3.8.0 of Python.

def Safe_Multiply(self, multiplicand:float, Multiplier:float) -> float:

    if ((multiplicand == None) or (Multiplier == None)):
        # 如果被乘数为 None 或者乘数为 None。
        # ..........................................
        return None
    else:
        Result:float = (multiplicand * Multiplier)
        # ..........................................
        return Result

def Safe_Divide(self, Divisor:float, Dividend:float) -> float:

    if ((Dividend == 0.0) or (Dividend == None)):
        # 如果分母为 0 或者分母为 None。
        # ..........................................
        return None
    else:
        Result:float = (Divisor / Dividend)
        # ..........................................
        return Result

def List_Average(self, Lst:list) -> float:

    Result = self.Safe_Divide(sum(Lst), len(Lst))
    # ..............................................
    return Result

# ################################################################################
# JSON Functions Related to Version 3.8.0 of Python.

import json

def Read_JSONFile_and_Remove_Comments(JSONFilePath:str):

    JSONFile = open(JSONFilePath, 'r', encoding="utf-8")
    JSONString = JSONFile.read()
    
    # Define Regular Expressions to Match C-Style Comments.
    # (定义正则表达式来匹配 C 风格的注释)
    Comment_Pattern_1 = re.compile(r"\/\*.*\*\/")
    Comment_Pattern_2 = re.compile(r"\/\/.*")

    # Replace Comments with Regular Expressions.
    # (使用正则表达式替换掉注释)
    New_JSON_String = re.sub(Comment_Pattern_1, str(''), JSONString)
    New_JSON_String = re.sub(Comment_Pattern_2, str(''), New_JSON_String)

    # Parse JSON String.
    # (解析 JSON 字符串)
    ParsedJSON = json.loads(New_JSON_String)
    # ..............................................
    return ParsedJSON

def JSONString_Extract_Element_By_Index(JSONString:str, Index:int):

    # Convert "JSON String" to "Python List" (将 JSON 字符串转为 Python 列表)
    List_Obj = json.loads(JSONString)

    Element = List_Obj[Index]
    # ..............................................
    return Element

def JSONString_Replace_Element_By_Index(JSONString:str, Index:int, New_Element):

    # Convert "JSON String" to "Python List" (将 JSON 字符串转为 Python 列表)
    List_Obj = json.loads(JSONString)

    List_Obj[Index] = New_Element

    # 例如: Python 列表 [1, 2, 3] 将输出 '[1, 2, 3]'
    # (注意这里的输出实际上是带有双引号的, 并且数字之间有空格, 符合 JSON 标准)
    New_JSON_String = json.dumps(List_Obj)
    # ..............................................
    return New_JSON_String

# ################################################################################

# Mapping 类(Class) - 序列操作(Sequence Operate)。
class Mapping_SeqOpr(object):

    def __init__(self):

        # 通常使用 Map 映射操作属于单向迭代, 因此只有 向后填充(Fill Backward) 操作, 而不需要 向前填充(Fill Forward) 操作。
        self.PREPARED_FILL_VALUE_for_FILL_BACKWARD:object = None
        self.APPEARED_FILL_TYPES_for_FILL_BACKWARD_AND_COUNT:list = []
        self.PREPARED_FILL_VALUE_for_FILL_BACKWARD_AND_COUNT:object = None

    # 向后填充(Fill Backward).
    def Fill_Backward(self, Index:int, Value:object) -> object:

        if (Index == 1):

            self.PREPARED_FILL_VALUE_for_FILL_BACKWARD = Value
            # ......................................
            Fill_Value = Value
            # ......................................
            return Fill_Value

        else:

            if (Value == None):
                Fill_Value = self.PREPARED_FILL_VALUE_for_FILL_BACKWARD
                # ..................................
                return Fill_Value
            else:
                self.PREPARED_FILL_VALUE_for_FILL_BACKWARD = Value
                # ..................................
                Fill_Value = Value
                # ..................................
                return Fill_Value
        # ==========================================
        # End of Function.

    # 向后填充并计数(Fill Backward and Count).
    def Fill_Backward_and_Count(self, Index:int, Value:object) -> object:
    
        # Return => "[(Value), (), (Times)]" => "[1, 2, 3]"
        # ..........................................
        # There are a Total of 4 Situations When Filling:
        # 1. Prepared Fill Value is Empty, Passing Value is Non-Empty (Initialize Prepare Fill Value).
        # 2. Prepared Fill Value is Non-Empty, Passing Value is Empty (Filling).
        # 3. Prepared Fill Value is Non-Empty, Passing Value is Non-Empty (Reset Prepare Fill Value).
        # 4. Prepared Fill Value is Empty, Passing Value is Empty (Skip).

        if (Index == 1):

            self.APPEARED_FILL_TYPES_for_FILL_BACKWARD_AND_COUNT.clear()
            # ......................................
            self.PREPARED_FILL_VALUE_for_FILL_BACKWARD_AND_COUNT = None

        # Condition: Prepared Fill Value is Empty, Passing Value is Non-Empty (Initialize Prepare Fill Value).
        if (self.PREPARED_FILL_VALUE_for_FILL_BACKWARD_AND_COUNT == None) and (Value != None):

            # Used to Count The Number of Occurrences.
            self.APPEARED_FILL_TYPES_for_FILL_BACKWARD_AND_COUNT.append(Value)
            # ......................................
            # First Filling, with a Frequency of 1 Occurrence.
            # First Filling, with a Frequency of 1 Filling.
            Fill_Value = ("[%s, 1, 1]" % str(Value))
            # ......................................
            self.PREPARED_FILL_VALUE_for_FILL_BACKWARD_AND_COUNT = Fill_Value
            # ......................................
            return Fill_Value

        # Condition: Prepared Fill Value is Non-Empty, Passing Value is Empty (Filling).
        elif (self.PREPARED_FILL_VALUE_for_FILL_BACKWARD_AND_COUNT != None) and (Value == None):

            Fill_Value = \
            self.PREPARED_FILL_VALUE_for_FILL_BACKWARD_AND_COUNT
            # ......................................
            # Used to Count The Number of Fillings.
            Times = JSONString_Extract_Element_By_Index(Fill_Value, 2)
            Times = (Times + 1)
            # ......................................
            Fill_Value = \
            JSONString_Replace_Element_By_Index(Fill_Value, 2, Times)
            # ......................................
            self.PREPARED_FILL_VALUE_for_FILL_BACKWARD_AND_COUNT = Fill_Value
            # ......................................
            return Fill_Value

        # Condition: Prepared Fill Value is Non-Empty, Passing Value is Non-Empty (Reset Prepare Fill Value).
        elif (self.PREPARED_FILL_VALUE_for_FILL_BACKWARD_AND_COUNT != None) and (Value != None):

            # Used to Count The Number of Occurrences.
            self.APPEARED_FILL_TYPES_for_FILL_BACKWARD_AND_COUNT.append(Value)
            # ......................................
            # Number of Occurrences.
            Occurr_Num = \
            self.APPEARED_FILL_TYPES_for_FILL_BACKWARD_AND_COUNT.count(Value)
            # ......................................
            Fill_Value = ("[%s, %s, 1]" % (str(Value), str(Occurr_Num)))
            # ......................................
            self.PREPARED_FILL_VALUE_for_FILL_BACKWARD_AND_COUNT = Fill_Value
            # ......................................
            return Fill_Value

        # Others: Prepared Fill Value is Empty, Passing Value is Empty (Skip).
        else:

            return None
        # ==========================================
        # End of Function.

    # 向下填充(Fill Down).
    def Fill_Down(self, Index:int, Value:object) -> object:

        Fill_Value = self.Fill_Backward(Index=Index, Value=Value)
        # ==========================================
        return Fill_Value

    # 向下填充并计数(Fill Down and Count).
    def Fill_Down_and_Count(self, Index:int, Value:object) -> object:

        Fill_Value = self.Fill_Backward_and_Count(Index=Index, Value=Value)
        # ==========================================
        return Fill_Value

# ################################################################################

# Mapping 类(Class) - 统计指标(Statistics Indicator)。
class Mapping_StatInd(object):

    # 增长率 (Growth Rate) 说明:
    # --------------------------------------------------
    # 增长率 (Growth Rate) 也称增长速度, 表示一定时期内某一数据指标的增长量与基期数据的比值, 用百分数表示。
    # 由于对比的基期不同, 增长率可以分为:
    # * 同比增长率(Year-on-Year Growth Rate / YoY+%)
    # * 环比增长率(Chain Growth Rate)
    # * 定基增长率(Growth Rate of a Fixed Base)
    # * 序列增长率(Growth Rate of Sequence)
    # --------------------------------------------------
    # 同比增长率 = (本期的指标值 - 同期的指标值) / 同期的指标值 * 100%。比如, 2009年3月与2008年3月的同比增长率的基期为2008年3月份；
    # 环比增长率 = (本期的指标值 - 上期的指标值) / 上期的指标值 * 100%。比如, 2008年5月与2008年4月的环比增长率的基期为相邻期 (2008年4月), 一般会将若干年的环比增长率进行列表和分析比较。
    # 定基增长率 = (本期的指标值 - 定期的指标值) / 定期的指标值 * 100%, 适用于长期发展比较。比如, 将某一时期1970年, 1980年, 1990年和2000年的GNP数值与1949年进行比较, 所获得的4个比例, 称为定基增长率。
    # 序列增长率 = (当前序列位置指标值 - 上个序列位置指标值) / 上个序列位置指标值 * 100%。

    def __init__(self):

        self.VALUE_LIST_for_Growth_Rate_of_Sequence:list = []

    # 序列增长率(Growth Rate of Sequence).
    def Growth_Rate_of_Sequence(self, Index:int, Curr_Value:float) -> float:

        if (Index == 1):
            self.VALUE_LIST_for_Growth_Rate_of_Sequence.clear() # -> 首次执行函数先清空全局列表变量。
            # ......................................
            self.VALUE_LIST_for_Growth_Rate_of_Sequence.append(Curr_Value)
            # ......................................
            return None
        else:
            self.VALUE_LIST_for_Growth_Rate_of_Sequence.append(Curr_Value)
            # ......................................
            Idx = (Index - 1) # -> 由于行号索引是从 1 开始, 但 Python 列表索引是从 0 开始, 所以需要减去 1。
            # ......................................
            Prev_Value = self.VALUE_LIST_for_Growth_Rate_of_Sequence[Idx - 1]
            # ......................................
            Growth_Rate = Safe_Divide((Curr_Value - Prev_Value), Prev_Value) * 1.00
            # ......................................
            return Growth_Rate

    # 定基增长率(Growth Rate of a Fixed Base).
    def Growth_Rate_of_Fixed_Base(self, Fixed_Base:float, Curr_Value:float) -> float:

        Growth_Rate = Safe_Divide((Curr_Value - Fixed_Base), Fixed_Base) * 1.00
        # ------------------------------------------
        return Growth_Rate

# ################################################################################

# Mapping 类(Class) - 金融指标(Finance Indicator)。
class Mapping_FinInd(object):

    def __init__(self):

        self.HIGH_LIST_for_KDJ:list = []
        self.LOW_LIST_for_KDJ:list = []
        self.CLOSE_LIST_for_KDJ:list = []
        self.CHANGE_LIST_for_RSI:list = []
        self.EMA_LIST_for_EMA:list = []
        self.EMA_LIST_for_MACD_DEA:list = []
        self.K_LIST_for_KDJ:list = []
        self.D_LIST_for_KDJ:list = []

    # 指数移动平均(Exponential Moving Average).
    def EMA(self, Index:int, Period:int, Close:float) -> float:

        # 注意: 首日EMA直接使用当日收盘价。
        # ------------------------------------------
        # 公式：EMA = 当日收盘价 x 2/(N+1) + 前一日EMA x (N-1)/(N+1)
        # ------------------------------------------
        # 以计算12日EMA举例：
        # 其中"i"代表当日日期。
        # EMA12 = 2/(12+1) * Close[i] + (12+1-2)/(12+1) * EMA12[i-1]
        # 首日EMA由于没有昨日EMA数据, 所以首日EMA直接使用当日收盘价。

        # ------------------------------------------
        if (Index == 1):
            self.EMA_LIST_for_EMA.clear() # -> 首次执行函数先清空全局列表变量。
            # ......................................
            self.EMA_LIST_for_EMA.append(Close)
            # ......................................
            return Close
        else:
            Idx = (Index - 1) # -> 由于行号索引是从 1 开始, 但 Python 列表索引是从 0 开始, 所以需要减去 1。
            # ......................................
            EMA_Value = 2 / (Period + 1) * Close + (Period + 1 - 2) / (Period + 1) * self.EMA_LIST_for_EMA[Idx - 1]
            # ......................................
            self.EMA_LIST_for_EMA.append(EMA_Value)
            # ......................................
            return EMA_Value
        # ==========================================
        # End of Function.

    # 异同移动平均线 - DIF (Moving Average Convergence / Divergence - DIF).
    def MACD_DIF(self, EMA12:float, EMA26:float) -> float:

        # 公式：MACD_DIF = 当日EMA(12) - 当日EMA(26)
        # 12日EMA和26日EMA通常是MACD的常用值, 如要修改MACD的观测参数, 则修改对应的EMA数值。
        # ------------------------------------------
        MACD_DIF_Value = EMA12 - EMA26
        # ------------------------------------------
        return MACD_DIF_Value
        # ==========================================
        # End of Function.

    # 异同移动平均线 - DEA (Moving Average Convergence / Divergence - DEA).
    def MACD_DEA(self, Index:int, MACD_DIF:float) -> float:

        # 注意: 首日DEA直接使用0值。
        # ------------------------------------------
        # DEA又叫：计算DIF的9日EMA。
        # 根据离差值计算其9日的EMA, 即离差平均值, 是所求的MACD值。为了不与指标原名相混淆, 此值又名DEA。
        # 公式：当日DEA = 2/(9+1) * 当日DIF + (9+1-2)/(9+1) * 前日DEA
        # 首日DEA由于没有昨日DEA数据, 所以首日DEA直接使用0值。

        # ------------------------------------------
        if (Index == 1):
            self.EMA_LIST_for_MACD_DEA.clear() # -> 首次执行函数先清空全局列表变量。
            # ......................................
            self.EMA_LIST_for_MACD_DEA.append(0.0)
            # ......................................
            return 0.0
        else:
            Idx = (Index - 1) # -> 由于行号索引是从 1 开始, 但 Python 列表索引是从 0 开始, 所以需要减去 1。
            # ......................................
            MACD_DEA_Value = 2 / (9 + 1) * MACD_DIF + (9 + 1 - 2)/(9 + 1) * self.EMA_LIST_for_MACD_DEA[Idx - 1]
            # ......................................
            self.EMA_LIST_for_MACD_DEA.append(MACD_DEA_Value)
            # ......................................
            return MACD_DEA_Value
        # ##############################################
        # End of Function.

    # 异同移动平均线 - STICK (Moving Average Convergence / Divergence - STICK).
    def MACD_STICK(self, MACD_DIF:float, MACD_DEA:float) -> float:

        # 用 (DIF - DEA ) x 2 即为MACD柱状图, 一般称作MACD或STICK。
        # 公式：MACD_STICK(MACD) = (MACD_DIF - MACD_DEA) * 2
        # ------------------------------------------
        MACD_STICK_Value = (MACD_DIF - MACD_DEA) * 2
        # ------------------------------------------
        return MACD_STICK_Value
        # ==========================================
        # End of Function.

    # 相对强弱指标 (Relative Strength Index).
    def RSI(self, Index:int, Period:int, Change:float) -> float:

        # 相对强弱指标 RSI 是用以计测市场供需关系和买卖力道的方法及指标。
        #
        # 公式一:
        # RSI(N) = A ÷ ( A + B ) × 100
        # A = N 日内收盘价所有上涨额度之和
        # B = N 日内收盘价所有下跌额度之和(取正数, 即乘以(-1))
        #
        # 公式二:
        # RS(相对强度) = N日内收盘价所有上涨额度之和的平均值 ÷ N日内收盘价所有下跌额度之和的平均值(取绝对值)
        # RSI(相对强弱指标) = 100 - 100 ÷ ( 1 + RS )
        #
        # 这两个公式虽然有些不同, 但计算的结果一样。
        #
        # 股票 RSI 三条线分别为 RSI1, RSI2, RSI3。
        # RSI1 是白线, 一般指 6 日相对强弱指标;
        # RSI2 是黄线, 一般指 12 日相对强弱指标;
        # RSI3 是紫线, 一般指 24 日相对强弱指标.

        # ------------------------------------------
        if   (Index == 1):

            self.CHANGE_LIST_for_RSI.clear()
            # ......................................
            self.CHANGE_LIST_for_RSI.append(Change)
            # ......................................
            return None

        elif (1 < Index and Index < Period):

            self.CHANGE_LIST_for_RSI.append(Change)
            # ......................................
            return None

        else:

            # --------------------------------------
            self.CHANGE_LIST_for_RSI.append(Change)

            # --------------------------------------
            Idx = (Index - 1) # -> 由于行号索引是从 1 开始, 但 Python 列表索引是从 0 开始, 所以需要减去 1。

            # --------------------------------------
            # 提取周期内上涨之和(Change Up)和周期内下跌之和(Change Down)。
            Chg_Up_Sum:float = 0.0 # -> 周期内的上涨之和.
            Chg_Dn_Sum:float = 0.0 # -> 周期内的下跌之和.
            # ......................................
            Chg_Up_Sum = sum([self.CHANGE_LIST_for_RSI[i] for i in range((Idx + 1 - Period), (Idx + 1)) if self.CHANGE_LIST_for_RSI[i] >= 0.0])
            Chg_Dn_Sum = sum([self.CHANGE_LIST_for_RSI[i] * (-1) for i in range((Idx + 1 - Period), (Idx + 1)) if self.CHANGE_LIST_for_RSI[i] < 0.0])

            # --------------------------------------
            # 计算 RSI。
            # ......................................
            # 每天既没上涨也没下跌, 最近 N 天的所有的 Up Move 之和是 0, 最近 N 天的所有的 Down Move 之和是 0, RS 会是 0 除以 0。
            # 但实际并不处于每天都是上涨或每天都是下跌的情况, 所以行情属于中性, 这种特殊情况定义 RSI 为 50。
            if   (Chg_Up_Sum == 0.0) and (Chg_Dn_Sum == 0.0):

                return float(50.0)

            # 每天都是下跌, 这将导致没有 Up Move 的日期, 最近 N 天的所有的 Up Move 之和是 0, Down Move 会是某个正数, 0 除以某个正数是 0。
            # 所以这种特殊情况会定义 RSI 为 0。
            elif (Chg_Up_Sum == 0.0) and (Chg_Dn_Sum != 0.0):

                return float(0.0)

            # 每天都是上涨, 这将导致没有 Down Move 的日期, 最近 N 天的所有的 Down Move 之和是 0, RS 会是某个正数除以 0, 数学上这是非法的。
            # 所以这种特殊情况会定义 RSI 为 100。
            elif (Chg_Up_Sum != 0.0) and (Chg_Dn_Sum == 0.0):

                return float(100.0)

            else:

                RS:float = (Chg_Up_Sum / Period) / (Chg_Dn_Sum / Period)
                # ..................................
                RSI:float = (100 - 100 / (1 + RS))
                # ..................................
                return RSI
        # ==========================================
        # End of Function.

    # KDJ 随机指标 - RSV (KDJ Stochastic Oscillator - RSV).
    def KDJ_RSV(self, Index:int, Period:int, High:float, Low:float, Close:float) -> float:

        # KDJ 随机指标 - RSV (KDJ Stochastic Oscillator - RSV)
        # RSV 是指当日收盘价与过去一段时间的最低价和最高价之间的比值。
        # 计算条件: 股票在过去 9 个交易日的收盘价数据已知。
        # RSV 是衡量最近 N 天内 (例如 9 天) 收盘价相对于这 N 天最高价和最低价的位置的指标。其计算方法大致可以理解为:
        # RSV = (最近 N 天的收盘价 - 最近 N 天的最低价) / (最近 N 天的最高价 - 最近 N 天的最低价) * 100
        # 注意: 这里的乘 100 是为了将结果从 0 - 1 的数值转换为 0 - 100 的数值，方便观察。

        # ------------------------------------------
        if   (Index == 1):

            self.HIGH_LIST_for_KDJ.clear()
            self.LOW_LIST_for_KDJ.clear()
            self.CLOSE_LIST_for_KDJ.clear()
            # ......................................
            self.HIGH_LIST_for_KDJ.append(High)
            self.LOW_LIST_for_KDJ.append(Low)
            self.CLOSE_LIST_for_KDJ.append(Close)
            # ......................................
            return None

        elif (1 < Index and Index < Period):

            self.HIGH_LIST_for_KDJ.append(High)
            self.LOW_LIST_for_KDJ.append(Low)
            self.CLOSE_LIST_for_KDJ.append(Close)
            # ......................................
            return None

        else:

            # --------------------------------------
            self.HIGH_LIST_for_KDJ.append(High)
            self.LOW_LIST_for_KDJ.append(Low)
            self.CLOSE_LIST_for_KDJ.append(Close)

            # --------------------------------------
            Idx = (Index - 1) # -> 由于行号索引是从 1 开始, 但 Python 列表索引是从 0 开始, 所以需要减去 1。

            # --------------------------------------
            Min_Within_Period = min(self.LOW_LIST_for_KDJ[(Idx + 1 - Period):(Idx + 1)]) # -> 周期内(例如 9 日)最小值。
            Max_Within_Period = max(self.HIGH_LIST_for_KDJ[(Idx + 1 - Period):(Idx + 1)]) # -> 周期内(例如 9 日)最大值。

            # --------------------------------------
            RSV:float = (Close - Min_Within_Period) / (Max_Within_Period - Min_Within_Period) * 100
            # ......................................
            return RSV
        # ==========================================
        # End of Function.

    # KDJ 随机指标 - K (KDJ Stochastic Oscillator - K).
    def KDJ_K(self, Index:int, RSV_Prd:int, K_Prd:int, High:float, Low:float, Close:float) -> float:

        # KDJ 随机指标 - K (KDJ Stochastic Oscillator - K)
        # K 值是 RSV 的平滑值, 通常使用前一天的 K 值和当天的 RSV 值来计算。一个简化的计算方法是:
        # 当天 K 值 = 前一天的 K 值 * 2/3 + 当天的 RSV 值 * 1/3
        # 注意: 这里的 2/3 和 1/3 是平滑系数, 实际计算中可能会有所不同。
        # 在实际计算中, 如果没有前一日的 K 值数据, 通常会使用初始值, 如 50%。

        # ------------------------------------------
        # Calling Other Function.
        RSV = self.KDJ_RSV(Index=Index, Period=RSV_Prd, High=High, Low=Low, Close=Close)

        # ------------------------------------------
        if   (Index == 1):

            self.K_LIST_for_KDJ.clear()
            # ......................................
            self.K_LIST_for_KDJ.append(50)
            # ......................................
            return 50

        elif (1 < Index and Index < RSV_Prd): # -> RSV 值为空的时候均不计算, 因为 K 值计算依赖于 RSV 值。

            self.K_LIST_for_KDJ.append(50)
            # ......................................
            return 50

        else:

            # --------------------------------------
            Idx = (Index - 1) # -> 由于行号索引是从 1 开始, 但 Python 列表索引是从 0 开始, 所以需要减去 1。

            # --------------------------------------
            # 计算平滑系数(Smoothing Factor)。
            Smoothing_Factor:float = (1 / K_Prd)

            # --------------------------------------
            K_Val = Smoothing_Factor * RSV + (1 - Smoothing_Factor) * self.K_LIST_for_KDJ[Idx - 1]

            # --------------------------------------
            self.K_LIST_for_KDJ.append(K_Val)
            # ......................................
            return K_Val
        # ==========================================
        # End of Function.

    # KDJ 随机指标 - D (KDJ Stochastic Oscillator - D).
    def KDJ_D(self, Index:int, RSV_Prd:int, D_Prd:int, K_Val:float) -> float:

        # KDJ 随机指标 - D (KDJ Stochastic Oscillator - D)
        # D 值是 K 值的进一步平滑, 用于减缓 K 值的波动。其计算方法类似于 K 值, 但通常使用更长的平滑周期:
        # 当天 D 值 = 前一天的 D 值 * 2/3 + 当天的 K 值 * 1/3
        # 注意: 这里的 2/3 和 1/3 是平滑系数, 实际计算中可能会有所不同。
        # 在实际计算中, 如果没有前一日的 D 值数据, 通常会使用初始值, 如 50%。

        # ------------------------------------------
        if   (Index == 1):

            self.D_LIST_for_KDJ.clear()
            # ......................................
            self.D_LIST_for_KDJ.append(50)
            # ......................................
            return 50

        elif (1 < Index and Index < RSV_Prd): # -> RSV 值为空的时候均不计算, 因为 K 值计算依赖于 RSV, 而 D 值计算依赖于 K 值。

            self.D_LIST_for_KDJ.append(50)
            # ......................................
            return 50

        else:

            # --------------------------------------
            Idx = (Index - 1) # -> 由于行号索引是从 1 开始, 但 Python 列表索引是从 0 开始, 所以需要减去 1。

            # --------------------------------------
            # 计算平滑系数(Smoothing Factor)。
            Smoothing_Factor:float = (1 / D_Prd)

            # --------------------------------------
            D_Val = Smoothing_Factor * K_Val + (1 - Smoothing_Factor) * self.D_LIST_for_KDJ[Idx - 1]

            # --------------------------------------
            self.D_LIST_for_KDJ.append(D_Val)
            # ......................................
            return D_Val
        # ==========================================
        # End of Function.

    # KDJ 随机指标 - J (KDJ Stochastic Oscillator - J).
    def KDJ_J(self, K_Val:float, D_Val:float) -> float:

        # KDJ 随机指标 - J (KDJ Stochastic Oscillator - J)
        # J 值是 KDJ 指标中的方向敏感线, 用于反映市场趋势的强弱。它通常是通过 K 值和 D 值的某种组合来计算的, 例如:
        # 当天 J 值 = 3 * 当天 K 值 - 2 * 当天 D 值

        # ------------------------------------------
        J_Val = 3 * K_Val - 2 * D_Val
        # ......................................
        return J_Val
        # ==========================================
        # End of Function.

# Mapping 类(Class) - 缠论指标(Entanglement Theory Indicator).
class Mapping_EtgInd(object):

    def __init__(self):

        self.TRS_Left_Exists:int = 0
        self.TRS_Top_Exists:int = 0
        # ..........................................
        self.BRS_Left_Exists:int = 0
        self.BRS_Bottom_Exists:int = 0
        # ..........................................
        self.TRS_Left_UpperEdge:float = None
        self.TRS_Left_LowerEdge:float = None
        # ..........................................
        self.BRS_Left_UpperEdge:float = None
        self.BRS_Left_LowerEdge:float = None
        # ..........................................
        self.TRS_Top_UpperEdge:float = None
        self.TRS_Top_LowerEdge:float = None
        # ..........................................
        self.BRS_Bottom_UpperEdge:float = None
        self.BRS_Bottom_LowerEdge:float = None

    # 顶分型(Top Reversal Shape).
    def Top_Reversal_Shape(self, Index:int, IN_for_UpperEdge:float, IN_for_LowerEdge:float) -> str:

        if ((Index == 1) or (self.TRS_Left_Exists == 0)): # -> 当前为第 1 根 K 线, 或者 分型左侧不存在。

            CURR_for_UpperEdge:float = IN_for_UpperEdge # -> 可能多余的步骤(仅用于表达传递关系)。
            CURR_for_LowerEdge:float = IN_for_LowerEdge # -> 可能多余的步骤(仅用于表达传递关系)。

            self.TRS_Left_UpperEdge = CURR_for_UpperEdge
            self.TRS_Left_LowerEdge = CURR_for_LowerEdge

            self.TRS_Left_Exists = 1
            self.TRS_Top_Exists = 0

            return None

        if ((Index == 2) or ((self.TRS_Left_Exists == 1) and (self.TRS_Top_Exists == 0))): # -> 当前为第 2 根 K 线, 或者 (分型左侧存在 并且 分型顶部不存在)。

            CURR_for_UpperEdge:float = IN_for_UpperEdge
            CURR_for_LowerEdge:float = IN_for_LowerEdge

            if (CURR_for_UpperEdge  > self.TRS_Left_UpperEdge): # -> 当前输入 K 线上沿 大于 顶分型左侧上沿。

                if (CURR_for_LowerEdge  > self.TRS_Left_LowerEdge): # -> 分型顶部成立。

                    self.TRS_Top_UpperEdge = CURR_for_UpperEdge
                    self.TRS_Top_LowerEdge = CURR_for_LowerEdge
                    self.TRS_Top_Exists = 1

                    return None

                if (CURR_for_LowerEdge <= self.TRS_Left_LowerEdge): # -> 包含关系成立, 当前输入 K 线 包含了 顶分型左侧。

                    self.TRS_Left_UpperEdge = CURR_for_UpperEdge
                    self.TRS_Left_LowerEdge = CURR_for_LowerEdge

                    return None

            if (CURR_for_UpperEdge == self.TRS_Left_UpperEdge): # -> 当前输入 K 线上沿 等于 顶分型左侧上沿。

                if (CURR_for_LowerEdge >= self.TRS_Left_LowerEdge): # -> 被包含关系成立, 当前输入 K 线 被 顶分型左侧 包含。

                    pass

                    return None

                if (CURR_for_LowerEdge  < self.TRS_Left_LowerEdge): # -> 包含关系成立, 当前输入 K 线 包含了 顶分型左侧。

                    self.TRS_Left_UpperEdge = CURR_for_UpperEdge # -> 可能多余的步骤(仅用于表达逻辑关系)。
                    self.TRS_Left_LowerEdge = CURR_for_LowerEdge

                    return None

            if (CURR_for_UpperEdge  < self.TRS_Left_UpperEdge): # -> 当前输入 K 线上沿 小于 顶分型左侧上沿。

                if (CURR_for_LowerEdge >= self.TRS_Left_LowerEdge): # -> 被包含关系成立, 当前输入 K 线 被 顶分型左侧 包含。

                    pass

                    return None

                if (CURR_for_LowerEdge  < self.TRS_Left_LowerEdge): # -> 分型底部成立, 重置分型左侧。

                    # 在判断顶分型的函数中, 排除底分型相关结论, 重置分型左侧 (重置上下沿而不是重置状态)。
                    # self.TRS_Top_Exists = 0 # -> 此例为重置状态的例子。
                    self.TRS_Left_UpperEdge = CURR_for_UpperEdge
                    self.TRS_Left_LowerEdge = CURR_for_LowerEdge

                    return None

        if ((Index >= 3) and ((self.TRS_Left_Exists == 1) and (self.TRS_Top_Exists == 1))): # -> 当前为第 3 根或之后的 K 线, 并且 分型左侧 和 分型顶部 都存在。

            CURR_for_UpperEdge:float = IN_for_UpperEdge
            CURR_for_LowerEdge:float = IN_for_LowerEdge

            if (CURR_for_UpperEdge  > self.TRS_Top_UpperEdge): # -> 当前输入 K 线上沿 大于 顶分型顶部上沿。

                if (CURR_for_LowerEdge  > self.TRS_Top_LowerEdge) and (CURR_for_LowerEdge  > self.TRS_Left_LowerEdge): # -> 新的 分型顶部上边沿 和 分型顶部下边沿 以及 新的 分型左侧上边沿 和 分型左侧下边沿。

                    self.TRS_Left_UpperEdge = self.TRS_Top_UpperEdge
                    self.TRS_Left_LowerEdge = self.TRS_Top_LowerEdge
                    # ..............................
                    self.TRS_Top_UpperEdge = CURR_for_UpperEdge
                    self.TRS_Top_LowerEdge = CURR_for_LowerEdge

                    return None

                if (CURR_for_LowerEdge == self.TRS_Top_LowerEdge) and (CURR_for_LowerEdge  > self.TRS_Left_LowerEdge): # -> 新的 分型顶部上边沿。

                    self.TRS_Top_UpperEdge = CURR_for_UpperEdge

                    return None

                if (CURR_for_LowerEdge  < self.TRS_Top_LowerEdge) and (CURR_for_LowerEdge >= self.TRS_Left_LowerEdge): # -> 包含关系, 新的 分型顶部上边沿 和 分型顶部下边沿。

                    self.TRS_Top_UpperEdge = CURR_for_UpperEdge
                    self.TRS_Top_LowerEdge = CURR_for_LowerEdge

                    return None

                if (CURR_for_LowerEdge  < self.TRS_Top_LowerEdge) and (CURR_for_LowerEdge  < self.TRS_Left_LowerEdge): # -> 顶分型成立。

                    self.TRS_Left_Exists = 0
                    self.TRS_Top_Exists = 0 # -> 可能多余的步骤(仅用于表达逻辑关系)。

                    return "Top Reversal Shape"

            if (CURR_for_UpperEdge == self.TRS_Top_UpperEdge): # -> 当前输入 K 线上沿 等于 顶分型顶部上沿。

                if (CURR_for_LowerEdge >= self.TRS_Top_LowerEdge) and (CURR_for_LowerEdge  > self.TRS_Left_LowerEdge): # -> 被包含关系成立, 当前输入 K 线 被 顶分型顶部 包含。

                    pass

                    return None

                if (CURR_for_LowerEdge  < self.TRS_Top_LowerEdge) and (CURR_for_LowerEdge >= self.TRS_Left_LowerEdge): # -> 包含关系, 新的 分型顶部上边沿 和 分型顶部下边沿。

                    self.TRS_Top_UpperEdge = CURR_for_UpperEdge # -> 可能多余的步骤(仅用于表达逻辑关系)。
                    self.TRS_Top_LowerEdge = CURR_for_LowerEdge

                    return None

                if (CURR_for_LowerEdge  < self.TRS_Top_LowerEdge) and (CURR_for_LowerEdge  < self.TRS_Left_LowerEdge): # -> 顶分型成立。

                    self.TRS_Left_Exists = 0
                    self.TRS_Top_Exists = 0 # -> 可能多余的步骤(仅用于表达逻辑关系)。

                    return "Top Reversal Shape"

            if (CURR_for_UpperEdge  < self.TRS_Top_UpperEdge): # -> 当前输入 K 线上沿 小于 顶分型顶部上沿。

                if (CURR_for_LowerEdge >= self.TRS_Top_LowerEdge) and (CURR_for_LowerEdge  > self.TRS_Left_LowerEdge): # -> 被包含关系成立, 当前输入 K 线 被 顶分型顶部 包含。

                    pass

                    return None

                if (CURR_for_LowerEdge  < self.TRS_Top_LowerEdge) and (CURR_for_LowerEdge >= self.TRS_Left_LowerEdge): # -> 新的 分型顶部下边沿。

                    self.TRS_Top_LowerEdge = CURR_for_LowerEdge

                    return None

                if (CURR_for_LowerEdge  < self.TRS_Top_LowerEdge) and (CURR_for_LowerEdge  < self.TRS_Left_LowerEdge): # -> 顶分型成立。

                    self.TRS_Left_Exists = 0
                    self.TRS_Top_Exists = 0 # -> 可能多余的步骤(仅用于表达逻辑关系)。

                    return "Top Reversal Shape"

    # 底分型(Bottom Reversal Shape).
    def Bottom_Reversal_Shape(self, Index:int, IN_for_UpperEdge:float, IN_for_LowerEdge:float) -> str:

        if ((Index == 1) or (self.BRS_Left_Exists == 0)): # -> 当前为第 1 根 K 线, 或者 分型左侧不存在。

            CURR_for_UpperEdge:float = IN_for_UpperEdge # -> 可能多余的步骤(仅用于表达传递关系)。
            CURR_for_LowerEdge:float = IN_for_LowerEdge # -> 可能多余的步骤(仅用于表达传递关系)。

            self.BRS_Left_UpperEdge = CURR_for_UpperEdge
            self.BRS_Left_LowerEdge = CURR_for_LowerEdge

            self.BRS_Left_Exists = 1
            self.BRS_Bottom_Exists = 0

            return None

        if ((Index == 2) or ((self.BRS_Left_Exists == 1) and (self.BRS_Bottom_Exists == 0))): # -> 当前为第 2 根 K 线, 或者 (分型左侧存在 并且 分型底部不存在)。

            CURR_for_UpperEdge:float = IN_for_UpperEdge
            CURR_for_LowerEdge:float = IN_for_LowerEdge

            if (CURR_for_LowerEdge  > self.BRS_Left_LowerEdge): # -> 当前输入 K 线下沿 大于 底分型左侧下沿。

                if (CURR_for_UpperEdge <= self.BRS_Left_UpperEdge): # -> 被包含关系成立, 当前输入 K 线 被 底分型左侧 包含。

                    pass

                    return None

                if (CURR_for_UpperEdge  > self.BRS_Left_UpperEdge): # -> 分型顶部成立, 重置分型左侧。

                    # 在判断底分型的函数中, 排除顶分型相关结论, 重置分型左侧 (重置上下沿而不是重置状态)。
                    # self.BRS_Bottom_Exists = 0 # -> 此例为重置状态的例子。
                    self.BRS_Left_UpperEdge = CURR_for_UpperEdge
                    self.BRS_Left_LowerEdge = CURR_for_LowerEdge

                    return None

            if (CURR_for_LowerEdge == self.BRS_Left_LowerEdge): # -> 当前输入 K 线下沿 等于 底分型左侧下沿。

                if (CURR_for_UpperEdge <= self.BRS_Left_UpperEdge): # -> 被包含关系成立, 当前输入 K 线 被 底分型左侧 包含。

                     pass

                     return None

                if (CURR_for_UpperEdge  > self.BRS_Left_UpperEdge): # -> 包含关系成立, 当前输入 K 线 包含了 底分型左侧。

                    self.BRS_Left_UpperEdge = CURR_for_UpperEdge
                    self.BRS_Left_LowerEdge = CURR_for_LowerEdge # -> 可能多余的步骤(仅用于表达逻辑关系)。

                    return None

            if (CURR_for_LowerEdge  < self.BRS_Left_LowerEdge): # -> 当前输入 K 线下沿 小于 底分型左侧下沿。

                if (CURR_for_UpperEdge  < self.BRS_Left_UpperEdge): # -> 分型底部成立。

                    self.BRS_Bottom_UpperEdge = CURR_for_UpperEdge
                    self.BRS_Bottom_LowerEdge = CURR_for_LowerEdge
                    self.BRS_Bottom_Exists = 1

                    return None

                if (CURR_for_UpperEdge >= self.BRS_Left_UpperEdge): # -> 包含关系成立, 当前输入 K 线 包含了 底分型左侧。

                    self.BRS_Left_UpperEdge = CURR_for_UpperEdge
                    self.BRS_Left_LowerEdge = CURR_for_LowerEdge

                    return None

        if ((Index >= 3) and ((self.BRS_Left_Exists == 1) and (self.BRS_Bottom_Exists == 1))): # -> 当前为第 3 根或之后的 K 线, 并且 分型左侧 和 分型底部 都存在。

            CURR_for_UpperEdge:float = IN_for_UpperEdge
            CURR_for_LowerEdge:float = IN_for_LowerEdge

            if (CURR_for_LowerEdge  > self.BRS_Bottom_LowerEdge): # -> 当前输入 K 线下沿 大于 底分型底部下沿。

                if (CURR_for_UpperEdge <= self.BRS_Bottom_UpperEdge) and (CURR_for_UpperEdge  < self.BRS_Left_UpperEdge): # -> 被包含关系成立, 当前输入 K 线 被 底分型底部 包含。

                    pass

                    return None

                if (CURR_for_UpperEdge  > self.BRS_Bottom_UpperEdge) and (CURR_for_UpperEdge <= self.BRS_Left_UpperEdge): # -> 新的 分型底部上边沿。

                    self.BRS_Bottom_UpperEdge = CURR_for_UpperEdge

                    return None

                if (CURR_for_UpperEdge  > self.BRS_Bottom_UpperEdge) and (CURR_for_UpperEdge  > self.BRS_Left_UpperEdge): # -> 底分型成立。

                    self.BRS_Left_Exists = 0
                    self.BRS_Bottom_Exists = 0 # -> 可能多余的步骤(仅用于表达逻辑关系)。

                    return "Bottom Reversal Shape"

            if (CURR_for_LowerEdge == self.BRS_Bottom_LowerEdge): # -> 当前输入 K 线上沿 等于 顶分型顶部上沿。

                if (CURR_for_UpperEdge <= self.BRS_Bottom_UpperEdge) and (CURR_for_UpperEdge  < self.BRS_Left_UpperEdge): # -> 被包含关系成立, 当前输入 K 线 被 底分型底部 包含。

                    pass

                    return None

                if (CURR_for_UpperEdge  > self.BRS_Bottom_UpperEdge) and (CURR_for_UpperEdge <= self.BRS_Left_UpperEdge): # -> 包含关系, 新的 分型底部上边沿 和 分型底部下边沿。

                    self.BRS_Bottom_UpperEdge = CURR_for_UpperEdge
                    self.BRS_Bottom_LowerEdge = CURR_for_LowerEdge # -> 可能多余的步骤(仅用于表达逻辑关系)。

                    return None

                if (CURR_for_UpperEdge  > self.BRS_Bottom_UpperEdge) and (CURR_for_UpperEdge  > self.BRS_Left_UpperEdge): # -> 底分型成立。

                    self.BRS_Left_Exists = 0
                    self.BRS_Bottom_Exists = 0 # -> 可能多余的步骤(仅用于表达逻辑关系)。

                    return "Bottom Reversal Shape"

            if (CURR_for_LowerEdge  < self.BRS_Bottom_LowerEdge): # -> 当前输入 K 线上沿 小于 顶分型顶部上沿。

                if (CURR_for_UpperEdge  < self.BRS_Bottom_UpperEdge) and (CURR_for_UpperEdge  < self.BRS_Left_UpperEdge): # -> 新的 分型底部上边沿 和 分型底部下边沿 以及 新的 分型左侧上边沿 和 分型左侧下边沿。

                    self.BRS_Left_UpperEdge = self.BRS_Bottom_UpperEdge
                    self.BRS_Left_LowerEdge = self.BRS_Bottom_LowerEdge
                    # ..............................
                    self.BRS_Bottom_UpperEdge = CURR_for_UpperEdge
                    self.BRS_Bottom_LowerEdge = CURR_for_LowerEdge

                    return None

                if (CURR_for_UpperEdge == self.BRS_Bottom_UpperEdge) and (CURR_for_UpperEdge  < self.BRS_Left_UpperEdge): # -> 新的 分型底部下边沿。

                    self.BRS_Bottom_LowerEdge = CURR_for_LowerEdge

                    return None

                if (CURR_for_UpperEdge  > self.BRS_Bottom_UpperEdge) and (CURR_for_UpperEdge <= self.BRS_Left_UpperEdge): # -> 包含关系, 新的 分型底部上边沿 和 分型底部下边沿。

                    self.BRS_Bottom_UpperEdge = CURR_for_UpperEdge
                    self.BRS_Bottom_LowerEdge = CURR_for_LowerEdge

                    return None

                if (CURR_for_UpperEdge  > self.BRS_Bottom_UpperEdge) and (CURR_for_UpperEdge  > self.BRS_Left_UpperEdge): # -> 底分型成立。

                    self.BRS_Left_Exists = 0
                    self.BRS_Bottom_Exists = 0 # -> 可能多余的步骤(仅用于表达逻辑关系)。

                    return "Bottom Reversal Shape"

    def TBRS_String(self, TRS:str, BRS:str) -> str:

        if ((TRS == "Top Reversal Shape") and (BRS == None)):
            return "Top Reversal Shape"
        if ((TRS == None) and (BRS == "Bottom Reversal Shape")):
            return "Bottom Reversal Shape"
        if ((TRS == None) and (BRS == None)):
            return None
        if ((TRS == "Top Reversal Shape") and (BRS == "Bottom Reversal Shape")):
            # 前一根 K 线实体很短, 后一根实体很长, 比例差距越大, 反转信号就越强
            # 吞没形态的第二根 K 线伴随有明显放大的交易量, 信号增强
            # 特别是 "一吞九" (一根巨阳吞掉左边九根 K 线) 组合。这种组合无论是短线操作, 还是中长线威力都非常的不错, 少则 30% - 50% 涨幅, 多则涨幅翻番, 有的甚至几倍涨幅。
            return "Engulfing Pattern"

    def TBRS_Integer_Coding(self, TRS:str, BRS:str) -> str:

        if (self.TBRS_String(TRS=TRS, BRS=BRS) == "Top Reversal Shape"):
            return str(-1)
        if (self.TBRS_String(TRS=TRS, BRS=BRS) == "Bottom Reversal Shape"):
            return str( 1)
        if (self.TBRS_String(TRS=TRS, BRS=BRS) == "Engulfing Pattern"):
            return str( 0)
        if (self.TBRS_String(TRS=TRS, BRS=BRS) == None):
            return None

# EOF Signed by GF.
