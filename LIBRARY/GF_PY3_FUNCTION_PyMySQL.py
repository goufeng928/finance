def MySQL_Connecting():

    # 创建 MySQL 数据库连接。
    Connected = pymysql.connect(host="110.xx.xxx.xxx", # -> 主机名 / IP地址 (本地连接使用 localhost 或者 127.0.0.1)。
                                 user="goufeng",        # -> MySQL 数据库用户名。
                                 port=3306,             # -> MySQL 数据库端口 (默认为3306)。
                                 password='xxxxxxxx',   # -> MySQL 数据库用户密码。
                                 database="dataset",    # -> (可选项) 要使用的数据库。
                                 charset='utf8mb4',     # -> (可选项) 要使用的字符集。
                                 cursorclass=pymysql.cursors.DictCursor) # -> (可选项) 游标类型。
    
    return Connected

def MySQL_Execute_SQL_Stmt(Connected, SQL_Stmt:str):
    
    # 从创建的 MySQL 数据库连接使用 .cursor() 方法获取游标并保存到 Cursor 变量。
    Cursor = Connected.cursor()
    
    Cursor.execute(SQL_Stmt)
    
    # MySQL 是自动提交事务的 (即: autocommit=1)。
    # 但 Python 的 PyMySQL 默认是不自动提交事务的 (即: autocommit = false)。
    # 查询不需要 commit()。增(insert) / 删(delete) / 改(update) 需要 commit()，提交事务，操作才真正会影响到数据库中的数据，否则数据库数据不变。
    Connected.commit()
    
    Cursor.close()